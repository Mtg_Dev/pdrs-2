import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { RolesGuard } from './roles.guard';
import { ApiCookieAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/users/auth/jwt-auth-guard';

export function Auth(...roles: string[]) {
  return applyDecorators(
    SetMetadata('roles', roles),
    UseGuards(JwtAuthGuard, RolesGuard),
    ApiCookieAuth(),
  );
}
