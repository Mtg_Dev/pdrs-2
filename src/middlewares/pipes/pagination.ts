import { ApiQuery } from '@nestjs/swagger';
import { applyDecorators } from '@nestjs/common';
import { Pagination } from 'src/utils/decorators';

export function ApiPagination() {
  return applyDecorators(
    ApiQuery({
      name: 'size',
      type: Number,
      required: false,
    }),
    ApiQuery({
      name: 'page',
      type: Number,
      required: false,
    }),
  );
}
