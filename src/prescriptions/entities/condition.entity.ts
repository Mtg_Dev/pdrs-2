import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Classification } from './classification.entity';

@Entity()
export class Condition {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ nullable: false })
  classificationId: number;

  @ManyToOne(
    type => Classification,
    Classification => Classification.conditions,
    { onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'classificationId' })
  classification: Classification;
}
