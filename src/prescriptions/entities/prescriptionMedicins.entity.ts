import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Condition } from './condition.entity';
import { Doctor } from '../../users/entities/doctor.entity';
import { Prescription } from './prescription.entity';
import { Medicine } from './medicins.entity';
import { Pharmacy } from '../../users/entities/pharmacy.entity';

@Entity()
export class PrescriptionMedicine {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(
    type => Prescription,
    Prescription => Prescription.prescriptionMedicine,
    { onDelete: 'CASCADE' },
  )
  prescription: Prescription;

  @ManyToOne(type => Medicine)
  @JoinColumn({ name: 'medicineId' })
  medicine: Medicine;

  @Column({ nullable: true })
  medicineId: number;

  @ManyToOne(type => Pharmacy, { onDelete: 'SET NULL' })
  @JoinColumn({ name: 'pharmacyId' })
  pharmacy: Pharmacy;

  //this field is used to get the pharmacyId column without joining tables when querying
  @Column({ nullable: true })
  pharmacyId: number;

  @Column({ nullable: true })
  tempMedicine: string;

  @Column({ default: 1 })
  count: number;

  @Column({ default: false })
  isBold: boolean;

  @Column({ default: false })
  isChronic: boolean;
}
