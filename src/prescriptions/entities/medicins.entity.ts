import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
  JoinTable,
  ManyToMany,
} from 'typeorm';
import { Condition } from './condition.entity';

@Entity()
export class Medicine {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToMany(tyep => Medicine)
  @JoinTable({
    name: 'medicins_contradictions',
  })
  contradictingMedicins: Medicine[];
}
