import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToMany,
} from 'typeorm';
import { Condition } from './condition.entity';
import { Doctor } from '../../users/entities/doctor.entity';

@Entity()
export class Classification {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  image: string;

  @Column({ nullable: true })
  isPrivate: boolean;

  @OneToMany(
    type => Condition,
    Condition => Condition.classification,
  )
  conditions: Condition[];

  @ManyToMany(
    type => Doctor,
    d => d.classifications,
  )
  doctors: Doctor[];
}
