import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToMany,
  ManyToOne,
  CreateDateColumn,
  JoinColumn,
} from 'typeorm';
import { Condition } from './condition.entity';
import { Doctor } from '../../users/entities/doctor.entity';
import { Patient } from '../../users/entities/patient.entity';
import { Classification } from './classification.entity';
import { PrescriptionMedicine } from './prescriptionMedicins.entity';

@Entity()
export class Prescription {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  createdAt: Date;

  @Column({ nullable: true })
  tempCondition: string;

  @Column({ nullable: true })
  note: string;

  @Column({ nullable: true })
  isPrivate: boolean;

  @Column({ nullable: false })
  patientId: string;

  @ManyToOne(
    type => Patient,
    Patient => Patient.prescriptions,
  )
  @JoinColumn({ name: 'patientId' })
  patient: Patient;

  @Column({ nullable: false })
  doctorId: string;
  @ManyToOne(
    type => Doctor,
    Doctor => Doctor.prescriptions,
    { onDelete: 'SET NULL' },
  )
  @JoinColumn({ name: 'doctorId' })
  doctor: Doctor;

  @ManyToOne(type => Classification)
  classification: Classification;

  @ManyToOne(type => Condition)
  condition: Condition;

  @OneToMany(
    type => PrescriptionMedicine,
    pm => pm.prescription,
    { onDelete: 'CASCADE' },
  )
  prescriptionMedicine: PrescriptionMedicine[];
}
