import { Module } from '@nestjs/common';
import { PrescriptionsService } from './prescriptions.service';
import { PrescriptionsController } from './prescriptions.controller';
import { ClassificationsController } from './classifications/classifications.controller';
import { ClassificationsService } from './classifications/classifications.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Classification } from './entities/classification.entity';
import { Condition } from './entities/condition.entity';
import { MedicinsController } from './medicins/medicins.controller';
import { MedicinsService } from './medicins/medicins.service';
import { Medicine } from './entities/medicins.entity';
import { Prescription } from './entities/prescription.entity';
import { Doctor } from 'src/users/entities/doctor.entity';
import { Patient } from 'src/users/entities/patient.entity';
import { PrescriptionMedicine } from './entities/prescriptionMedicins.entity';
import { UsersModule } from 'src/users/users.module';
import { NotificationsModule } from 'src/users/notifications/notifications.module';
import { Report } from './entities/reports.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Classification,
      Doctor,
      Patient,
      Condition,
      Medicine,
      Prescription,
      PrescriptionMedicine,
      Report,
    ]),
    UsersModule,
    NotificationsModule,
  ],
  providers: [PrescriptionsService, ClassificationsService, MedicinsService],
  controllers: [
    PrescriptionsController,
    ClassificationsController,
    MedicinsController,
  ],
})
export class PrescriptionsModule {}
