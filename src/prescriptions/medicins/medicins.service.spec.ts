import { Test, TestingModule } from '@nestjs/testing';
import { MedicinsService } from './medicins.service';

describe('MedicinsService', () => {
  let service: MedicinsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MedicinsService],
    }).compile();

    service = module.get<MedicinsService>(MedicinsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
