import { Injectable } from '@nestjs/common';
import { Medicine } from '../entities/medicins.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UpdateMedicineDto } from '../Dtos/updateMedicine.dto';
import { medicins } from 'src/utils/seed/classificationsSeed';

@Injectable()
export class MedicinsService {
  constructor(
    @InjectRepository(Medicine)
    private medicinesRepository: Repository<Medicine>,
  ) {}

  async getMedicins(name: string, page: number = 0, size: number = 10) {
    return (
      this.medicinesRepository
        .createQueryBuilder('medicine')
        .where('medicine.name like :name ', {
          name: '%' + name + '%',
        })
        // .take(size)
        // .skip(page * size)
        .leftJoinAndSelect(
          'medicine.contradictingMedicins',
          'contradictingMedicins',
        )
        .getMany()
    );
  }

  createMedicine(name: string) {
    return this.medicinesRepository.save({
      name,
    });
  }

  updateMedicine(updatedMedicine: UpdateMedicineDto) {
    return this.medicinesRepository.update(updatedMedicine.id, updatedMedicine);
  }
}
