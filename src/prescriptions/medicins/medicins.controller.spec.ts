import { Test, TestingModule } from '@nestjs/testing';
import { MedicinsController } from './medicins.controller';

describe('MedicinsController', () => {
  let controller: MedicinsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MedicinsController],
    }).compile();

    controller = module.get<MedicinsController>(MedicinsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
