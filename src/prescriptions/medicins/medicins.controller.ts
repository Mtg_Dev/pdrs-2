import { Controller, Get, Query, Post, Body, Put } from '@nestjs/common';
import { ApiTags, ApiProperty, ApiQuery } from '@nestjs/swagger';
import { MedicinsService } from './medicins.service';
import { Pagination } from 'src/utils/decorators';
import { ApiPagination } from 'src/middlewares/pipes/pagination';
import { Auth } from 'src/middlewares/guards/auth.guard';
import { ROLES } from 'src/utils/constants';
import { UpdateMedicineDto } from '../Dtos/updateMedicine.dto';
import { medicins } from 'src/utils/seed/classificationsSeed';

@ApiTags('Medicins')
@Controller('api/medicins')
export class MedicinsController {
  constructor(private medicinsService: MedicinsService) {}

  @Get()
  @ApiPagination()
  async getMedicins(@Pagination() { page, size }, @Query('name') name: string) {
    return this.medicinsService.getMedicins(name, page, size);
  }

  @Post()
  @Auth(ROLES.Admin)
  addMedicine(@Body() newMedicine: { name: string }) {
    return this.medicinsService.createMedicine(newMedicine.name);
  }

  @Put()
  @Auth(ROLES.Admin)
  updateMedicine(@Body() medicine: UpdateMedicineDto) {
    return this.medicinsService.updateMedicine(medicine);
  }
}
