import { Controller, Query, Get, Post, Body, HttpCode } from '@nestjs/common';
import { PrescriptionsService } from './prescriptions.service';
import { classifications } from 'src/utils/seed/classificationsSeed';
import { ApiTags } from '@nestjs/swagger';
import { Auth } from 'src/middlewares/guards/auth.guard';
import { ROLES } from 'src/utils/constants';
import { CreatePrescriptionDTO } from './Dtos/createPrescription.dto';
import { DispensePrescriptionDto } from './Dtos/dispensePrescription.dto';
import { UserId, Pagination, PaginationQuery } from 'src/utils/decorators';
import { ApiPagination } from 'src/middlewares/pipes/pagination';

@ApiTags('Prescripitons')
@Controller('api/prescriptions')
export class PrescriptionsController {
  constructor(private prescriptionsService: PrescriptionsService) {}

  @Get()
  @ApiPagination()
  @Auth(ROLES.Admin, ROLES.Doctor)
  async getPrescriptions(
    @Query('patientId') patientId: string,
    @UserId() userId,
    @Query('classificationId') classificationId?: number,
    @Pagination() pagination?,
  ) {
    return this.prescriptionsService.getPatientPrescriptions(
      patientId,
      userId,
      classificationId,
      pagination,
    );
  }

  @Get('to-dispense')
  @ApiPagination()
  @Auth(ROLES.Pharmacy)
  async getPrescriptionsToDispense(
    @Query('patientId') patientId: string,
    @Pagination() pagination?,
  ) {
    return this.prescriptionsService.getPatientPrescriptionsToDispense(
      patientId,

      pagination,
    );
  }

  @Post()
  @Auth(ROLES.Admin, ROLES.Doctor)
  async createPrescription(
    @Body() prescriptionDto: CreatePrescriptionDTO,
    @UserId() userId: string,
  ) {
    return this.prescriptionsService.createPrescription(
      prescriptionDto,
      userId,
    );
  }

  @Post('dispense')
  @HttpCode(200)
  @Auth(ROLES.Admin, ROLES.Pharmacy)
  async DispensePrescription(
    @Body() prescriptionDto: DispensePrescriptionDto,
    @UserId() userId: string,
  ) {
    return this.prescriptionsService.dispensePrescription(
      prescriptionDto,
      userId,
    );
  }

  @Post('report')
  @HttpCode(200)
  @Auth(ROLES.Admin, ROLES.Pharmacy, ROLES.Doctor)
  async ReportPrescription(
    @Body() { prescriptionId, note }: { prescriptionId: number; note: string },
    @UserId() userId: string,
  ) {
    return this.prescriptionsService.reportPrescription(
      userId,
      prescriptionId,
      note,
    );
  }
}
