import { Injectable, BadRequestException } from '@nestjs/common';
import { Prescription } from './entities/prescription.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';
import { CreatePrescriptionDTO } from './Dtos/createPrescription.dto';
import { Doctor } from 'src/users/entities/doctor.entity';
import { Classification } from './entities/classification.entity';
import { Condition } from './entities/condition.entity';
import { Patient } from 'src/users/entities/patient.entity';
import { PrescriptionMedicine } from './entities/prescriptionMedicins.entity';
import { Medicine } from './entities/medicins.entity';
import { medicins } from 'src/utils/seed/classificationsSeed';
import { DispensePrescriptionDto } from './Dtos/dispensePrescription.dto';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { ClassificationsService } from './classifications/classifications.service';
import { NotificationsService } from 'src/users/notifications/notifications.service';
import { PaginationQuery } from 'src/utils/decorators';
import { Report } from './entities/reports.entity';

@Injectable()
export class PrescriptionsService {
  constructor(
    private userService: UsersService,
    private classificationService: ClassificationsService,
    private notificationsService: NotificationsService,

    @InjectRepository(Prescription)
    private prescriptionsRepository: Repository<Prescription>,
    @InjectRepository(Doctor)
    private doctorsRepository: Repository<Doctor>,
    @InjectRepository(Classification)
    private classificationsRepository: Repository<Classification>,
    @InjectRepository(Condition)
    private conditionsRepository: Repository<Condition>,
    @InjectRepository(Patient)
    private patientsRepository: Repository<Patient>,
    @InjectRepository(Medicine)
    private medicineRepository: Repository<Medicine>,
    @InjectRepository(PrescriptionMedicine)
    private prescriptionMedicinesRepository: Repository<PrescriptionMedicine>,
    @InjectRepository(Report)
    private reportsRepository: Repository<Report>,
  ) {}

  async getPatientPrescriptions(
    patientId: string,
    userId: string,
    classification?: number,
    pagination?: PaginationQuery,
  ) {
    const doctor = await this.userService.getDoctor(userId);

    const prescriptions = await this.prescriptionsRepository.find({
      where: {
        patientId,
        ...(classification && { classification }),
      },
      order: {
        createdAt: 'DESC',
      },
      join: {
        alias: 'prescription',
        leftJoinAndSelect: {
          classification: 'prescription.classification',
          condition: 'prescription.condition',
          prescriptionMedicine: 'prescription.prescriptionMedicine',
          medicine: 'prescriptionMedicine.medicine',
        },
      },
      skip: pagination.size * pagination.page,
      take: pagination.size,
    });
    return prescriptions.map(p => {
      if (!p.isPrivate || p.doctorId === doctor.id) return p;
      return { ...p, condition: null };
    });
  }

  async getPatientPrescriptionsToDispense(
    patientId: string,
    pagination?: PaginationQuery,
  ) {
    return this.prescriptionsRepository
      .createQueryBuilder('prescription')
      .where('prescription.patientId = :patientId', { patientId })
      .innerJoinAndSelect(
        'prescription.prescriptionMedicine',
        'prescriptionMedicine',
      )
      .innerJoinAndSelect('prescription.doctor', 'doctor')
      .andWhere('prescriptionMedicine.pharmacyId IS NULL')
      .innerJoinAndSelect('prescriptionMedicine.medicine', 'medicine')
      .take(pagination.size)
      .skip(pagination.size * pagination.page)
      .orderBy('prescription.createdAt', 'DESC')
      .getMany();
  }

  async createPrescription(
    prescription: CreatePrescriptionDTO,
    userId: string,
  ) {
    try {
      const doctor = await this.userService.getDoctor(userId);

      const classification = await this.classificationService.getClassification(
        prescription.classificationId,
      );

      if (!classification)
        throw new BadRequestException({ msg: 'Classification Error' });

      let condition = null;
      let tempCondition = null;

      if (!prescription.conditionId && !prescription.tempCondition)
        throw new BadRequestException();

      if (!prescription.conditionId) tempCondition = prescription.tempCondition;
      else
        condition = await this.classificationService.getCondition(
          prescription.conditionId,
        );

      const newPrescription = await this.prescriptionsRepository.save({
        patientId: prescription.patientId,
        classification,
        isPrivate: classification.isPrivate && prescription.isPrivate,
        doctor,
        ...(prescription.note && { note: prescription.note }),
        ...(condition && { condition }),
        ...(tempCondition && { tempCondition }),
      });

      if (tempCondition)
        this.notificationsService.addCondition(newPrescription);

      for (let {
        id,
        isBold,
        isChronic,
        count,
        tempMedicine,
      } of prescription.medicins) {
        const medicine = await this.medicineRepository.findOne({
          where: {
            id,
          },
        });
        if (medicine) tempMedicine = null;

        const createdMedicine = await this.prescriptionMedicinesRepository.save(
          {
            isBold,
            isChronic,
            count,
            ...(medicine && { medicine }),
            ...(tempMedicine && { tempMedicine }),
            prescription: newPrescription,
          },
        );

        if (tempMedicine)
          this.notificationsService.addMedicine(createdMedicine);
      }
      return newPrescription;
    } catch (error) {
      throw new BadRequestException();
    }
  }

  async dispensePrescription(
    prescription: DispensePrescriptionDto,
    userId: string,
  ) {
    try {
      const pharmacy = await this.userService.getPharmacy(userId);
      return this.prescriptionMedicinesRepository.update(
        prescription.prescriptionMedicins.map(m => m.id),
        { pharmacy },
      );
    } catch (error) {
      throw new BadRequestException();
    }
  }

  async reportPrescription(
    userId: string,
    prescriptionId: number,
    note?: string,
  ) {
    const report = await this.reportsRepository.save({
      userId,
      note,
      prescriptionId,
    });
    this.notificationsService.addReport(report);
    return report;
  }
}
