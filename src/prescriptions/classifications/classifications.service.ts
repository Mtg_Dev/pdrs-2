import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Classification } from '../entities/classification.entity';
import { Condition } from '../entities/condition.entity';
import { Repository } from 'typeorm';
import { PostConditionDTO } from '../Dtos/PostCondition.dto';
import { PostClassificationDTO } from '../Dtos/PostClassification.dto';
import fs from 'fs';
import { removeFile } from 'src/utils/helpers';
import { ROOT_DIR } from 'src/utils/constants';

@Injectable()
export class ClassificationsService {
  constructor(
    @InjectRepository(Classification)
    private classificationsRepository: Repository<Classification>,
    @InjectRepository(Condition)
    private conditionsRepository: Repository<Condition>,
  ) {}

  async getAllClassifications() {
    return this.classificationsRepository.find();
  }

  getClassification(id: number) {
    return this.classificationsRepository.findOne({
      where: {
        id,
      },
    });
  }

  getCondition(id: number) {
    return this.conditionsRepository.findOne({
      where: {
        id,
      },
    });
  }

  async getConditions(classificationId: string) {
    return this.conditionsRepository.find({
      where: {
        classificationId,
      },
    });
  }

  async postCondition(newCondition: PostConditionDTO) {
    try {
      // return await this.conditionsRepository.save({
      //   name: newCondition.name,
      //   classification: await this.classificationsRepository.findOneOrFail(
      //     newCondition.classificationId,
      //   ),
      // });
      return this.conditionsRepository.save(newCondition);
    } catch (error) {
      throw new BadRequestException();
    }
  }

  async postClassification(
    classification: PostClassificationDTO,
    image?: string,
  ) {
    try {
      if (classification.id && image) {
        const oldImage = (
          await this.classificationsRepository.findOne(classification.id)
        ).image;
        await removeFile(oldImage);
      }
      return await this.classificationsRepository.save({
        ...classification,
        ...(image && { image }),
      });
    } catch (error) {
      if (image) await removeFile(image);
      throw new BadRequestException();
    }
  }

  async deleteClassification(id: number) {
    try {
      const classification = await this.classificationsRepository.findOneOrFail(
        id,
      );
      await removeFile(classification.image);
      return this.classificationsRepository.delete({ id });
    } catch (error) {
      throw new BadRequestException();
    }
  }
}
