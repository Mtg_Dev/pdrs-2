import {
  Controller,
  Get,
  Query,
  Post,
  Body,
  UploadedFile,
  Delete,
} from '@nestjs/common';
import { ClassificationsService } from './classifications.service';
import { PostConditionDTO } from '../Dtos/PostCondition.dto';
import { ApiTags } from '@nestjs/swagger';
import { FileUpload } from 'src/utils/decorators';
import { UPLOAD_DIR } from 'src/utils/constants';
import { PostClassificationDTO } from '../Dtos/PostClassification.dto';

@ApiTags('Classifications')
@Controller('api/classifications')
export class ClassificationsController {
  constructor(private classificationsService: ClassificationsService) {}

  @Get()
  async getClassifications() {
    return this.classificationsService.getAllClassifications();
  }

  @Get('conditions')
  async getConditions(@Query('classificationId') id: string) {
    return this.classificationsService.getConditions(id);
  }

  @Post('conditions')
  async createCondition(@Body() conditionDto: PostConditionDTO) {
    return this.classificationsService.postCondition(conditionDto);
  }

  @Post()
  @FileUpload({
    fieldName: 'image',
    uploadDir: UPLOAD_DIR.Classifications,
    allowdFileTypes: ['image/png', 'image/svg'],
  })
  async createClassification(
    @Body() classificationDto: PostClassificationDTO,
    @UploadedFile() file,
  ) {
    return this.classificationsService.postClassification(
      classificationDto,
      file.path,
    );
  }

  @Delete()
  async deleteClassification(@Body() { id }: { id: number }) {
    return this.classificationsService.deleteClassification(id);
  }
}
