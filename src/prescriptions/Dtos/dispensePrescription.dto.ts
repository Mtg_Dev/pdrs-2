import {
  IsString,
  IsUUID,
  IsOptional,
  ValidateNested,
  IsNumber,
  IsBoolean,
  ValidateIf,
} from 'class-validator';

export class DispensePrescriptionDto {
  @ValidateNested()
  prescriptionMedicins: PrescriptionMedicine[];
}

class PrescriptionMedicine {
  @IsNumber()
  id: number;
}
