import {
  IsString,
  IsUUID,
  IsOptional,
  ValidateNested,
  IsNumber,
  IsBoolean,
  ValidateIf,
} from 'class-validator';

export class CreatePrescriptionDTO {
  @IsString()
  patientId: string;

  @IsNumber()
  classificationId: number;

  @IsNumber()
  @IsOptional()
  conditionId: number;

  @IsString()
  @ValidateIf(p => !p.conditionId)
  tempCondition: string;

  @ValidateNested()
  medicins: Medicine[];

  @IsString()
  @IsOptional()
  note: string;

  @IsBoolean()
  @IsOptional()
  isPrivate: boolean;
}

class Medicine {
  @IsNumber()
  id: number;

  @IsString()
  @ValidateIf(m => !m.id)
  tempMedicine: string;

  @IsBoolean()
  @IsOptional()
  isBold: boolean;

  @IsBoolean()
  @IsOptional()
  isChronic: boolean;

  @IsNumber()
  @IsOptional()
  count: number = 1;
}
