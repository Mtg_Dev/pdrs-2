import {
  IsString,
  IsNumber,
  IsNumberString,
  IsOptional,
  ValidateNested,
} from 'class-validator';

export class UpdateMedicineDto {
  @IsNumber()
  id: number;

  @IsString()
  name: string;

  @ValidateNested()
  @IsOptional()
  contradictingMedicins: { id: number }[];
}
