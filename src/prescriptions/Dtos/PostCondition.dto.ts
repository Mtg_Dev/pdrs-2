import {
  IsString,
  IsNumber,
  IsNumberString,
  IsOptional,
} from 'class-validator';
import { Transform } from 'class-transformer';

export class PostConditionDTO {
  @IsNumber()
  @IsOptional()
  @Transform(value => +value)
  id: number;

  @IsString()
  name: string;

  @IsNumber()
  @IsOptional()
  @Transform(value => +value)
  classificationId: number;
}
