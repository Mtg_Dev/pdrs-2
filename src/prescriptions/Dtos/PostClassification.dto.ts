import { IsString, IsNumber, IsOptional } from 'class-validator';
import { Transform } from 'class-transformer';

export class PostClassificationDTO {
  @IsNumber()
  @IsOptional()
  @Transform(value => +value)
  id: number;

  @IsString()
  name: string;
}
