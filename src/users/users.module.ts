import { Module } from '@nestjs/common';
import { AuthController } from './auth/auth.controller';
import { AuthService } from './auth/auth.service';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Role } from './entities/role.entity';
import { UsersController } from './users.controller';
import { Patient } from './entities/patient.entity';
import { PatientInfo } from './entities/patient_info.entity';
import { Doctor } from './entities/doctor.entity';
import { Pharmacy } from './entities/pharmacy.entity';
import { Prescription } from 'src/prescriptions/entities/prescription.entity';
import { NotificationsService } from './notifications/notifications.service';
import { NotificationsController } from './notifications/notifications.controller';
import { Notification } from './entities/notification.entity';
import { PrescriptionMedicine } from 'src/prescriptions/entities/prescriptionMedicins.entity';
import { NotificationsModule } from './notifications/notifications.module';
import { AccountRequest } from './entities/account_request.entity';
import { Message } from './entities/message.entity';
import { Country } from './entities/country.entity';
import { City } from './entities/city.entity';
import { Classification } from 'src/prescriptions/entities/classification.entity';
import { JwtModule } from '@nestjs/jwt';
import { configService } from 'src/utils/config';

@Module({
  imports: [
    JwtModule.register({
      secret: configService.getJwtSecret(),
      signOptions: { expiresIn: '6h' },
    }),
    TypeOrmModule.forFeature([
      User,
      Role,
      Patient,
      PatientInfo,
      Doctor,
      Pharmacy,
      Prescription,
      AccountRequest,
      Message,
      Country,
      City,
      Classification,
    ]),
    NotificationsModule,
  ],
  providers: [UsersService],
  exports: [UsersService],
  controllers: [UsersController],
})
export class UsersModule {}
