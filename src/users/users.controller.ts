import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Query,
  Put,
  UploadedFile,
} from '@nestjs/common';
import { Auth } from 'src/middlewares/guards/auth.guard';
import { ROLES, UPLOAD_DIR } from 'src/utils/constants';
import { ApiTags } from '@nestjs/swagger';
import { UsersService } from './users.service';
import { CreatePatientDTO } from './Dtos/createPatient.dto';
import {
  UserId,
  Pagination,
  User as UserDecortaor,
  FileUpload,
} from 'src/utils/decorators';
import { RequestAccountDTO } from './Dtos/requestAccount.dto';
import { MessageDTO } from './Dtos/message.dto';
import { ApiPagination } from 'src/middlewares/pipes/pagination';
import { CreateUserDto } from './Dtos/createUser.dto';
import { UpdateAccountDTO } from './Dtos/updateAccount.dto';
import { UserJwt } from './auth/auth.service';
import { ConfirmAccountDTO } from './Dtos/confirmAccount.dto';
import { UpdatePatientDTO } from './Dtos/updatePatient.dto';

import { MailingService } from 'src/services/mail.service';
import { User } from './entities/user.entity';

@ApiTags('Users')
@Controller('api/users')
export class UsersController {
  constructor(private userService: UsersService) {}

  @Get()
  @Auth(ROLES.Admin)
  @ApiPagination()
  async getUsers(@Pagination() { page, size }) {
    return await this.userService.getUsers(page, size);
  }

  @Get('doctors')
  @ApiPagination()
  async getDoctors(
    @Pagination() { page, size },
    @Query('query') query: string,
    @Query('countryId') countryId: number,
    @Query('cityId') cityId: number,
    @Query('classificationIds') classificationIds: number[],
  ) {
    return await this.userService.getDoctors({
      page,
      size,
      query,
      countryId,
      cityId,
      classificationIds,
    });
  }

  @Get('profile')
  @Auth()
  @ApiPagination()
  async getProfile(@UserDecortaor() user: UserJwt, @Query('id') id: string) {
    if (!id) return this.userService.getUserInfo(user.id, user.role);
    const role = await this.userService.getUserRole(id);
    return this.userService.getUserInfo(id, role.name);
  }

  @Get('patient/:id')
  @Auth(ROLES.Admin, ROLES.Doctor)
  async getPatient(@Param('id') id: string) {
    return await this.userService.getPatinet(id);
  }

  @Post()
  @Auth(ROLES.Admin)
  async newUser(@Body() createUserDto: CreateUserDto) {
    return this.userService.createUser(createUserDto);
  }

  @FileUpload({
    fieldName: 'image',
    uploadDir: UPLOAD_DIR.Profiles,
    allowdFileTypes: ['image/*'],
  })
  @Post('update-photo')
  @Auth(ROLES.Doctor)
  async updatePhoto(@UploadedFile() file, @UserId() id: string) {
    return this.userService.updatePhoto(id, file.path);
  }

  @Put()
  @Auth()
  async updateAccountInfo(
    @UserDecortaor() user: UserJwt,
    @Body() updateAccountDTO: UpdateAccountDTO,
  ) {
    return this.userService.updateAccount(user, updateAccountDTO);
  }

  @Put('confirm-account')
  @Auth()
  async confirmAccountInfo(
    @UserDecortaor() user: UserJwt,
    @Body() confimrAccountDTO: ConfirmAccountDTO,
  ) {
    return this.userService.updateAccount(user, confimrAccountDTO);
  }

  @Post('patient')
  @Auth(ROLES.Admin, ROLES.Doctor)
  async newPatient(
    @UserId() userId,
    @Body() createPatientDTO: CreatePatientDTO,
  ) {
    return this.userService.savePatient(userId, createPatientDTO);
  }

  @Put('patient')
  @Auth(ROLES.Admin, ROLES.Doctor)
  async updatePatient(
    @UserId() userId,
    @Body() updatePatientDTO: UpdatePatientDTO,
  ) {
    return this.userService.savePatient(userId, updatePatientDTO);
  }

  @Get('doctor-patients')
  @Auth(ROLES.Doctor)
  async getDoctorPatients(@UserId() id: string) {
    return await this.userService.getDoctorsPatients(id);
  }

  @Get('patients')
  @Auth(ROLES.Doctor, ROLES.Pharmacy)
  async getPatients(@Query('id') id: string, @Query('name') name: string) {
    return this.userService.getPatients(id, name, true);
  }

  @Post('request-account')
  requestAccount(@Body() request: RequestAccountDTO) {
    return this.userService.requestAccount(request);
  }

  @Post('toggle-block')
  @Auth(ROLES.Admin)
  toggleBlock(@Body() user: { id: string }) {
    return this.userService.toggleBlock(user.id);
  }

  @Post('send-message')
  @Auth(ROLES.Doctor, ROLES.Pharmacy)
  sendMessage(@Body() message: MessageDTO, @UserId() userId: string) {
    return this.userService.sendMessage(message, userId);
  }

  @Post('respond-message')
  @Auth(ROLES.Admin)
  respondMessage(@Body() message: MessageDTO) {
    return this.userService.respondMessage(message);
  }

  @Get('countries')
  @Auth()
  async getCountries() {
    return await this.userService.getCountries();
  }

  @Get('cities')
  @Auth()
  async getCities(@Query('countryId') id: number) {
    return await this.userService.getCities(id);
  }

  @Get('request-reset-password')
  requestResetPassword(@Query('email') email: string) {
    return this.userService.requestResetPassword(email);
  }

  @Post('reset-password')
  resetPassword(
    @Body() { token, newPassword }: { token: string; newPassword: string },
  ) {
    return this.userService.resetPassword(token, newPassword);
  }
}
