import {
  IsString,
  Length,
  IsEmail,
  IsIn,
  IsDate,
  IsOptional,
  IsNumber,
  IsDateString,
  ValidateNested,
  ValidateIf,
} from 'class-validator';

class DoctorDto {
  @IsString()
  name: string;
  @IsString()
  @IsOptional()
  image: string;
  @IsEmail()
  email: string;
  @IsString()
  phone: string;
}

class PharmacyDto {
  @IsString()
  name: string;
}

export class CreateUserDto {
  @IsEmail()
  email: string;

  @IsString()
  @IsOptional()
  password: string;

  @ValidateNested()
  @ValidateIf(u => !u.pharmacy)
  doctor: DoctorDto;

  @ValidateNested()
  @ValidateIf(u => !u.doctor)
  pharmacy: PharmacyDto;
}
