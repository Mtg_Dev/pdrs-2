import {
  IsString,
  Length,
  IsEmail,
  IsIn,
  IsDate,
  IsOptional,
  IsNumber,
  IsDateString,
} from 'class-validator';
import { GENDER } from 'src/utils/constants';

export class CreatePatientDTO {
  @IsString()
  ID: string;

  @IsString()
  name: string;

  @IsIn(Object.values(GENDER))
  sex: string;

  @IsDateString()
  birthDate: Date;

  @IsString()
  @IsOptional()
  bloodType: string;

  @IsNumber()
  @IsOptional()
  height: number;

  @IsNumber()
  @IsOptional()
  weight: number;

  @IsNumber()
  @IsOptional()
  sugarLevel: number;
}
