import {
  IsString,
  Length,
  IsEmail,
  IsIn,
  IsDate,
  IsOptional,
  IsNumber,
  IsDateString,
  ValidateNested,
  ValidateIf,
  MinLength,
  IsNotEmpty,
  ArrayMinSize,
  ArrayMaxSize,
} from 'class-validator';

export class UpdateAccountDTO {
  @IsString()
  @IsOptional()
  @IsNotEmpty()
  @MinLength(8)
  password: string;

  @IsString()
  @IsOptional()
  @MinLength(5)
  name: string;

  @IsString()
  @IsOptional()
  phone: string;

  @IsString()
  @IsOptional()
  address: string;

  @IsString()
  @IsOptional()
  @IsNotEmpty()
  specification: string;

  @IsString()
  @IsOptional()
  summary: string;

  @IsNumber()
  @IsOptional()
  cityId: number;

  @IsOptional()
  @IsNumber({}, { each: true })
  @ArrayMinSize(1)
  @ArrayMaxSize(3)
  classifications: number[];
}
