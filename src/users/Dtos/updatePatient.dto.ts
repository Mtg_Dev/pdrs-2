import {
  IsString,
  Length,
  IsEmail,
  IsIn,
  IsDate,
  IsOptional,
  IsNumber,
  IsDateString,
} from 'class-validator';
import { GENDER } from 'src/utils/constants';

export class UpdatePatientDTO {
  @IsString()
  ID: string;

  @IsString()
  @IsOptional()
  name: string;

  @IsIn(Object.values(GENDER))
  @IsOptional()
  sex: string;

  @IsDateString()
  @IsOptional()
  birthDate: Date;

  @IsString()
  @IsOptional()
  bloodType: string;

  @IsNumber()
  @IsOptional()
  height: number;

  @IsNumber()
  @IsOptional()
  weight: number;

  @IsNumber()
  @IsOptional()
  sugarLevel: number;
}
