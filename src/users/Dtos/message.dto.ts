import { IsString, IsEmail, IsOptional } from 'class-validator';

export class MessageDTO {
  @IsEmail()
  @IsOptional()
  to: string;

  @IsString()
  content: string;
}
