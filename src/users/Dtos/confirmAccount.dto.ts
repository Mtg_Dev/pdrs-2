import {
  IsString,
  Length,
  IsEmail,
  IsIn,
  IsDate,
  IsOptional,
  IsNumber,
  IsDateString,
  ValidateNested,
  ValidateIf,
  MinLength,
  IsNotEmpty,
  ArrayMinSize,
  ArrayMaxSize,
} from 'class-validator';

export class ConfirmAccountDTO {
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  password: string;

  @IsString()
  @MinLength(5)
  name: string;

  @IsString()
  phone: string;

  @IsString()
  @IsOptional()
  address: string;

  @IsString()
  @IsNotEmpty()
  specification: string;

  @IsString()
  @IsOptional()
  summary: string;

  @IsNumber()
  cityId: number;

  @IsNumber({}, { each: true })
  @ArrayMinSize(1)
  @ArrayMaxSize(3)
  classifications: number[];
}
