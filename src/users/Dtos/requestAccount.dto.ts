import { IsString, IsEmail } from 'class-validator';

export class RequestAccountDTO {
  @IsString()
  name: string;

  @IsEmail()
  email: string;

  @IsString()
  phone: string;

  @IsString()
  specification: string;

  @IsString()
  address: string;
}
