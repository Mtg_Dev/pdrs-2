import {
  Injectable,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository, In } from 'typeorm';
import { Role } from './entities/role.entity';
import { Patient } from './entities/patient.entity';
import { Doctor } from './entities/doctor.entity';
import { Pharmacy } from './entities/pharmacy.entity';
import { PatientInfo } from './entities/patient_info.entity';
import { CreatePatientDTO } from './Dtos/createPatient.dto';
import { ROLES, TOKENS_TYPES } from 'src/utils/constants';
import { getRandomRange, autoMap, removeFile } from 'src/utils/helpers';
import { Prescription } from 'src/prescriptions/entities/prescription.entity';
import * as argon from 'argon2';
import { RequestAccountDTO } from './Dtos/requestAccount.dto';
import { NotificationsService } from './notifications/notifications.service';
import { AccountRequest } from './entities/account_request.entity';
import { MessageDTO } from './Dtos/message.dto';
import { Message } from './entities/message.entity';
import { CreateUserDto } from './Dtos/createUser.dto';
import { Country } from './entities/country.entity';
import { City } from './entities/city.entity';
import { UpdateAccountDTO } from './Dtos/updateAccount.dto';
import { UserJwt } from './auth/auth.service';
import { Classification } from 'src/prescriptions/entities/classification.entity';
import { MailingService } from 'src/services/mail.service';
import { create } from 'domain';
import { JwtService } from '@nestjs/jwt';
import { assert } from 'console';

@Injectable()
export class UsersService {
  /**
   *
   */
  constructor(
    private notificationsService: NotificationsService,
    private jwtService: JwtService,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(Role)
    private rolesRepository: Repository<Role>,
    @InjectRepository(Patient)
    private patientsRepository: Repository<Patient>,
    @InjectRepository(PatientInfo)
    private patientInfosRepository: Repository<PatientInfo>,
    @InjectRepository(Doctor)
    private doctorsRepository: Repository<Doctor>,
    @InjectRepository(Pharmacy)
    private pharmacysRepository: Repository<Pharmacy>,
    @InjectRepository(Prescription)
    private prescriptionsRepository: Repository<Prescription>,
    @InjectRepository(AccountRequest)
    private accountRequestsRepository: Repository<AccountRequest>,
    @InjectRepository(Message)
    private messagesRepository: Repository<Message>,
    @InjectRepository(Country)
    private countriesRepository: Repository<Country>,
    @InjectRepository(City)
    private citiesRepository: Repository<City>,
    @InjectRepository(Classification)
    private classificationRepository: Repository<Classification>,
  ) {}

  findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  findOne(id: string, relations: string[] = []): Promise<User> {
    return this.usersRepository.findOne(id, { relations });
  }

  findByEmail(email: string): Promise<User> {
    return this.usersRepository.findOne({
      where: {
        email,
      },
      select: ['id', 'email', 'password'],
      relations: ['role'],
    });
  }

  async saveRefreshToken(userId: string, refreshToken: string) {
    return this.usersRepository.update(userId, {
      refreshToken: await argon.hash(refreshToken),
    });
  }

  async validateUserRefreshToken(refreshToken: string, userId: string) {
    const user = await this.findOne(userId);

    if (
      !user.isBlocked &&
      (await argon.verify(user.refreshToken, refreshToken))
    )
      return user;
  }

  async getUserRole(id: string): Promise<Role> {
    try {
      return (
        await this.usersRepository.findOne({
          where: {
            id,
          },
          relations: ['role'],
        })
      ).role;
    } catch (error) {
      throw new BadRequestException();
    }
  }

  getUserInfo(id: string, role: string): Promise<User> {
    let relations = [];
    if (role === ROLES.Doctor) {
      return this.usersRepository
        .createQueryBuilder('user')
        .where('user.id = :id', { id })
        .leftJoinAndSelect('user.doctor', 'doctor')
        .leftJoinAndSelect('doctor.classifications', 'user.classifications')
        .leftJoinAndMapOne(
          'doctor.city',
          'city',
          'city',
          'city.id=doctor.cityId',
        )
        .leftJoinAndMapOne(
          'doctor.country',
          'country',
          'country',
          'country.id=city.countryId',
        )
        .getOne();
    } else if (role === ROLES.Pharmacy) relations = [ROLES.Pharmacy];
    else if (role === ROLES.User) relations = [ROLES.User];
    return this.usersRepository.findOne({
      where: { id },
      relations,
    });
  }

  getUsers(page, size) {
    return this.usersRepository.find({
      relations: ['doctor', 'pharmacy', 'patient', 'role'],
      take: size,
      skip: page * size,
    });
  }

  async getDoctors({
    page,
    size,
    query = '',
    countryId,
    cityId,
    classificationIds,
  }: {
    page: number;
    size: number;
    query?: string;
    countryId?: number;
    cityId?: number;
    classificationIds?: number[];
  }) {
    classificationIds = classificationIds?.map(c => +c) || null;

    let sqlQuery = this.doctorsRepository.createQueryBuilder('doctor');
    if (classificationIds)
      sqlQuery.innerJoin(
        'doctor.classifications',
        'classification',
        'classification.id IN (:...classificationIds)',
        { classificationIds },
      );
    if (query)
      sqlQuery.andWhere('doctor.name like :name', { name: `%${query}%` });
    if (countryId) {
      sqlQuery.innerJoin('doctor.city', 'city');
      sqlQuery.innerJoin('city.country', 'country', 'country.id=:countryId', {
        countryId,
      });
    }
    if (cityId) sqlQuery.andWhere('doctor.city = :cityId', { cityId });
    return sqlQuery
      .take(size)
      .skip(size * page)
      .getMany();
  }

  async createUser(userDto: CreateUserDto) {
    const exist = await this.usersRepository.findOne({ email: userDto.email });
    if (exist) throw new BadRequestException();

    const password = `password${getRandomRange(111, 999)}`;
    const role = await this.rolesRepository.findOne({
      name: userDto.doctor ? 'doctor' : 'pharmacy',
    });

    const createdUser = await this.usersRepository.save({
      email: userDto.email,
      password: await argon.hash(password),
      role,
    });
    // Send the user email and password to his email

    if (userDto.doctor) {
      const doctor = await this.doctorsRepository.save({
        email: userDto.email,
        name: userDto.doctor.name,
        phone: userDto.doctor.phone,
        user: createdUser,
      });

      await MailingService.sendCreateAccountEmail(
        createdUser,
        password,
        doctor.name,
      );
      delete doctor.user;
      delete createdUser.password;
      return {
        ...createdUser,
        doctor,
      };
    } else if (userDto.pharmacy) {
      const pharmacy = await this.pharmacysRepository.save({
        name: userDto.pharmacy.name,
        user: createdUser,
      });

      await MailingService.sendCreateAccountEmail(
        createdUser,
        password,
        pharmacy.name,
      );

      delete pharmacy.user;
      delete createdUser.password;
      return {
        ...createdUser,
        pharmacy,
      };
    }
  }

  async updatePhoto(id: string, image: string) {
    const doctor = await this.getDoctor(id);
    await removeFile(doctor.image);
    doctor.image = image;
    return this.doctorsRepository.save(doctor);
  }

  async updateAccount(user: UserJwt, updateAccountDto: UpdateAccountDTO) {
    try {
      if (updateAccountDto.password)
        await this.usersRepository.update(user.id, {
          password: await argon.hash(updateAccountDto.password),
        });

      if (user.role === ROLES.Doctor) {
        // If Does Not Exsit, Create it
        let doctor = await this.doctorsRepository.findOne({
          where: {
            userId: user.id,
          },
        });

        //Update Doctor's Classifications
        if (updateAccountDto.classifications.length > 0) {
          const doctorClassifications = await this.classificationRepository.find(
            {
              where: {
                id: In(updateAccountDto.classifications),
              },
            },
          );

          doctor.classifications = doctorClassifications;
          await this.doctorsRepository.save(doctor);
        }

        if (!doctor)
          //Create Doctor if not exist
          return await this.doctorsRepository.save({
            ...autoMap(
              ['name', 'address', 'phone', 'specification', 'summary', 'email'],
              updateAccountDto,
            ),
            userId: user.id,
          });
        else {
          const updateObject = autoMap(
            ['name', 'address', 'phone', 'specification', 'summary', 'email'],
            updateAccountDto,
          );
          if (updateObject)
            await this.doctorsRepository
              .createQueryBuilder()
              .update(Doctor)
              .set(updateObject)
              .where('doctor.userId=:id', { id: user.id })
              .execute();
        }
      } else if (user.role === ROLES.Pharmacy) {
        await this.pharmacysRepository
          .createQueryBuilder()
          .update(Pharmacy)
          .set(autoMap(['name'], updateAccountDto))
          .where('pharmacy.userId=:id', { id: user.id })
          .execute();
      }
      return this.getUserInfo(user.id, user.role);
    } catch (error) {
      throw new BadRequestException();
    }
  }

  async savePatient(userId: string, createPatientDto: CreatePatientDTO) {
    // Get The Patient
    const patientExist = await this.patientsRepository.findOne({
      where: { id: createPatientDto.ID },
      relations: ['patientInfo'],
    });

    // Get The Doctor
    const doctor = await this.doctorsRepository.findOne({
      where: {
        userId,
      },
    });

    // Update Patient
    if (patientExist) {
      const updatePatientObject = autoMap(
        ['name', 'sex', 'birthDate'],
        createPatientDto,
      );
      const udpatePatientInfoObject = autoMap(
        ['height', 'weight', 'sugarLevel', 'bloodType'],
        createPatientDto,
      );
      if (updatePatientObject)
        await this.patientsRepository.update(
          patientExist.id,
          updatePatientObject,
        );
      if (udpatePatientInfoObject)
        await this.patientInfosRepository.update(
          patientExist.patientInfo.id,
          udpatePatientInfoObject,
        );
      return patientExist;
    }

    const newPatient = await this.patientsRepository.save({
      id: createPatientDto.ID,
      ...createPatientDto,
    });

    // create the patient info
    await this.patientInfosRepository.save({
      ...createPatientDto,
      doctor,
      patient: newPatient,
    });

    return await this.patientsRepository.findOne(createPatientDto.ID);
  }

  async getPatinet(id: string) {
    try {
      return await this.patientsRepository.findOneOrFail({
        where: {
          id: id,
        },

        relations: ['patientInfo'],
      });
    } catch (error) {
      throw new NotFoundException();
    }
  }

  async getPatinetById(id: string) {
    return this.patientsRepository.findOne({
      where: {
        id,
      },

      relations: ['patientInfo'],
    });
  }

  async getDoctor(id: string) {
    return this.doctorsRepository.findOne({
      where: {
        userId: id,
      },
    });
  }

  async getDoctorsPatients(id: string) {
    // get prescriptions with this doctor id and with unique patients ids then extraxt the patients names and ids then return it
    const doctorId = (await this.getDoctor(id)).id;
    return this.prescriptionsRepository
      .createQueryBuilder('prescription')
      .where('doctorId = :doctorId', { doctorId })
      .innerJoinAndMapOne(
        'prescription.patient',
        'patient',
        'patient',
        'patient.id=prescription.patientId',
      )
      .select(['patient'])
      .distinct(true)
      .getRawMany();
  }

  async getPatients(id: string, name: string, encode: boolean = false) {
    if ((!name || name.trim().length <= 1) && (!id || id.trim().length <= 6))
      return [];

    return this.patientsRepository
      .createQueryBuilder('patients')
      .where('id like :id', { id: `${id}%` })
      .orWhere('name like :name', { name: `%${name}%` })
      .getMany()
      .then(patients =>
        patients.map(p => ({ ...p, id: `${p.id.slice(0, -5)}*****` })),
      );
  }

  async getPharmacy(id: string) {
    return this.pharmacysRepository.findOne({
      where: {
        user: id,
      },
    });
  }

  async requestAccount(requestDto: RequestAccountDTO) {
    const request = await this.accountRequestsRepository.save({
      ...requestDto,
    });
    this.notificationsService.addRequest(request);
    await MailingService.newRequestToAdmin(requestDto.name);
    return request;
  }

  async sendMessage(message: MessageDTO, userId: string) {
    const doctor = await this.getDoctor(userId);
    const createdMessage = await this.messagesRepository.save({
      content: message.content,
      doctor,
    });
    this.notificationsService.addMessage(createdMessage);
    await MailingService.newMessageToAdmin(doctor.name);
    return createdMessage;
  }

  async respondMessage(message: MessageDTO) {
    await MailingService.responseMessage(message.content, message.to);
  }

  async toggleBlock(id: string) {
    const user = await this.usersRepository.findOne({ id });
    user.isBlocked = !user.isBlocked;
    return this.usersRepository.save(user);
  }

  async requestResetPassword(email: string) {
    // generate a special token
    const user = await this.findByEmail(email);
    if (!user) throw new BadRequestException("email doesn't exist");
    const token = this.jwtService.sign({
      email,
      type: TOKENS_TYPES.ResetPassword,
    });
    await MailingService.sendResetPassword(email, token);
  }

  async resetPassword(token: string, newPassword: string) {
    try {
      const result = await this.jwtService.verifyAsync(token);
      assert(result.type === TOKENS_TYPES.ResetPassword);
      const user = await this.findByEmail(result.email);
      return await this.usersRepository.update(user.id, {
        password: await argon.hash(newPassword),
      });
    } catch (error) {
      throw new BadRequestException('token invalid');
    }
  }

  async getCountries() {
    return this.countriesRepository.find({});
  }

  async getCities(id: number) {
    return this.citiesRepository.find({
      where: {
        country: id,
      },
    });
  }
}
