import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users.service';
import * as argon2 from 'argon2';
import { User } from '../entities/user.entity';

export interface UserJwt {
  id: string;
  role: string;
}

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<User | null> {
    const user = await this.usersService.findByEmail(email);

    if (user && (await argon2.verify(user.password, password))) {
      let result = await this.usersService.getUserInfo(user.id, user.role.name);

      result.role = user.role;
      return result;
    }
    return null;
  }

  async login(user: any) {
    const payload: UserJwt = { id: user.id, role: user.role.name };
    return {
      accessToken: this.jwtService.sign(payload, {
        expiresIn: '15m',
      }),
      refreshToken: this.jwtService.sign(
        { id: user.id },
        {
          expiresIn: '15d',
        },
      ),
    };
  }
}
