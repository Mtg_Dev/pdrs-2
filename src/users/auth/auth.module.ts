import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth.controller';
import { JwtStrategy } from './jwt-strategy';
import { UsersModule } from '../users.module';
import { configService } from 'src/utils/config';
import { JwtRefreshTokenStrategy } from './jwt-refresh-strategy';

@Module({
  imports: [
    UsersModule,
    JwtModule.register({
      secret: configService.getJwtSecret(),
      signOptions: { expiresIn: '15d' },
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, JwtRefreshTokenStrategy],
  exports: [AuthService, JwtStrategy, JwtRefreshTokenStrategy],
})
export class AuthModule {}
