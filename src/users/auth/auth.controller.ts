import {
  Controller,
  Get,
  Post,
  Body,
  Res,
  HttpCode,
  BadRequestException,
  UseGuards,
  Req,
} from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';
import { UsersService } from '../users.service';
import { AuthService } from './auth.service';
import { LoginUserDto } from '../Dtos/loginUser.dto';
import JwtRefreshGuard from 'src/middlewares/guards/refresh.guard';
import { UserId } from 'src/utils/decorators';

@ApiTags('Auth')
@Controller('api/auth')
export class AuthController {
  /**
   *
   */
  constructor(
    private userService: UsersService,
    private authService: AuthService,
  ) {}

  // @Post('signup')
  // async signup(@Body() user: CreateUserDto) {
  //   return await this.userService.createUser(user);
  // }

  @Post('login')
  @HttpCode(200)
  async login(@Res() response: any, @Body() user: LoginUserDto) {
    const userData = await this.authService.validateUser(
      user.email,
      user.password,
    );

    if (!userData) throw new BadRequestException();
    const { accessToken, refreshToken } = await this.authService.login(
      userData,
    );

    await this.userService.saveRefreshToken(userData.id, refreshToken);

    response.cookie('accessToken', accessToken);
    response.cookie('refreshToken', refreshToken);

    return response.send({
      accessToken,
      user: userData,
    });
  }

  @UseGuards(JwtRefreshGuard)
  @Get('refresh')
  async refresh(@Res() response: any, @UserId() userId) {
    const user = await this.userService.findOne(userId, ['role']);
    const { accessToken, refreshToken } = await this.authService.login(user);

    await this.userService.saveRefreshToken(user.id, refreshToken);

    response.cookie('accessToken', accessToken); // Using express res object.
    response.cookie('refreshToken', refreshToken);
    return response.send({
      accessToken,
    });
  }

  @Post('logout')
  @HttpCode(200)
  async logout(@Res() response: any) {
    response.clearCookie('accessToken');
    return response.send({
      msg: 'Logged Out Successfully',
    });
  }
}
