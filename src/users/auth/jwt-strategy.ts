import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Request } from 'express';
import { use } from 'passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { configService } from 'src/utils/config';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (request: Request) => {
          return request?.cookies?.accessToken;
        },
      ]),
      ignoreExpiration: false,
      secretOrKey: configService.getJwtSecret(),
    });
  }

  // Passport first verifies the JWT's signature and decodes the JSON. It then
  // invokes our validate() method passing the decoded JSON as its single parameter
  validate(payload: any) {
    return { id: payload.id, role: payload.role };
  }
}
