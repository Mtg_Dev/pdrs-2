import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  JoinColumn,
  PrimaryColumn,
} from 'typeorm';
import { User } from './user.entity';
import { PatientInfo } from './patient_info.entity';
import { Prescription } from '../../prescriptions/entities/prescription.entity';

@Entity()
export class Patient {
  @PrimaryColumn()
  id: string;

  @Column()
  name: string;

  @Column()
  sex: string;

  @Column()
  birthDate: Date;

  @OneToOne(type => PatientInfo, { onDelete: 'CASCADE' })
  @JoinColumn()
  patientInfo: PatientInfo;

  @OneToOne(type => User, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  user: User;

  @OneToMany(
    type => Prescription,
    p => p.patient,
  )
  prescriptions: Prescription[];
}
