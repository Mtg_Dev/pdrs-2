import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  JoinColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Role } from './role.entity';
import { User } from './user.entity';
import { Classification } from '../../prescriptions/entities/classification.entity';
import { Prescription } from '../../prescriptions/entities/prescription.entity';
import { City } from './city.entity';

@Entity()
export class Doctor {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column({ nullable: true })
  image: string;

  @Column({ nullable: true })
  email: string;

  @Column({ nullable: true })
  phone: string;

  @Column({ nullable: true })
  address: string;

  @Column({ nullable: true })
  specification: string;

  @Column({ nullable: true })
  summary: string;

  @Column({ nullable: false })
  userId: string;

  @OneToOne(type => User, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'userId' })
  user: User;

  @ManyToMany(
    type => Classification,
    c => c.doctors,
    {
      nullable: true,
      onDelete: 'CASCADE',
    },
  )
  @JoinTable({ name: 'doctors_classifications' })
  classifications: Classification[];

  @OneToMany(
    type => Prescription,
    p => p.doctor,
  )
  prescriptions: Prescription[];

  @ManyToOne(type => City)
  city: City;
}
