import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  JoinColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from './user.entity';
import { Patient } from './patient.entity';
import { Doctor } from './doctor.entity';

@Entity()
export class PatientInfo {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: true })
  bloodType: string;

  @Column({ nullable: true })
  height: number;

  @Column({ nullable: true })
  weight: number;

  @Column({ nullable: true })
  sugarLevel: number;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @ManyToOne(type => Doctor, { onDelete: 'SET NULL' })
  @JoinColumn()
  doctor: Doctor;
}
