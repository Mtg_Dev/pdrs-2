import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';
import { Role } from './role.entity';
import { Doctor } from './doctor.entity';
import { Pharmacy } from './pharmacy.entity';
import { Patient } from './patient.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: true, unique: true })
  email: string;

  @Column({ nullable: true, select: false })
  password: string;

  @Column({ nullable: true, default: false })
  isBlocked: boolean;

  @Column({ nullable: true })
  refreshToken: string;

  @ManyToOne(
    type => Role,
    role => role.users,
  )
  role: Role;

  @OneToOne(
    type => Doctor,
    doctor => doctor.user,
  )
  doctor: Doctor;

  @OneToOne(
    type => Pharmacy,
    pharmacy => pharmacy.user,
  )
  pharmacy: Pharmacy;

  @OneToOne(
    type => Patient,
    patient => patient.user,
  )
  patient: Patient;

  public static of(params: Partial<User>): User {
    const entity = new User();
    Object.assign(entity, params);
    return entity;
  }

  // @BeforeInsert()
  // async setPassword(password: string) {
  //   const salt = await bcrypt.genSalt()
  //   this.password = await bcrypt.hash(password || this.password, salt)
  // }
}
