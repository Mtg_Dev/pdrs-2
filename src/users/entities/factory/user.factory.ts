import { define } from 'typeorm-seeding';
import { User } from '../user.entity';
import Faker from 'faker';

define(User, (faker: typeof Faker, context: any) => {
  let user = new User();
  user.email = context.email;
  user.password = context.password;
  user.role = context.role;
  return user;
});
