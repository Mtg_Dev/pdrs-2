import { Injectable } from '@nestjs/common';
import { PrescriptionMedicine } from 'src/prescriptions/entities/prescriptionMedicins.entity';
import { Notification } from '../entities/notification.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { NOTIFICATIONS_TYPES } from 'src/utils/constants';
import { Prescription } from 'src/prescriptions/entities/prescription.entity';
import { AccountRequest } from '../entities/account_request.entity';
import { Message } from '../entities/message.entity';
import { classifications } from 'src/utils/seed/classificationsSeed';
import { Report } from 'src/prescriptions/entities/reports.entity';
import { MailingService } from 'src/services/mail.service';

@Injectable()
export class NotificationsService {
  /**
   *
   */
  constructor(
    @InjectRepository(Notification)
    private NotificationsRepository: Repository<Notification>,
    @InjectRepository(Prescription)
    private PrescriptionsRepository: Repository<Prescription>,
    @InjectRepository(PrescriptionMedicine)
    private PrescriptionMedicinesRepository: Repository<PrescriptionMedicine>,
    @InjectRepository(AccountRequest)
    private AccountRequestsRepository: Repository<AccountRequest>,
    @InjectRepository(Message)
    private MessagesRepository: Repository<Message>,
    @InjectRepository(Report)
    private ReportsRepository: Repository<Report>,
  ) {}

  getNotificationsTypes() {
    return NOTIFICATIONS_TYPES;
  }

  markRead(id: number) {
    return this.NotificationsRepository.update(id, { isRead: true });
  }

  async addMedicine(medicine: PrescriptionMedicine) {
    this.NotificationsRepository.save({
      tableId: medicine.id.toString(),
      type: NOTIFICATIONS_TYPES.NewMedicine,
    });
  }

  async addCondition(prescription: Prescription) {
    this.NotificationsRepository.save({
      tableId: prescription.id.toString(),
      type: NOTIFICATIONS_TYPES.NewCondition,
    });
  }

  async addRequest(request: AccountRequest) {
    this.NotificationsRepository.save({
      tableId: request.id.toString(),
      type: NOTIFICATIONS_TYPES.NewRequest,
    });
  }

  async addReport(report: Report) {
    this.NotificationsRepository.save({
      tableId: report.id.toString(),
      type: NOTIFICATIONS_TYPES.NewReport,
    });
  }

  async addMessage(message: Message) {
    this.NotificationsRepository.save({
      tableId: message.id.toString(),
      type: NOTIFICATIONS_TYPES.NewMessage,
    });
  }

  async getNotifications() {
    const newNotifications = await this.NotificationsRepository.find({
      where: {
        isRead: false,
      },
    });

    let result = [];
    let i = 0;
    for (const notification of newNotifications) {
      result.push({
        id: notification.id,
        date: notification.date,
        type: notification.type,
      });
      if (notification.type === NOTIFICATIONS_TYPES.NewCondition) {
        // Get The New Condition
        const prescription = await this.PrescriptionsRepository.findOne({
          where: {
            id: notification.tableId,
          },

          relations: ['classification', 'doctor'],
        });

        result[i].condition = prescription.tempCondition;
        result[i].classification = prescription.classification;
        result[i].doctor = prescription.doctor;
      } else if (notification.type === NOTIFICATIONS_TYPES.NewMedicine) {
        const prescriptionMedicine = await this.PrescriptionMedicinesRepository.findOne(
          {
            where: {
              id: notification.tableId,
            },
            relations: ['prescription', 'prescription.doctor'],
          },
        );

        result[i].medicine = prescriptionMedicine.tempMedicine;
        result[i].doctor = prescriptionMedicine.prescription.doctor;
      } else if (notification.type === NOTIFICATIONS_TYPES.NewRequest) {
        const accountRequest = await this.AccountRequestsRepository.findOne({
          where: {
            id: notification.tableId,
          },
        });

        result[i].request = accountRequest;
      } else if (notification.type === NOTIFICATIONS_TYPES.NewMessage) {
        const accountRequest = await this.MessagesRepository.findOne({
          where: {
            id: notification.tableId,
          },
          relations: ['doctor'],
        });

        result[i].message = accountRequest;
      } else if (notification.type === NOTIFICATIONS_TYPES.NewReport) {
        // const prescription = await this.PrescriptionsRepository.findOne({
        //   where: {
        //     id: notification.tableId,
        //   },
        //   relations: ['prescriptionMedicine'],
        // });

        result[i].report = await this.ReportsRepository.createQueryBuilder(
          'report',
        )
          .where('report.id=:id', { id: notification.tableId })
          .innerJoinAndSelect('report.user', 'user')
          .innerJoinAndSelect('report.prescription', 'prescription')
          .innerJoinAndSelect(
            'prescription.prescriptionMedicine',
            'prescriptionMedicine',
          )
          .innerJoinAndSelect('prescriptionMedicine.medicine', 'medicine')
          .getMany();
      }

      i++;
    }

    return result;
  }
}
