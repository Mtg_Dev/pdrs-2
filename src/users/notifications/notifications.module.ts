import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Prescription } from 'src/prescriptions/entities/prescription.entity';
import { NotificationsService } from './notifications.service';
import { NotificationsController } from './notifications.controller';
import { Notification } from '../entities/notification.entity';
import { PrescriptionMedicine } from 'src/prescriptions/entities/prescriptionMedicins.entity';
import { AccountRequest } from '../entities/account_request.entity';
import { Message } from '../entities/message.entity';
import { Report } from 'src/prescriptions/entities/reports.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Prescription,
      Notification,
      Prescription,
      PrescriptionMedicine,
      AccountRequest,
      Message,
      Report,
    ]),
  ],
  providers: [NotificationsService],
  exports: [NotificationsService],
  controllers: [NotificationsController],
})
export class NotificationsModule {}
