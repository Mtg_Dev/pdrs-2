import {
  Controller,
  Get,
  Put,
  Body,
  Query,
  Post,
  HttpCode,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { NotificationsService } from './notifications.service';
import { Auth } from 'src/middlewares/guards/auth.guard';
import { ROLES } from 'src/utils/constants';

@ApiTags('Notifications')
@Controller('api/notifications')
export class NotificationsController {
  /**
   *
   */
  constructor(private notificationService: NotificationsService) {}

  @Get('types')
  getNotificationsTypes() {
    return this.notificationService.getNotificationsTypes();
  }

  @Get()
  @Auth(ROLES.Admin)
  getNotifications() {
    return this.notificationService.getNotifications();
  }

  @Post('mark-read')
  @HttpCode(200)
  @Auth(ROLES.Admin)
  readNotification(@Body() notification: { id: number }) {
    return this.notificationService.markRead(notification.id);
  }
}
