import * as nodemailer from 'nodemailer';
import * as Mailgen from 'mailgen';
import * as google from 'googleapis';

import { configService } from 'src/utils/config';

const adminMailingList = ['mtg0987654321@gmail.com'];

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    type: 'OAuth2',
    user: 'pdrs.sys@gmail.com',
    clientId: configService.getEmailCredentials().clientID,
    clientSecret: configService.getEmailCredentials().clientSecret,
    refreshToken: configService.getEmailCredentials().refreshToken,
    accessToken: configService.getEmailCredentials().accessToken,
  },
});

// verify connection configuration
transporter.verify(function(error, success) {
  if (error) {
    console.log(error);
  } else {
    console.log('Server is ready to take our messages');
  }
});

let MailGenerator = new Mailgen({
  theme: 'default',
  product: {
    name: 'PDRS',
    link: 'https://pdrs.herokuapp.com',
  },
});

import { User } from 'src/users/entities/user.entity';

export class MailingService {
  /**
   *
   */
  constructor() {}

  static async sendCreateAccountEmail(
    user: User,
    password: string,
    name: string,
  ) {
    let response = {
      body: {
        name,
        intro: [
          'Welcome to PDRS!!',
          'Your request have been accepted and You are now offically a member of this system.',
          'Your Login Credentials Are:',
          `Email: ${user.email}`,
          `Password: ${password}`,
        ],
        action: {
          instructions: 'To get started with PDRS, Login From Here:',
          button: {
            color: '#3f51b5', // Optional action button color
            text: 'Login Now',
            link: 'https://pdrs.herokuapp.com/login',
          },
        },
        outro:
          "Need help, or have questions? Just reply to this email, we'd love to help.",
      },
    };

    let mail = MailGenerator.generate(response);

    let message = {
      from: configService.getEmail(),
      to: user.email,
      subject: 'Welcome To PDRS!!',
      html: mail,
    };

    return await transporter.sendMail(message);
  }

  static async sendResetPassword(email: string, token: string) {
    let response = {
      body: {
        intro: [
          'You have requested a password reset for your PDRS Account',
          'if it is not you,then please simply ignore this email',
          'this email is valid for 6 hours only',
        ],
        action: {
          instructions: 'To set your new password use the link below:',
          button: {
            color: '#3f51b5', // Optional action button color
            text: 'Reset Password',
            link: `https://pdrs.herokuapp.com/reset-password?token=${token}`,
          },
        },
        outro:
          "Need help, or have questions? Just reply to this email, we'd love to help.",
      },
    };

    let mail = MailGenerator.generate(response);

    let message = {
      from: configService.getEmail(),
      to: email,
      subject: 'Reset Password',
      html: mail,
    };

    return await transporter.sendMail(message);
  }

  static async newMessageToAdmin(name: string) {
    let response = {
      body: {
        intro: [`Dr.${name} sent you a new message`],
        action: {
          instructions: 'To View the message go to your dashboard:',
          button: {
            color: '#3f51b5', // Optional action button color
            text: 'Open Dashboard',
            link: 'https://pdrs.herokuapp.com/admin',
          },
        },
      },
    };

    let mail = MailGenerator.generate(response);

    let message = {
      from: configService.getEmail(),
      to: adminMailingList,
      subject: 'New Message!',
      html: mail,
    };

    return await transporter.sendMail(message);
  }

  static async responseMessage(content: string, to: string) {
    let response = {
      body: {
        intro: content,
      },
    };

    let mail = MailGenerator.generate(response);

    let message = {
      from: configService.getEmail(),
      to,
      subject: 'Message From PDRS',
      html: mail,
    };

    return await transporter.sendMail(message);
  }

  static async newRequestToAdmin(name: string) {
    let response = {
      body: {
        intro: [`${name} sent you an account request`],
        action: {
          instructions: 'To View the request go to your dashboard:',
          button: {
            color: '#3f51b5', // Optional action button color
            text: 'Open Dashboard',
            link: 'https://pdrs.herokuapp.com/admin',
          },
        },
      },
    };

    let mail = MailGenerator.generate(response);

    let message = {
      from: configService.getEmail(),
      to: adminMailingList,
      subject: 'New Account Request!',
      html: mail,
    };

    return await transporter.sendMail(message);
  }
}
