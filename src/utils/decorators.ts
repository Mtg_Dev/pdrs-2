import {
  SetMetadata,
  createParamDecorator,
  ExecutionContext,
  UseInterceptors,
  BadRequestException,
} from '@nestjs/common';
import { request } from 'express';
import multer, { diskStorage } from 'multer';
import { extname } from 'path';
import { FileInterceptor } from '@nestjs/platform-express';

export const User = createParamDecorator(
  (data: string, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const user = request.user;

    return data ? user && user[data] : user;
  },
);

export const UserId = createParamDecorator(
  (data: string, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const user = request.user;
    if (user) return user.id;
    return null;
  },
);

export interface PaginationQuery {
  page: number;
  size: number;
}

export const Pagination = createParamDecorator(
  (
    {
      defaultPage = 0,
      defaultSize = 10,
    }: { defaultPage?: number; defaultSize?: number } = {},
    ctx: ExecutionContext,
  ): PaginationQuery => {
    let { page, size } = ctx.switchToHttp().getRequest().query;
    page = page ?? defaultPage;
    size = size ?? defaultSize;
    return { page, size };
  },
);

interface fileInterface {
  fieldName: string;
  uploadDir: string;
  fileSize?: number;
  allowdFileTypes?: string[];
}

export const FileUpload = (options: fileInterface) => {
  return UseInterceptors(
    FileInterceptor(options.fieldName, {
      storage: diskStorage({
        destination: `assets/${options.uploadDir}`,
        filename: (req, file, cb) => {
          // Generating a 32 random chars long string
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          //Calling the callback passing the random name generated with the original extension name
          cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
      limits: {
        fileSize: options.fileSize || 2 * 1024 * 1024,
      },
      fileFilter: (req, file, cb) => {
        if (!options.allowdFileTypes) return cb(null, true);
        for (let i = 0; i < options.allowdFileTypes.length; i++) {
          const allowedFileType = options.allowdFileTypes[i];
          //For Global Filetypes (eg 'image/*')

          if (
            allowedFileType.indexOf('/*') !== -1 &&
            file.mimetype.startsWith(
              allowedFileType.slice(0, allowedFileType.indexOf('/*')),
            )
          )
            return cb(null, true);

          // For Specific FileTypes (eg. 'image/png')
          if (file.mimetype === allowedFileType) return cb(null, true);
        }
        return cb(new BadRequestException('File Type Not Allowed'), false);
      },
    }),
  );
};
