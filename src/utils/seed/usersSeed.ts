import { Seeder, Factory } from 'typeorm-seeding';
import { Connection, In } from 'typeorm';
import { Role } from '../../users/entities/role.entity';
import { ROLES, GENDER, NOTIFICATIONS_TYPES } from '../constants';
import { User } from '../../users/entities/user.entity';
import { Doctor } from '../../users/entities/doctor.entity';
import { Pharmacy } from '../../users/entities/pharmacy.entity';
import { Patient } from '../../users/entities/patient.entity';
import { PatientInfo } from '../../users/entities/patient_info.entity';
import { classifications as classificationsData } from './classificationsSeed';
import { Classification } from '../../prescriptions/entities/classification.entity';
import { getRandomItem, getRandomRange } from '../helpers';
import * as argon2 from 'argon2';
import { City } from '../../users/entities/city.entity';
import { Message } from '../../users/entities/message.entity';
import { AccountRequest } from '../../users/entities/account_request.entity';
import { request } from 'express';
import { Notification } from '../../users/entities/notification.entity';
import * as faker from 'faker';
import { random } from 'faker';

const roles = [
  { id: '101', name: ROLES.Admin },
  { id: '102', name: ROLES.Doctor },
  { id: '103', name: ROLES.Pharmacy },
  { id: '104', name: ROLES.User }, //patient
];

const users = [
  {
    email: 'mtg@gmail.com',
    password: 'mtgmtgmtg',
    role: roles[0],
  },
  {
    email: 'ahmad@gmail.com',
    password: '123123123',
    role: roles[1],
    info: {
      name: 'Ahmad Ghazal',
      image: null,
      email: 'ahmad@gmail.com',
      phone: '0963 969737645',
    },
  },
  {
    email: 'mourad79@gmail.com',
    password: '123123123',
    role: roles[1],
    info: {
      name: 'Mourad Niazi',
      image: null,
      email: 'mourad79@gmail.com',
      phone: '0963 912341231',
    },
  },
  {
    email: 'samer@gmail.com',
    password: '123123123',
    role: roles[2],
    info: {
      name: 'Al-Fateh Pharmacy',
    },
  },
];

const getRandomId = () => {
  let id = '';
  for (let i = 0; i < 4; i++) {
    id = id.concat(faker.random.number({ min: 100, max: 999 }).toString());
  }
  return id;
};

export async function SeedUsers(
  factory: Factory,
  connection: Connection,
): Promise<any> {
  console.log('  Seeding Users');

  // Initialize Repos
  const rolesRepository = connection.getRepository(Role);
  const usersRepository = connection.getRepository(User);
  const doctorsRepository = connection.getRepository(Doctor);
  const pharmaciesRepository = connection.getRepository(Pharmacy);
  const patientsRepository = connection.getRepository(Patient);
  const patientsInfoRepository = connection.getRepository(PatientInfo);
  const classificationRepository = connection.getRepository(Classification);
  const cityRepository = connection.getRepository(City);

  const messageRepository = connection.getRepository(Message);
  const accountRequestRepository = connection.getRepository(AccountRequest);
  const notificationRepository = connection.getRepository(Notification);

  // for using later
  let doctors = [];
  let patients = [];
  const classifications = await classificationRepository.find({
    where: {
      name: In(classificationsData.map(c => c.name)),
    },
  });

  // Create Roles
  await rolesRepository.save(roles);
  for (const user of users) {
    //Create Users
    const role = await rolesRepository.findOne(user.role.id);
    let password = user.password;
    if (password) password = await argon2.hash(user.password);

    let createdUser = null;

    if (user.email)
      createdUser = await factory(User)({
        ...user,
        password,
        role,
      }).create();

    //Create Additional Info If exist
    if (user.role.name === ROLES.Doctor) {
      const city = getRandomItem(await cityRepository.find({ take: 20 }));
      const doctor: Doctor = await doctorsRepository.save({
        ...user.info,
        user: createdUser,
        city,
      });
      doctors.push(doctor);

      //Add Docotors Classifications
      doctor.classifications = getRandomItem(classifications, 2, false);
      await doctorsRepository.save(doctor);
    } else if (user.role.name === ROLES.Pharmacy) {
      await pharmaciesRepository.save({ ...user.info, user: createdUser });
    }
  }

  const cities = await cityRepository.find({ take: 20 });

  // Generate Random Doctors
  for (let i = 0; i < getRandomRange(10, 15); i++) {
    const firstName = faker.name.firstName();
    const lastName = faker.name.lastName();
    const email = faker.internet.email(firstName, lastName, 'gmail.com');
    const createdUser = await factory(User)({
      password: await argon2.hash(faker.internet.password(8)),
      email,
      role: roles[1],
    }).create();
    const city = getRandomItem(cities);
    const doctor: Doctor = await doctorsRepository.save({
      user: createdUser,
      email,
      city,
      address: faker.address.streetAddress(),
      classifications: getRandomItem(classifications, 2, false),
      phone: faker.phone.phoneNumber(),
      specification: 'Heart Surgon',
      summary: faker.lorem.paragraph(2),
      name: `${firstName} ${lastName}`,
    });
    doctors.push(doctor);
  }

  for (let i = 0; i < getRandomRange(10, 15); i++) {
    const patientInfo = await patientsInfoRepository.save({
      doctor: getRandomItem(doctors),
      bloodType: faker.random.arrayElement(['A', 'B', 'O', 'AB']),
      height: faker.random.number({ min: 150, max: 190 }),
      weight: faker.random.number({ min: 50, max: 90 }),
      sugarLevel: faker.random.number({ min: 70, max: 110 }),
    });
    const createdPatient = await patientsRepository.save({
      id: getRandomId(),
      birthDate: faker.date.between('1980', '2003'),
      sex: faker.random.arrayElement([GENDER.Male, GENDER.Female]),
      name: faker.name.findName(),
      patientInfo,
    });
    patients.push(createdPatient);
  }

  const messages = [
    { content: ' Hello how are you doing ?? ', doctor: getRandomItem(doctors) },
    {
      content:
        ' I want to contact you for something would you mind calling me  ?? ',
      doctor: getRandomItem(doctors),
    },
  ];

  // Create some messages
  const createdMsgs = await messageRepository.save(messages);

  for (const msg of createdMsgs) {
    await notificationRepository.save({
      type: NOTIFICATIONS_TYPES.NewMessage,
      tableId: msg.id.toString(),
    });
  }

  const requests = [
    {
      name: 'Kareem Fallaha',
      email: 'dr.fallaha@email.com',
      address: 'Cairo Egypt',
      phone: '935 98115331',
      specification: 'Dentist',
    },

    {
      name: 'Mazen Sabagh',
      email: 'mazen@email.com',
      address: 'USA New York',
      phone: '911 94772324',
      specification: 'Cosmetics Doctor',
    },
    {
      name: 'Hamadan Khaled',
      email: 'hamdan@email.com',
      address: 'Damascus Syria',
      phone: '963 988422423',
      specification: 'Al Hadi Pharmacy',
    },
    {
      name: 'Sami Tahaan',
      email: 'samitah@email.com',
      address: 'UK London',
      phone: '954 94772324',
      specification: 'General Surgon',
    },
  ];

  const createdRequests = await accountRequestRepository.save(requests);

  for (const request of createdRequests) {
    await notificationRepository.save({
      type: NOTIFICATIONS_TYPES.NewRequest,
      tableId: request.id.toString(),
    });
  }
}
