import { Seeder, Factory } from 'typeorm-seeding';
import { Connection, In } from 'typeorm';
import { Role } from '../../users/entities/role.entity';
import { ROLES, GENDER, NOTIFICATIONS_TYPES } from '../constants';
import { User } from '../../users/entities/user.entity';
import { Doctor } from '../../users/entities/doctor.entity';
import { Pharmacy } from '../../users/entities/pharmacy.entity';
import { Patient } from '../../users/entities/patient.entity';
import { PatientInfo } from '../../users/entities/patient_info.entity';
import { classifications as classificationsData } from './classificationsSeed';
import { Classification } from '../../prescriptions/entities/classification.entity';
import { Medicine } from '../../prescriptions/entities/medicins.entity';
import { Condition } from '../../prescriptions/entities/condition.entity';
import { Prescription } from '../../prescriptions/entities/prescription.entity';
import { PrescriptionMedicine } from '../../prescriptions/entities/prescriptionMedicins.entity';
import { Notification } from '../../users/entities/notification.entity';
import {
  getRandomItem,
  getRandomRange,
  getRandomPossibility,
} from '../helpers';

const tempConditions = [
  'Heart Attack Type C',
  'High Sugar Level',
  'Kidney Collapse',
  'Critical Brain Damage',
];

const notes = [
  'The Patient has previously suffered from a trauma',
  'please note that the patient occasionally takes some painkillers',
  'although the accident didnt do much visible damage but the brain suffered from an intense shock',
  'the patinet should not be given any extra sleeping pills',
];

const tempMedicins = ['Vardinal 420', 'Tramaroll', 'Krapreen 710', 'Dramazol '];

export async function SeedPrescriptions(
  factory: Factory,
  connection: Connection,
): Promise<any> {
  console.log('  Seeding Prescriptions');

  // Initialize Repos
  const rolesRepository = connection.getRepository(Role);
  const usersRepository = connection.getRepository(User);
  const doctorsRepository = connection.getRepository(Doctor);
  const pharmaciesRepository = connection.getRepository(Pharmacy);
  const patientsRepository = connection.getRepository(Patient);
  const patientsInfoRepository = connection.getRepository(PatientInfo);
  const classificationRepository = connection.getRepository(Classification);
  const conditionRepository = connection.getRepository(Condition);
  const medicineRepository = connection.getRepository(Medicine);
  const prescriptionRepository = connection.getRepository(Prescription);
  const notificationRepository = connection.getRepository(Notification);
  const prescriptionMedicineRepository = connection.getRepository(
    PrescriptionMedicine,
  );

  const classifications = await classificationRepository.find();
  const medicins = await medicineRepository.find();
  const doctors = await doctorsRepository.find();
  const pharmacies = await pharmaciesRepository.find();
  const patients = await patientsRepository.find();

  for (const patient of patients.slice(0, patients.length - 1)) {
    for (let i = 0; i < getRandomRange(5, 15); i++) {
      const selectedDoctor = getRandomItem(doctors);
      const selectedPharmacy = getRandomItem(pharmacies);
      const selectedClassification = getRandomItem(classifications);

      let tempCondition = '';
      let note = '';
      if (getRandomPossibility(10)) {
        tempCondition = getRandomItem(tempConditions);
      }

      if (getRandomPossibility(10)) {
        note = getRandomItem(notes);
      }
      const selectedCondition = getRandomItem(
        await conditionRepository.find({
          where: {
            classification: selectedClassification,
          },
        }),
      );

      const prescription: Prescription = await prescriptionRepository.save({
        classification: selectedClassification,
        ...(!tempCondition
          ? { condition: selectedCondition }
          : { tempCondition }),
        ...(note && { note }),
        doctor: selectedDoctor,
        patientId: patient.id,
      });

      if (tempCondition)
        await notificationRepository.save({
          type: NOTIFICATIONS_TYPES.NewCondition,
          tableId: prescription.id.toString(),
        });

      let selectedMedicins = getRandomItem(
        medicins,
        getRandomRange(2, 5),
        false,
      );

      if (getRandomPossibility(10))
        selectedMedicins.push({
          tempMedicine: getRandomItem(tempMedicins),
        });

      const prescriptionMedicins = await prescriptionMedicineRepository.save<
        PrescriptionMedicine
      >(
        selectedMedicins.map(medicine => ({
          ...(medicine.tempMedicine
            ? { tempMedicine: medicine.tempMedicine }
            : { medicine }),
          prescription,
          isBold: getRandomPossibility(15),
          isChronic: getRandomPossibility(7),
          pharmacy: getRandomPossibility(85) ? selectedPharmacy : null,
        })),
      );

      for (const pm of prescriptionMedicins) {
        if (pm.tempMedicine)
          await notificationRepository.save({
            type: NOTIFICATIONS_TYPES.NewMedicine,
            tableId: pm.id.toString(),
          });
      }
    }
  }
}
