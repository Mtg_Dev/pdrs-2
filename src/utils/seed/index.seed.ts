import { Seeder, Factory } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { SeedClassifications } from './classificationsSeed';
import { SeedUsers } from './usersSeed';
import { SeedPrescriptions } from './prescriptionSeed';
import { SeedCountries } from './countriesSeed';
import { removeData } from './remove-date';

export default class CreateSeed implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await removeData(connection);
    await SeedCountries(factory, connection);
    await SeedClassifications(factory, connection);
    await SeedUsers(factory, connection);
    await SeedPrescriptions(factory, connection);
  }
}
