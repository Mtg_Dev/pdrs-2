import { Connection } from 'typeorm';
import { Classification } from '../../prescriptions/entities/classification.entity';
import { Condition } from '../../prescriptions/entities/condition.entity';
import { Medicine } from '../../prescriptions/entities/medicins.entity';
import { Role } from '../../users/entities/role.entity';
import { User } from '../../users/entities/user.entity';
import { PatientInfo } from '../../users/entities/patient_info.entity';
import { Prescription } from '../../prescriptions/entities/prescription.entity';
import { PrescriptionMedicine } from '../../prescriptions/entities/prescriptionMedicins.entity';
import { Notification } from '../../users/entities/notification.entity';

export async function removeData(connection: Connection) {
  console.log(' Removing Data ');

  // Initialize Repos
  const classificationsRepository = connection.getRepository(Classification);
  const conditionsRepository = connection.getRepository(Condition);
  const medicinsRepository = connection.getRepository(Medicine);
  const prescriptionRepository = connection.getRepository(Prescription);
  const prescriptionMedicineRepository = connection.getRepository(
    PrescriptionMedicine,
  );
  const rolesRepository = connection.getRepository(Role);
  const usersRepository = connection.getRepository(User);
  const notificationsRepository = connection.getRepository(Notification);

  const patientsInfoRepository = connection.getRepository(PatientInfo);

  //Clear Tables
  await notificationsRepository.delete({});
  await prescriptionMedicineRepository.delete({});
  await prescriptionRepository.delete({});
  await medicinsRepository.delete({});
  await classificationsRepository.delete({});
  await usersRepository.delete({});
  await rolesRepository.delete({});
  await patientsInfoRepository.delete({});
}
