import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export const initSwagger = app => {
  const options = new DocumentBuilder()
    .setTitle('P.D.R.S ')
    .setDescription(
      'Prescription Dispensing Regulating System is an online central platform that improves & enhances the doctors work and makes their work easier and more effiecent',
    )
    .setVersion('1.0')
    .addCookieAuth('accessToken')
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('swagger', app, document);
};
