import * as fs from 'fs';
import { ROOT_DIR } from './constants';

export function generatePassword() {
  var length = 8,
    charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
    retVal = '';
  for (var i = 0, n = charset.length; i < length; ++i) {
    retVal += charset.charAt(Math.floor(Math.random() * n));
  }
  return retVal;
}

/**
   * Returns one or more random items form an array of items.
   *
 
   *
   * @param items - The items array
   * @param count - how many items do you want
   * @param allowDuplication - is duplication allowed between items
   * @returns returns either one item (default) or an array of items if "count" parameter is specified
   *
   */
export function getRandomItem<T extends any[] | []>(
  items: T,
  count: number = 1,
  allowDuplication: boolean = true,
) {
  let result = [];
  if (count > 1) {
    if (allowDuplication) {
      for (let i = 0; i < count; i++) {
        result.push(items[Math.floor(Math.random() * items.length)]);
      }
    } else {
      if (count > items.length) return items;

      let itemsToPick = new Set();
      while (itemsToPick.size < count)
        itemsToPick.add(items[Math.floor(Math.random() * items.length)]);

      result = Array.from(itemsToPick);
    }
    return result;
  } else return items[Math.floor(Math.random() * items.length)];
}

export function getRandomRange(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min)) + min;
}

export function getRandomPossibility(percentage: number) {
  if (percentage > 100 || percentage < 0) return false;
  return Math.random() * 100 < percentage;
}

/**
 * Returns an array with arrays of the given size.
 *
 * @param myArray {Array} array to split
 * @param chunk_size {Integer} Size of every group
 */
export function chunkArray(myArray, chunk_size) {
  var index = 0;
  var arrayLength = myArray.length;
  var tempArray = [];

  for (index = 0; index < arrayLength; index += chunk_size) {
    const myChunk = myArray.slice(index, index + chunk_size);
    // Do something if you want with the group
    tempArray.push(myChunk);
  }

  return tempArray;
}

export function autoMap(obj1, obj2) {
  let result = {};

  let keys = [];
  if (typeof obj1 === 'function') keys = Object.keys(new obj1());
  else if (Array.isArray(obj1)) keys = obj1;
  else keys = Object.keys(obj1);
  for (const key of keys) {
    if (obj2.hasOwnProperty(key)) {
      // or obj1.hasOwnProperty(key)
      result[key] = obj2[key];
    }
  }
  if (Object.keys(result).length === 0) return null;

  return result;
}

export async function removeFile(path: string) {
  path = `${ROOT_DIR}/${path}`;
  fs.exists(path, exists => {
    if (!exists) return;
    return new Promise((res, rej) =>
      fs.unlink(path, err => {
        if (err) rej(err);
        res();
      }),
    );
  });
}
