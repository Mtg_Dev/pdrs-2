// src/config/config.service.ts
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

require('dotenv').config();

class ConfigService {
  constructor(private env: { [k: string]: string | undefined }) {}

  private getValue(key: string, throwOnMissing = true): string {
    const value = this.env[key];
    if (!value && throwOnMissing) {
      throw new Error(`config error - missing env.${key}`);
    }

    return value;
  }

  public ensureValues(keys: string[]) {
    keys.forEach(k => this.getValue(k, true));
    return this;
  }

  public getJwtSecret() {
    return this.getValue('JWT_SECRET', true);
  }

  public getEmail() {
    return this.getValue('EMAIL', true);
  }

  public getEmailCredentials() {
    return {
      clientID: this.getValue('GMAIL_CLIENT_ID'),
      clientSecret: this.getValue('GMAIL_CLIENT_SECRET'),
      accessToken: this.getValue('GMAIL_ACCESS_TOKEN'),
      refreshToken: this.getValue('GMAIL_REFRESH_TOKEN'),
    };
  }

  public getPort() {
    return this.getValue('PORT', true);
  }

  public isProduction() {
    const mode = this.getValue('MODE', false);
    return mode != 'DEV';
  }

  public isTest() {
    return this.getValue('NODE_ENV', false) === 'test';
  }

  public getTypeOrmConfig(): TypeOrmModuleOptions {
    if (this.isTest())
      return {
        type: 'sqlite',
        database: ':memory:',
        entities: ['src/**/*.entity.ts'],
        dropSchema: true,
        synchronize: true,
      };

    return {
      type: 'sqlite',
      database: './sqlite/db.db',
      entities: ['dist/**/*.entity.js'],
      synchronize: true,

      migrationsTableName: 'migration',
      migrations: ['dist/migration/*.js'],
      cli: {
        migrationsDir: 'migration',
      },
    };
  }
}

const configService = new ConfigService(process.env).ensureValues([]);

export { configService };
