import { resolve } from 'path';

export const ROLES = {
  Admin: 'admin',
  Doctor: 'doctor',
  Pharmacy: 'pharmacy',
  User: 'user',
};
export const UPLOAD_DIR = {
  Classifications: 'images/classifications',
  Profiles: 'images/profiles',
};
export const GENDER = { Male: 'male', Female: 'female' };

export const TOKENS_TYPES = {
  ResetPassword: 'rp',
};

export const NOTIFICATIONS_TYPES = {
  NewCondition: 'cond',
  NewMedicine: 'med',
  NewMessage: 'msg',
  NewRequest: 'req',
  NewReport: 'rep',
};

export const ROOT_DIR = resolve(__dirname, '../..');

export default {
  ROLES,
  UPLOAD_DIR,
  ROOT_DIR,
};
