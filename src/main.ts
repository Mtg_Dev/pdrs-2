import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cookieParser from 'cookie-parser';
import { ValidationPipe } from '@nestjs/common';
import { initSwagger } from './utils/swagger';

import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import { ROOT_DIR } from './utils/constants';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.use(cookieParser());
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );
  app.enableCors();
  app.useStaticAssets(join(ROOT_DIR, 'assets'), {
    prefix: '/assets/',
  });

  initSwagger(app);
  await app.listen(process.env.PORT || 5000);
}
bootstrap();
