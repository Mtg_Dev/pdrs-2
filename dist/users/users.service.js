"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_entity_1 = require("./entities/user.entity");
const typeorm_2 = require("typeorm");
const role_entity_1 = require("./entities/role.entity");
const patient_entity_1 = require("./entities/patient.entity");
const doctor_entity_1 = require("./entities/doctor.entity");
const pharmacy_entity_1 = require("./entities/pharmacy.entity");
const patient_info_entity_1 = require("./entities/patient_info.entity");
const constants_1 = require("../utils/constants");
const helpers_1 = require("../utils/helpers");
const prescription_entity_1 = require("../prescriptions/entities/prescription.entity");
const argon = require("argon2");
const notifications_service_1 = require("./notifications/notifications.service");
const account_request_entity_1 = require("./entities/account_request.entity");
const message_entity_1 = require("./entities/message.entity");
const country_entity_1 = require("./entities/country.entity");
const city_entity_1 = require("./entities/city.entity");
const classification_entity_1 = require("../prescriptions/entities/classification.entity");
const mail_service_1 = require("../services/mail.service");
const jwt_1 = require("@nestjs/jwt");
const console_1 = require("console");
let UsersService = class UsersService {
    constructor(notificationsService, jwtService, usersRepository, rolesRepository, patientsRepository, patientInfosRepository, doctorsRepository, pharmacysRepository, prescriptionsRepository, accountRequestsRepository, messagesRepository, countriesRepository, citiesRepository, classificationRepository) {
        this.notificationsService = notificationsService;
        this.jwtService = jwtService;
        this.usersRepository = usersRepository;
        this.rolesRepository = rolesRepository;
        this.patientsRepository = patientsRepository;
        this.patientInfosRepository = patientInfosRepository;
        this.doctorsRepository = doctorsRepository;
        this.pharmacysRepository = pharmacysRepository;
        this.prescriptionsRepository = prescriptionsRepository;
        this.accountRequestsRepository = accountRequestsRepository;
        this.messagesRepository = messagesRepository;
        this.countriesRepository = countriesRepository;
        this.citiesRepository = citiesRepository;
        this.classificationRepository = classificationRepository;
    }
    findAll() {
        return this.usersRepository.find();
    }
    findOne(id, relations = []) {
        return this.usersRepository.findOne(id, { relations });
    }
    findByEmail(email) {
        return this.usersRepository.findOne({
            where: {
                email,
            },
            select: ['id', 'email', 'password'],
            relations: ['role'],
        });
    }
    async saveRefreshToken(userId, refreshToken) {
        return this.usersRepository.update(userId, {
            refreshToken: await argon.hash(refreshToken),
        });
    }
    async validateUserRefreshToken(refreshToken, userId) {
        const user = await this.findOne(userId);
        if (!user.isBlocked &&
            (await argon.verify(user.refreshToken, refreshToken)))
            return user;
    }
    async getUserRole(id) {
        try {
            return (await this.usersRepository.findOne({
                where: {
                    id,
                },
                relations: ['role'],
            })).role;
        }
        catch (error) {
            throw new common_1.BadRequestException();
        }
    }
    getUserInfo(id, role) {
        let relations = [];
        if (role === constants_1.ROLES.Doctor) {
            return this.usersRepository
                .createQueryBuilder('user')
                .where('user.id = :id', { id })
                .leftJoinAndSelect('user.doctor', 'doctor')
                .leftJoinAndSelect('doctor.classifications', 'user.classifications')
                .leftJoinAndMapOne('doctor.city', 'city', 'city', 'city.id=doctor.cityId')
                .leftJoinAndMapOne('doctor.country', 'country', 'country', 'country.id=city.countryId')
                .getOne();
        }
        else if (role === constants_1.ROLES.Pharmacy)
            relations = [constants_1.ROLES.Pharmacy];
        else if (role === constants_1.ROLES.User)
            relations = [constants_1.ROLES.User];
        return this.usersRepository.findOne({
            where: { id },
            relations,
        });
    }
    getUsers(page, size) {
        return this.usersRepository.find({
            relations: ['doctor', 'pharmacy', 'patient', 'role'],
            take: size,
            skip: page * size,
        });
    }
    async getDoctors({ page, size, query = '', countryId, cityId, classificationIds, }) {
        classificationIds = (classificationIds === null || classificationIds === void 0 ? void 0 : classificationIds.map(c => +c)) || null;
        let sqlQuery = this.doctorsRepository.createQueryBuilder('doctor');
        if (classificationIds)
            sqlQuery.innerJoin('doctor.classifications', 'classification', 'classification.id IN (:...classificationIds)', { classificationIds });
        if (query)
            sqlQuery.andWhere('doctor.name like :name', { name: `%${query}%` });
        if (countryId) {
            sqlQuery.innerJoin('doctor.city', 'city');
            sqlQuery.innerJoin('city.country', 'country', 'country.id=:countryId', {
                countryId,
            });
        }
        if (cityId)
            sqlQuery.andWhere('doctor.city = :cityId', { cityId });
        return sqlQuery
            .take(size)
            .skip(size * page)
            .getMany();
    }
    async createUser(userDto) {
        const exist = await this.usersRepository.findOne({ email: userDto.email });
        if (exist)
            throw new common_1.BadRequestException();
        const password = `password${helpers_1.getRandomRange(111, 999)}`;
        const role = await this.rolesRepository.findOne({
            name: userDto.doctor ? 'doctor' : 'pharmacy',
        });
        const createdUser = await this.usersRepository.save({
            email: userDto.email,
            password: await argon.hash(password),
            role,
        });
        if (userDto.doctor) {
            const doctor = await this.doctorsRepository.save({
                email: userDto.email,
                name: userDto.doctor.name,
                phone: userDto.doctor.phone,
                user: createdUser,
            });
            await mail_service_1.MailingService.sendCreateAccountEmail(createdUser, password, doctor.name);
            delete doctor.user;
            delete createdUser.password;
            return Object.assign(Object.assign({}, createdUser), { doctor });
        }
        else if (userDto.pharmacy) {
            const pharmacy = await this.pharmacysRepository.save({
                name: userDto.pharmacy.name,
                user: createdUser,
            });
            await mail_service_1.MailingService.sendCreateAccountEmail(createdUser, password, pharmacy.name);
            delete pharmacy.user;
            delete createdUser.password;
            return Object.assign(Object.assign({}, createdUser), { pharmacy });
        }
    }
    async updatePhoto(id, image) {
        const doctor = await this.getDoctor(id);
        await helpers_1.removeFile(doctor.image);
        doctor.image = image;
        return this.doctorsRepository.save(doctor);
    }
    async updateAccount(user, updateAccountDto) {
        try {
            if (updateAccountDto.password)
                await this.usersRepository.update(user.id, {
                    password: await argon.hash(updateAccountDto.password),
                });
            if (user.role === constants_1.ROLES.Doctor) {
                let doctor = await this.doctorsRepository.findOne({
                    where: {
                        userId: user.id,
                    },
                });
                if (updateAccountDto.classifications.length > 0) {
                    const doctorClassifications = await this.classificationRepository.find({
                        where: {
                            id: typeorm_2.In(updateAccountDto.classifications),
                        },
                    });
                    doctor.classifications = doctorClassifications;
                    await this.doctorsRepository.save(doctor);
                }
                if (!doctor)
                    return await this.doctorsRepository.save(Object.assign(Object.assign({}, helpers_1.autoMap(['name', 'address', 'phone', 'specification', 'summary', 'email'], updateAccountDto)), { userId: user.id }));
                else {
                    const updateObject = helpers_1.autoMap(['name', 'address', 'phone', 'specification', 'summary', 'email'], updateAccountDto);
                    if (updateObject)
                        await this.doctorsRepository
                            .createQueryBuilder()
                            .update(doctor_entity_1.Doctor)
                            .set(updateObject)
                            .where('doctor.userId=:id', { id: user.id })
                            .execute();
                }
            }
            else if (user.role === constants_1.ROLES.Pharmacy) {
                await this.pharmacysRepository
                    .createQueryBuilder()
                    .update(pharmacy_entity_1.Pharmacy)
                    .set(helpers_1.autoMap(['name'], updateAccountDto))
                    .where('pharmacy.userId=:id', { id: user.id })
                    .execute();
            }
            return this.getUserInfo(user.id, user.role);
        }
        catch (error) {
            throw new common_1.BadRequestException();
        }
    }
    async savePatient(userId, createPatientDto) {
        const patientExist = await this.patientsRepository.findOne({
            where: { id: createPatientDto.ID },
            relations: ['patientInfo'],
        });
        const doctor = await this.doctorsRepository.findOne({
            where: {
                userId,
            },
        });
        if (patientExist) {
            const updatePatientObject = helpers_1.autoMap(['name', 'sex', 'birthDate'], createPatientDto);
            const udpatePatientInfoObject = helpers_1.autoMap(['height', 'weight', 'sugarLevel', 'bloodType'], createPatientDto);
            if (updatePatientObject)
                await this.patientsRepository.update(patientExist.id, updatePatientObject);
            if (udpatePatientInfoObject)
                await this.patientInfosRepository.update(patientExist.patientInfo.id, udpatePatientInfoObject);
            return patientExist;
        }
        const newPatient = await this.patientsRepository.save(Object.assign({ id: createPatientDto.ID }, createPatientDto));
        await this.patientInfosRepository.save(Object.assign(Object.assign({}, createPatientDto), { doctor, patient: newPatient }));
        return await this.patientsRepository.findOne(createPatientDto.ID);
    }
    async getPatinet(id) {
        try {
            return await this.patientsRepository.findOneOrFail({
                where: {
                    id: id,
                },
                relations: ['patientInfo'],
            });
        }
        catch (error) {
            throw new common_1.NotFoundException();
        }
    }
    async getPatinetById(id) {
        return this.patientsRepository.findOne({
            where: {
                id,
            },
            relations: ['patientInfo'],
        });
    }
    async getDoctor(id) {
        return this.doctorsRepository.findOne({
            where: {
                userId: id,
            },
        });
    }
    async getDoctorsPatients(id) {
        const doctorId = (await this.getDoctor(id)).id;
        return this.prescriptionsRepository
            .createQueryBuilder('prescription')
            .where('doctorId = :doctorId', { doctorId })
            .innerJoinAndMapOne('prescription.patient', 'patient', 'patient', 'patient.id=prescription.patientId')
            .select(['patient'])
            .distinct(true)
            .getRawMany();
    }
    async getPatients(id, name, encode = false) {
        if ((!name || name.trim().length <= 1) && (!id || id.trim().length <= 6))
            return [];
        return this.patientsRepository
            .createQueryBuilder('patients')
            .where('id like :id', { id: `${id}%` })
            .orWhere('name like :name', { name: `%${name}%` })
            .getMany()
            .then(patients => patients.map(p => (Object.assign(Object.assign({}, p), { id: `${p.id.slice(0, -5)}*****` }))));
    }
    async getPharmacy(id) {
        return this.pharmacysRepository.findOne({
            where: {
                user: id,
            },
        });
    }
    async requestAccount(requestDto) {
        const request = await this.accountRequestsRepository.save(Object.assign({}, requestDto));
        this.notificationsService.addRequest(request);
        await mail_service_1.MailingService.newRequestToAdmin(requestDto.name);
        return request;
    }
    async sendMessage(message, userId) {
        const doctor = await this.getDoctor(userId);
        const createdMessage = await this.messagesRepository.save({
            content: message.content,
            doctor,
        });
        this.notificationsService.addMessage(createdMessage);
        await mail_service_1.MailingService.newMessageToAdmin(doctor.name);
        return createdMessage;
    }
    async respondMessage(message) {
        await mail_service_1.MailingService.responseMessage(message.content, message.to);
    }
    async toggleBlock(id) {
        const user = await this.usersRepository.findOne({ id });
        user.isBlocked = !user.isBlocked;
        return this.usersRepository.save(user);
    }
    async requestResetPassword(email) {
        const user = await this.findByEmail(email);
        if (!user)
            throw new common_1.BadRequestException("email doesn't exist");
        const token = this.jwtService.sign({
            email,
            type: constants_1.TOKENS_TYPES.ResetPassword,
        });
        await mail_service_1.MailingService.sendResetPassword(email, token);
    }
    async resetPassword(token, newPassword) {
        try {
            const result = await this.jwtService.verifyAsync(token);
            console_1.assert(result.type === constants_1.TOKENS_TYPES.ResetPassword);
            const user = await this.findByEmail(result.email);
            return await this.usersRepository.update(user.id, {
                password: await argon.hash(newPassword),
            });
        }
        catch (error) {
            throw new common_1.BadRequestException('token invalid');
        }
    }
    async getCountries() {
        return this.countriesRepository.find({});
    }
    async getCities(id) {
        return this.citiesRepository.find({
            where: {
                country: id,
            },
        });
    }
};
UsersService = __decorate([
    common_1.Injectable(),
    __param(2, typeorm_1.InjectRepository(user_entity_1.User)),
    __param(3, typeorm_1.InjectRepository(role_entity_1.Role)),
    __param(4, typeorm_1.InjectRepository(patient_entity_1.Patient)),
    __param(5, typeorm_1.InjectRepository(patient_info_entity_1.PatientInfo)),
    __param(6, typeorm_1.InjectRepository(doctor_entity_1.Doctor)),
    __param(7, typeorm_1.InjectRepository(pharmacy_entity_1.Pharmacy)),
    __param(8, typeorm_1.InjectRepository(prescription_entity_1.Prescription)),
    __param(9, typeorm_1.InjectRepository(account_request_entity_1.AccountRequest)),
    __param(10, typeorm_1.InjectRepository(message_entity_1.Message)),
    __param(11, typeorm_1.InjectRepository(country_entity_1.Country)),
    __param(12, typeorm_1.InjectRepository(city_entity_1.City)),
    __param(13, typeorm_1.InjectRepository(classification_entity_1.Classification)),
    __metadata("design:paramtypes", [notifications_service_1.NotificationsService,
        jwt_1.JwtService,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], UsersService);
exports.UsersService = UsersService;
//# sourceMappingURL=users.service.js.map