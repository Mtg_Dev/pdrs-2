"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersModule = void 0;
const common_1 = require("@nestjs/common");
const users_service_1 = require("./users.service");
const typeorm_1 = require("@nestjs/typeorm");
const user_entity_1 = require("./entities/user.entity");
const role_entity_1 = require("./entities/role.entity");
const users_controller_1 = require("./users.controller");
const patient_entity_1 = require("./entities/patient.entity");
const patient_info_entity_1 = require("./entities/patient_info.entity");
const doctor_entity_1 = require("./entities/doctor.entity");
const pharmacy_entity_1 = require("./entities/pharmacy.entity");
const prescription_entity_1 = require("../prescriptions/entities/prescription.entity");
const prescriptionMedicins_entity_1 = require("../prescriptions/entities/prescriptionMedicins.entity");
const notifications_module_1 = require("./notifications/notifications.module");
const account_request_entity_1 = require("./entities/account_request.entity");
const message_entity_1 = require("./entities/message.entity");
const country_entity_1 = require("./entities/country.entity");
const city_entity_1 = require("./entities/city.entity");
const classification_entity_1 = require("../prescriptions/entities/classification.entity");
const jwt_1 = require("@nestjs/jwt");
const config_1 = require("../utils/config");
let UsersModule = class UsersModule {
};
UsersModule = __decorate([
    common_1.Module({
        imports: [
            jwt_1.JwtModule.register({
                secret: config_1.configService.getJwtSecret(),
                signOptions: { expiresIn: '6h' },
            }),
            typeorm_1.TypeOrmModule.forFeature([
                user_entity_1.User,
                role_entity_1.Role,
                patient_entity_1.Patient,
                patient_info_entity_1.PatientInfo,
                doctor_entity_1.Doctor,
                pharmacy_entity_1.Pharmacy,
                prescription_entity_1.Prescription,
                account_request_entity_1.AccountRequest,
                message_entity_1.Message,
                country_entity_1.Country,
                city_entity_1.City,
                classification_entity_1.Classification,
            ]),
            notifications_module_1.NotificationsModule,
        ],
        providers: [users_service_1.UsersService],
        exports: [users_service_1.UsersService],
        controllers: [users_controller_1.UsersController],
    })
], UsersModule);
exports.UsersModule = UsersModule;
//# sourceMappingURL=users.module.js.map