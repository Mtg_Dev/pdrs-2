import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users.service';
import { User } from '../entities/user.entity';
export interface UserJwt {
    id: string;
    role: string;
}
export declare class AuthService {
    private usersService;
    private jwtService;
    constructor(usersService: UsersService, jwtService: JwtService);
    validateUser(email: string, password: string): Promise<User | null>;
    login(user: any): Promise<{
        accessToken: string;
        refreshToken: string;
    }>;
}
