"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const users_service_1 = require("../users.service");
const auth_service_1 = require("./auth.service");
const loginUser_dto_1 = require("../Dtos/loginUser.dto");
const refresh_guard_1 = require("../../middlewares/guards/refresh.guard");
const decorators_1 = require("../../utils/decorators");
let AuthController = class AuthController {
    constructor(userService, authService) {
        this.userService = userService;
        this.authService = authService;
    }
    async login(response, user) {
        const userData = await this.authService.validateUser(user.email, user.password);
        if (!userData)
            throw new common_1.BadRequestException();
        const { accessToken, refreshToken } = await this.authService.login(userData);
        await this.userService.saveRefreshToken(userData.id, refreshToken);
        response.cookie('accessToken', accessToken);
        response.cookie('refreshToken', refreshToken);
        return response.send({
            accessToken,
            user: userData,
        });
    }
    async refresh(response, userId) {
        const user = await this.userService.findOne(userId, ['role']);
        const { accessToken, refreshToken } = await this.authService.login(user);
        await this.userService.saveRefreshToken(user.id, refreshToken);
        response.cookie('accessToken', accessToken);
        response.cookie('refreshToken', refreshToken);
        return response.send({
            accessToken,
        });
    }
    async logout(response) {
        response.clearCookie('accessToken');
        return response.send({
            msg: 'Logged Out Successfully',
        });
    }
};
__decorate([
    common_1.Post('login'),
    common_1.HttpCode(200),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, loginUser_dto_1.LoginUserDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "login", null);
__decorate([
    common_1.UseGuards(refresh_guard_1.default),
    common_1.Get('refresh'),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Res()), __param(1, decorators_1.UserId()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "refresh", null);
__decorate([
    common_1.Post('logout'),
    common_1.HttpCode(200),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "logout", null);
AuthController = __decorate([
    swagger_1.ApiTags('Auth'),
    common_1.Controller('api/auth'),
    __metadata("design:paramtypes", [users_service_1.UsersService,
        auth_service_1.AuthService])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=auth.controller.js.map