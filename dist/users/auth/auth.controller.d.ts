import { UsersService } from '../users.service';
import { AuthService } from './auth.service';
import { LoginUserDto } from '../Dtos/loginUser.dto';
export declare class AuthController {
    private userService;
    private authService;
    constructor(userService: UsersService, authService: AuthService);
    login(response: any, user: LoginUserDto): Promise<any>;
    refresh(response: any, userId: any): Promise<any>;
    logout(response: any): Promise<any>;
}
