import { Request } from 'express';
import { UsersService } from '../users.service';
declare const JwtRefreshTokenStrategy_base: new (...args: any[]) => any;
export declare class JwtRefreshTokenStrategy extends JwtRefreshTokenStrategy_base {
    private readonly userService;
    constructor(userService: UsersService);
    validate(request: Request, payload: {
        id: string;
    }): Promise<import("../entities/user.entity").User>;
}
export {};
