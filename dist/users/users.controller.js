"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const auth_guard_1 = require("../middlewares/guards/auth.guard");
const constants_1 = require("../utils/constants");
const swagger_1 = require("@nestjs/swagger");
const users_service_1 = require("./users.service");
const createPatient_dto_1 = require("./Dtos/createPatient.dto");
const decorators_1 = require("../utils/decorators");
const requestAccount_dto_1 = require("./Dtos/requestAccount.dto");
const message_dto_1 = require("./Dtos/message.dto");
const pagination_1 = require("../middlewares/pipes/pagination");
const createUser_dto_1 = require("./Dtos/createUser.dto");
const updateAccount_dto_1 = require("./Dtos/updateAccount.dto");
const confirmAccount_dto_1 = require("./Dtos/confirmAccount.dto");
const updatePatient_dto_1 = require("./Dtos/updatePatient.dto");
const mail_service_1 = require("../services/mail.service");
let UsersController = class UsersController {
    constructor(userService) {
        this.userService = userService;
    }
    async getUsers({ page, size }) {
        return await this.userService.getUsers(page, size);
    }
    async getDoctors({ page, size }, query, countryId, cityId, classificationIds) {
        return await this.userService.getDoctors({
            page,
            size,
            query,
            countryId,
            cityId,
            classificationIds,
        });
    }
    async getProfile(user, id) {
        if (!id)
            return this.userService.getUserInfo(user.id, user.role);
        const role = await this.userService.getUserRole(id);
        return this.userService.getUserInfo(id, role.name);
    }
    async getPatient(id) {
        return await this.userService.getPatinet(id);
    }
    async newUser(createUserDto) {
        return this.userService.createUser(createUserDto);
    }
    async updatePhoto(file, id) {
        return this.userService.updatePhoto(id, file.path);
    }
    async updateAccountInfo(user, updateAccountDTO) {
        return this.userService.updateAccount(user, updateAccountDTO);
    }
    async confirmAccountInfo(user, confimrAccountDTO) {
        return this.userService.updateAccount(user, confimrAccountDTO);
    }
    async newPatient(userId, createPatientDTO) {
        return this.userService.savePatient(userId, createPatientDTO);
    }
    async updatePatient(userId, updatePatientDTO) {
        return this.userService.savePatient(userId, updatePatientDTO);
    }
    async getDoctorPatients(id) {
        return await this.userService.getDoctorsPatients(id);
    }
    async getPatients(id, name) {
        return this.userService.getPatients(id, name, true);
    }
    requestAccount(request) {
        return this.userService.requestAccount(request);
    }
    toggleBlock(user) {
        return this.userService.toggleBlock(user.id);
    }
    sendMessage(message, userId) {
        return this.userService.sendMessage(message, userId);
    }
    respondMessage(message) {
        return this.userService.respondMessage(message);
    }
    async getCountries() {
        return await this.userService.getCountries();
    }
    async getCities(id) {
        return await this.userService.getCities(id);
    }
    requestResetPassword(email) {
        return this.userService.requestResetPassword(email);
    }
    resetPassword({ token, newPassword }) {
        return this.userService.resetPassword(token, newPassword);
    }
};
__decorate([
    common_1.Get(),
    auth_guard_1.Auth(constants_1.ROLES.Admin),
    pagination_1.ApiPagination(),
    openapi.ApiResponse({ status: 200, type: [require("./entities/user.entity").User] }),
    __param(0, decorators_1.Pagination()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getUsers", null);
__decorate([
    common_1.Get('doctors'),
    pagination_1.ApiPagination(),
    openapi.ApiResponse({ status: 200, type: [require("./entities/doctor.entity").Doctor] }),
    __param(0, decorators_1.Pagination()),
    __param(1, common_1.Query('query')),
    __param(2, common_1.Query('countryId')),
    __param(3, common_1.Query('cityId')),
    __param(4, common_1.Query('classificationIds')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String, Number, Number, Array]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getDoctors", null);
__decorate([
    common_1.Get('profile'),
    auth_guard_1.Auth(),
    pagination_1.ApiPagination(),
    openapi.ApiResponse({ status: 200, type: require("./entities/user.entity").User }),
    __param(0, decorators_1.User()), __param(1, common_1.Query('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getProfile", null);
__decorate([
    common_1.Get('patient/:id'),
    auth_guard_1.Auth(constants_1.ROLES.Admin, constants_1.ROLES.Doctor),
    openapi.ApiResponse({ status: 200, type: require("./entities/patient.entity").Patient }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getPatient", null);
__decorate([
    common_1.Post(),
    auth_guard_1.Auth(constants_1.ROLES.Admin),
    openapi.ApiResponse({ status: 201 }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [createUser_dto_1.CreateUserDto]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "newUser", null);
__decorate([
    decorators_1.FileUpload({
        fieldName: 'image',
        uploadDir: constants_1.UPLOAD_DIR.Profiles,
        allowdFileTypes: ['image/*'],
    }),
    common_1.Post('update-photo'),
    auth_guard_1.Auth(constants_1.ROLES.Doctor),
    openapi.ApiResponse({ status: 201, type: require("./entities/doctor.entity").Doctor }),
    __param(0, common_1.UploadedFile()), __param(1, decorators_1.UserId()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "updatePhoto", null);
__decorate([
    common_1.Put(),
    auth_guard_1.Auth(),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, decorators_1.User()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, updateAccount_dto_1.UpdateAccountDTO]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "updateAccountInfo", null);
__decorate([
    common_1.Put('confirm-account'),
    auth_guard_1.Auth(),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, decorators_1.User()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, confirmAccount_dto_1.ConfirmAccountDTO]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "confirmAccountInfo", null);
__decorate([
    common_1.Post('patient'),
    auth_guard_1.Auth(constants_1.ROLES.Admin, constants_1.ROLES.Doctor),
    openapi.ApiResponse({ status: 201, type: require("./entities/patient.entity").Patient }),
    __param(0, decorators_1.UserId()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, createPatient_dto_1.CreatePatientDTO]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "newPatient", null);
__decorate([
    common_1.Put('patient'),
    auth_guard_1.Auth(constants_1.ROLES.Admin, constants_1.ROLES.Doctor),
    openapi.ApiResponse({ status: 200, type: require("./entities/patient.entity").Patient }),
    __param(0, decorators_1.UserId()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, updatePatient_dto_1.UpdatePatientDTO]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "updatePatient", null);
__decorate([
    common_1.Get('doctor-patients'),
    auth_guard_1.Auth(constants_1.ROLES.Doctor),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __param(0, decorators_1.UserId()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getDoctorPatients", null);
__decorate([
    common_1.Get('patients'),
    auth_guard_1.Auth(constants_1.ROLES.Doctor, constants_1.ROLES.Pharmacy),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Query('id')), __param(1, common_1.Query('name')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getPatients", null);
__decorate([
    common_1.Post('request-account'),
    openapi.ApiResponse({ status: 201, type: Object }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [requestAccount_dto_1.RequestAccountDTO]),
    __metadata("design:returntype", void 0)
], UsersController.prototype, "requestAccount", null);
__decorate([
    common_1.Post('toggle-block'),
    auth_guard_1.Auth(constants_1.ROLES.Admin),
    openapi.ApiResponse({ status: 201, type: require("./entities/user.entity").User }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], UsersController.prototype, "toggleBlock", null);
__decorate([
    common_1.Post('send-message'),
    auth_guard_1.Auth(constants_1.ROLES.Doctor, constants_1.ROLES.Pharmacy),
    openapi.ApiResponse({ status: 201, type: Object }),
    __param(0, common_1.Body()), __param(1, decorators_1.UserId()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [message_dto_1.MessageDTO, String]),
    __metadata("design:returntype", void 0)
], UsersController.prototype, "sendMessage", null);
__decorate([
    common_1.Post('respond-message'),
    auth_guard_1.Auth(constants_1.ROLES.Admin),
    openapi.ApiResponse({ status: 201 }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [message_dto_1.MessageDTO]),
    __metadata("design:returntype", void 0)
], UsersController.prototype, "respondMessage", null);
__decorate([
    common_1.Get('countries'),
    auth_guard_1.Auth(),
    openapi.ApiResponse({ status: 200, type: [require("./entities/country.entity").Country] }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getCountries", null);
__decorate([
    common_1.Get('cities'),
    auth_guard_1.Auth(),
    openapi.ApiResponse({ status: 200, type: [require("./entities/city.entity").City] }),
    __param(0, common_1.Query('countryId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getCities", null);
__decorate([
    common_1.Get('request-reset-password'),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Query('email')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], UsersController.prototype, "requestResetPassword", null);
__decorate([
    common_1.Post('reset-password'),
    openapi.ApiResponse({ status: 201 }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], UsersController.prototype, "resetPassword", null);
UsersController = __decorate([
    swagger_1.ApiTags('Users'),
    common_1.Controller('api/users'),
    __metadata("design:paramtypes", [users_service_1.UsersService])
], UsersController);
exports.UsersController = UsersController;
//# sourceMappingURL=users.controller.js.map