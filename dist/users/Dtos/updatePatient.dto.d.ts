export declare class UpdatePatientDTO {
    ID: string;
    name: string;
    sex: string;
    birthDate: Date;
    bloodType: string;
    height: number;
    weight: number;
    sugarLevel: number;
}
