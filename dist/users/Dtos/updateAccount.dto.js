"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateAccountDTO = void 0;
const openapi = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class UpdateAccountDTO {
    static _OPENAPI_METADATA_FACTORY() {
        return { password: { required: true, type: () => String, minLength: 8 }, name: { required: true, type: () => String, minLength: 5 }, phone: { required: true, type: () => String }, address: { required: true, type: () => String }, specification: { required: true, type: () => String }, summary: { required: true, type: () => String }, cityId: { required: true, type: () => Number }, classifications: { required: true, type: () => [Number] } };
    }
}
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.MinLength(8),
    __metadata("design:type", String)
], UpdateAccountDTO.prototype, "password", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    class_validator_1.MinLength(5),
    __metadata("design:type", String)
], UpdateAccountDTO.prototype, "name", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UpdateAccountDTO.prototype, "phone", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UpdateAccountDTO.prototype, "address", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], UpdateAccountDTO.prototype, "specification", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UpdateAccountDTO.prototype, "summary", void 0);
__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], UpdateAccountDTO.prototype, "cityId", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber({}, { each: true }),
    class_validator_1.ArrayMinSize(1),
    class_validator_1.ArrayMaxSize(3),
    __metadata("design:type", Array)
], UpdateAccountDTO.prototype, "classifications", void 0);
exports.UpdateAccountDTO = UpdateAccountDTO;
//# sourceMappingURL=updateAccount.dto.js.map