export declare class ConfirmAccountDTO {
    password: string;
    name: string;
    phone: string;
    address: string;
    specification: string;
    summary: string;
    cityId: number;
    classifications: number[];
}
