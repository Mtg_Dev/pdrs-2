declare class DoctorDto {
    name: string;
    image: string;
    email: string;
    phone: string;
}
declare class PharmacyDto {
    name: string;
}
export declare class CreateUserDto {
    email: string;
    password: string;
    doctor: DoctorDto;
    pharmacy: PharmacyDto;
}
export {};
