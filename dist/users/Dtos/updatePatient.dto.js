"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdatePatientDTO = void 0;
const openapi = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const constants_1 = require("../../utils/constants");
class UpdatePatientDTO {
    static _OPENAPI_METADATA_FACTORY() {
        return { ID: { required: true, type: () => String }, name: { required: true, type: () => String }, sex: { required: true, type: () => String }, birthDate: { required: true, type: () => Date }, bloodType: { required: true, type: () => String }, height: { required: true, type: () => Number }, weight: { required: true, type: () => Number }, sugarLevel: { required: true, type: () => Number } };
    }
}
__decorate([
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UpdatePatientDTO.prototype, "ID", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UpdatePatientDTO.prototype, "name", void 0);
__decorate([
    class_validator_1.IsIn(Object.values(constants_1.GENDER)),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UpdatePatientDTO.prototype, "sex", void 0);
__decorate([
    class_validator_1.IsDateString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", Date)
], UpdatePatientDTO.prototype, "birthDate", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UpdatePatientDTO.prototype, "bloodType", void 0);
__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], UpdatePatientDTO.prototype, "height", void 0);
__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], UpdatePatientDTO.prototype, "weight", void 0);
__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], UpdatePatientDTO.prototype, "sugarLevel", void 0);
exports.UpdatePatientDTO = UpdatePatientDTO;
//# sourceMappingURL=updatePatient.dto.js.map