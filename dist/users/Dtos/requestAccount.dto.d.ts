export declare class RequestAccountDTO {
    name: string;
    email: string;
    phone: string;
    specification: string;
    address: string;
}
