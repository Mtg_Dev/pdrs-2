export declare class UpdateAccountDTO {
    password: string;
    name: string;
    phone: string;
    address: string;
    specification: string;
    summary: string;
    cityId: number;
    classifications: number[];
}
