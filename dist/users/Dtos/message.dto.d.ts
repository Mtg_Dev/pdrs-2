export declare class MessageDTO {
    to: string;
    content: string;
}
