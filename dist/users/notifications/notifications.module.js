"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificationsModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const prescription_entity_1 = require("../../prescriptions/entities/prescription.entity");
const notifications_service_1 = require("./notifications.service");
const notifications_controller_1 = require("./notifications.controller");
const notification_entity_1 = require("../entities/notification.entity");
const prescriptionMedicins_entity_1 = require("../../prescriptions/entities/prescriptionMedicins.entity");
const account_request_entity_1 = require("../entities/account_request.entity");
const message_entity_1 = require("../entities/message.entity");
const reports_entity_1 = require("../../prescriptions/entities/reports.entity");
let NotificationsModule = class NotificationsModule {
};
NotificationsModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([
                prescription_entity_1.Prescription,
                notification_entity_1.Notification,
                prescription_entity_1.Prescription,
                prescriptionMedicins_entity_1.PrescriptionMedicine,
                account_request_entity_1.AccountRequest,
                message_entity_1.Message,
                reports_entity_1.Report,
            ]),
        ],
        providers: [notifications_service_1.NotificationsService],
        exports: [notifications_service_1.NotificationsService],
        controllers: [notifications_controller_1.NotificationsController],
    })
], NotificationsModule);
exports.NotificationsModule = NotificationsModule;
//# sourceMappingURL=notifications.module.js.map