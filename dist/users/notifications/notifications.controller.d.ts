import { NotificationsService } from './notifications.service';
export declare class NotificationsController {
    private notificationService;
    constructor(notificationService: NotificationsService);
    getNotificationsTypes(): {
        NewCondition: string;
        NewMedicine: string;
        NewMessage: string;
        NewRequest: string;
        NewReport: string;
    };
    getNotifications(): Promise<any[]>;
    readNotification(notification: {
        id: number;
    }): Promise<import("typeorm").UpdateResult>;
}
