"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificationsService = void 0;
const common_1 = require("@nestjs/common");
const prescriptionMedicins_entity_1 = require("../../prescriptions/entities/prescriptionMedicins.entity");
const notification_entity_1 = require("../entities/notification.entity");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const constants_1 = require("../../utils/constants");
const prescription_entity_1 = require("../../prescriptions/entities/prescription.entity");
const account_request_entity_1 = require("../entities/account_request.entity");
const message_entity_1 = require("../entities/message.entity");
const classificationsSeed_1 = require("../../utils/seed/classificationsSeed");
const reports_entity_1 = require("../../prescriptions/entities/reports.entity");
const mail_service_1 = require("../../services/mail.service");
let NotificationsService = class NotificationsService {
    constructor(NotificationsRepository, PrescriptionsRepository, PrescriptionMedicinesRepository, AccountRequestsRepository, MessagesRepository, ReportsRepository) {
        this.NotificationsRepository = NotificationsRepository;
        this.PrescriptionsRepository = PrescriptionsRepository;
        this.PrescriptionMedicinesRepository = PrescriptionMedicinesRepository;
        this.AccountRequestsRepository = AccountRequestsRepository;
        this.MessagesRepository = MessagesRepository;
        this.ReportsRepository = ReportsRepository;
    }
    getNotificationsTypes() {
        return constants_1.NOTIFICATIONS_TYPES;
    }
    markRead(id) {
        return this.NotificationsRepository.update(id, { isRead: true });
    }
    async addMedicine(medicine) {
        this.NotificationsRepository.save({
            tableId: medicine.id.toString(),
            type: constants_1.NOTIFICATIONS_TYPES.NewMedicine,
        });
    }
    async addCondition(prescription) {
        this.NotificationsRepository.save({
            tableId: prescription.id.toString(),
            type: constants_1.NOTIFICATIONS_TYPES.NewCondition,
        });
    }
    async addRequest(request) {
        this.NotificationsRepository.save({
            tableId: request.id.toString(),
            type: constants_1.NOTIFICATIONS_TYPES.NewRequest,
        });
    }
    async addReport(report) {
        this.NotificationsRepository.save({
            tableId: report.id.toString(),
            type: constants_1.NOTIFICATIONS_TYPES.NewReport,
        });
    }
    async addMessage(message) {
        this.NotificationsRepository.save({
            tableId: message.id.toString(),
            type: constants_1.NOTIFICATIONS_TYPES.NewMessage,
        });
    }
    async getNotifications() {
        const newNotifications = await this.NotificationsRepository.find({
            where: {
                isRead: false,
            },
        });
        let result = [];
        let i = 0;
        for (const notification of newNotifications) {
            result.push({
                id: notification.id,
                date: notification.date,
                type: notification.type,
            });
            if (notification.type === constants_1.NOTIFICATIONS_TYPES.NewCondition) {
                const prescription = await this.PrescriptionsRepository.findOne({
                    where: {
                        id: notification.tableId,
                    },
                    relations: ['classification', 'doctor'],
                });
                result[i].condition = prescription.tempCondition;
                result[i].classification = prescription.classification;
                result[i].doctor = prescription.doctor;
            }
            else if (notification.type === constants_1.NOTIFICATIONS_TYPES.NewMedicine) {
                const prescriptionMedicine = await this.PrescriptionMedicinesRepository.findOne({
                    where: {
                        id: notification.tableId,
                    },
                    relations: ['prescription', 'prescription.doctor'],
                });
                result[i].medicine = prescriptionMedicine.tempMedicine;
                result[i].doctor = prescriptionMedicine.prescription.doctor;
            }
            else if (notification.type === constants_1.NOTIFICATIONS_TYPES.NewRequest) {
                const accountRequest = await this.AccountRequestsRepository.findOne({
                    where: {
                        id: notification.tableId,
                    },
                });
                result[i].request = accountRequest;
            }
            else if (notification.type === constants_1.NOTIFICATIONS_TYPES.NewMessage) {
                const accountRequest = await this.MessagesRepository.findOne({
                    where: {
                        id: notification.tableId,
                    },
                    relations: ['doctor'],
                });
                result[i].message = accountRequest;
            }
            else if (notification.type === constants_1.NOTIFICATIONS_TYPES.NewReport) {
                result[i].report = await this.ReportsRepository.createQueryBuilder('report')
                    .where('report.id=:id', { id: notification.tableId })
                    .innerJoinAndSelect('report.user', 'user')
                    .innerJoinAndSelect('report.prescription', 'prescription')
                    .innerJoinAndSelect('prescription.prescriptionMedicine', 'prescriptionMedicine')
                    .innerJoinAndSelect('prescriptionMedicine.medicine', 'medicine')
                    .getMany();
            }
            i++;
        }
        return result;
    }
};
NotificationsService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(notification_entity_1.Notification)),
    __param(1, typeorm_1.InjectRepository(prescription_entity_1.Prescription)),
    __param(2, typeorm_1.InjectRepository(prescriptionMedicins_entity_1.PrescriptionMedicine)),
    __param(3, typeorm_1.InjectRepository(account_request_entity_1.AccountRequest)),
    __param(4, typeorm_1.InjectRepository(message_entity_1.Message)),
    __param(5, typeorm_1.InjectRepository(reports_entity_1.Report)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], NotificationsService);
exports.NotificationsService = NotificationsService;
//# sourceMappingURL=notifications.service.js.map