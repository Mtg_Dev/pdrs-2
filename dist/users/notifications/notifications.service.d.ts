import { PrescriptionMedicine } from 'src/prescriptions/entities/prescriptionMedicins.entity';
import { Notification } from '../entities/notification.entity';
import { Repository } from 'typeorm';
import { Prescription } from 'src/prescriptions/entities/prescription.entity';
import { AccountRequest } from '../entities/account_request.entity';
import { Message } from '../entities/message.entity';
import { Report } from 'src/prescriptions/entities/reports.entity';
export declare class NotificationsService {
    private NotificationsRepository;
    private PrescriptionsRepository;
    private PrescriptionMedicinesRepository;
    private AccountRequestsRepository;
    private MessagesRepository;
    private ReportsRepository;
    constructor(NotificationsRepository: Repository<Notification>, PrescriptionsRepository: Repository<Prescription>, PrescriptionMedicinesRepository: Repository<PrescriptionMedicine>, AccountRequestsRepository: Repository<AccountRequest>, MessagesRepository: Repository<Message>, ReportsRepository: Repository<Report>);
    getNotificationsTypes(): {
        NewCondition: string;
        NewMedicine: string;
        NewMessage: string;
        NewRequest: string;
        NewReport: string;
    };
    markRead(id: number): Promise<import("typeorm").UpdateResult>;
    addMedicine(medicine: PrescriptionMedicine): Promise<void>;
    addCondition(prescription: Prescription): Promise<void>;
    addRequest(request: AccountRequest): Promise<void>;
    addReport(report: Report): Promise<void>;
    addMessage(message: Message): Promise<void>;
    getNotifications(): Promise<any[]>;
}
