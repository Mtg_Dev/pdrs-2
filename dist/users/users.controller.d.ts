import { UsersService } from './users.service';
import { CreatePatientDTO } from './Dtos/createPatient.dto';
import { RequestAccountDTO } from './Dtos/requestAccount.dto';
import { MessageDTO } from './Dtos/message.dto';
import { CreateUserDto } from './Dtos/createUser.dto';
import { UpdateAccountDTO } from './Dtos/updateAccount.dto';
import { UserJwt } from './auth/auth.service';
import { ConfirmAccountDTO } from './Dtos/confirmAccount.dto';
import { UpdatePatientDTO } from './Dtos/updatePatient.dto';
import { User } from './entities/user.entity';
export declare class UsersController {
    private userService;
    constructor(userService: UsersService);
    getUsers({ page, size }: {
        page: any;
        size: any;
    }): Promise<User[]>;
    getDoctors({ page, size }: {
        page: any;
        size: any;
    }, query: string, countryId: number, cityId: number, classificationIds: number[]): Promise<import("./entities/doctor.entity").Doctor[]>;
    getProfile(user: UserJwt, id: string): Promise<User>;
    getPatient(id: string): Promise<import("./entities/patient.entity").Patient>;
    newUser(createUserDto: CreateUserDto): Promise<{
        doctor: {
            email: string;
            name: string;
            phone: string;
            user: {
                email: string;
                password: string;
                role: import("./entities/role.entity").Role;
            } & User;
        } & import("./entities/doctor.entity").Doctor;
        email: string;
        password: string;
        role: import("./entities/role.entity").Role;
        id: string;
        isBlocked: boolean;
        refreshToken: string;
        pharmacy: import("./entities/pharmacy.entity").Pharmacy;
        patient: import("./entities/patient.entity").Patient;
    }>;
    updatePhoto(file: any, id: string): Promise<import("./entities/doctor.entity").Doctor>;
    updateAccountInfo(user: UserJwt, updateAccountDTO: UpdateAccountDTO): Promise<User | ({
        userId: string;
    } & import("./entities/doctor.entity").Doctor)>;
    confirmAccountInfo(user: UserJwt, confimrAccountDTO: ConfirmAccountDTO): Promise<User | ({
        userId: string;
    } & import("./entities/doctor.entity").Doctor)>;
    newPatient(userId: any, createPatientDTO: CreatePatientDTO): Promise<import("./entities/patient.entity").Patient>;
    updatePatient(userId: any, updatePatientDTO: UpdatePatientDTO): Promise<import("./entities/patient.entity").Patient>;
    getDoctorPatients(id: string): Promise<any[]>;
    getPatients(id: string, name: string): Promise<{
        id: string;
        name: string;
        sex: string;
        birthDate: Date;
        patientInfo: import("./entities/patient_info.entity").PatientInfo;
        user: User;
        prescriptions: import("../prescriptions/entities/prescription.entity").Prescription[];
    }[]>;
    requestAccount(request: RequestAccountDTO): Promise<{
        name: string;
        email: string;
        phone: string;
        specification: string;
        address: string;
    } & import("./entities/account_request.entity").AccountRequest>;
    toggleBlock(user: {
        id: string;
    }): Promise<User>;
    sendMessage(message: MessageDTO, userId: string): Promise<{
        content: string;
        doctor: import("./entities/doctor.entity").Doctor;
    } & import("./entities/message.entity").Message>;
    respondMessage(message: MessageDTO): Promise<void>;
    getCountries(): Promise<import("./entities/country.entity").Country[]>;
    getCities(id: number): Promise<import("./entities/city.entity").City[]>;
    requestResetPassword(email: string): Promise<void>;
    resetPassword({ token, newPassword }: {
        token: string;
        newPassword: string;
    }): Promise<import("typeorm").UpdateResult>;
}
