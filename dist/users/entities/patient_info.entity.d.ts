import { Doctor } from './doctor.entity';
export declare class PatientInfo {
    id: string;
    bloodType: string;
    height: number;
    weight: number;
    sugarLevel: number;
    updatedAt: Date;
    doctor: Doctor;
}
