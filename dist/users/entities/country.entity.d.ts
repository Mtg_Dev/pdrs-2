import { City } from './city.entity';
export declare class Country {
    id: number;
    name: string;
    cities: City[];
}
