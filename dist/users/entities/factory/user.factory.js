"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_seeding_1 = require("typeorm-seeding");
const user_entity_1 = require("../user.entity");
typeorm_seeding_1.define(user_entity_1.User, (faker, context) => {
    let user = new user_entity_1.User();
    user.email = context.email;
    user.password = context.password;
    user.role = context.role;
    return user;
});
//# sourceMappingURL=user.factory.js.map