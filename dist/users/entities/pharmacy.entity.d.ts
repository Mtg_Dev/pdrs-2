import { User } from './user.entity';
export declare class Pharmacy {
    id: string;
    name: string;
    user: User;
}
