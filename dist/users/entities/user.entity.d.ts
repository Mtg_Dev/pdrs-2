import { Role } from './role.entity';
import { Doctor } from './doctor.entity';
import { Pharmacy } from './pharmacy.entity';
import { Patient } from './patient.entity';
export declare class User {
    id: string;
    email: string;
    password: string;
    isBlocked: boolean;
    refreshToken: string;
    role: Role;
    doctor: Doctor;
    pharmacy: Pharmacy;
    patient: Patient;
    static of(params: Partial<User>): User;
}
