import { User } from './user.entity';
import { PatientInfo } from './patient_info.entity';
import { Prescription } from '../../prescriptions/entities/prescription.entity';
export declare class Patient {
    id: string;
    name: string;
    sex: string;
    birthDate: Date;
    patientInfo: PatientInfo;
    user: User;
    prescriptions: Prescription[];
}
