"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var User_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const openapi = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const role_entity_1 = require("./role.entity");
const doctor_entity_1 = require("./doctor.entity");
const pharmacy_entity_1 = require("./pharmacy.entity");
const patient_entity_1 = require("./patient.entity");
let User = User_1 = class User {
    static of(params) {
        const entity = new User_1();
        Object.assign(entity, params);
        return entity;
    }
    static _OPENAPI_METADATA_FACTORY() {
        return { id: { required: true, type: () => String }, email: { required: true, type: () => String }, password: { required: true, type: () => String }, isBlocked: { required: true, type: () => Boolean }, refreshToken: { required: true, type: () => String }, role: { required: true, type: () => require("./role.entity").Role }, doctor: { required: true, type: () => require("./doctor.entity").Doctor }, pharmacy: { required: true, type: () => require("./pharmacy.entity").Pharmacy }, patient: { required: true, type: () => require("./patient.entity").Patient } };
    }
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], User.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ nullable: true, unique: true }),
    __metadata("design:type", String)
], User.prototype, "email", void 0);
__decorate([
    typeorm_1.Column({ nullable: true, select: false }),
    __metadata("design:type", String)
], User.prototype, "password", void 0);
__decorate([
    typeorm_1.Column({ nullable: true, default: false }),
    __metadata("design:type", Boolean)
], User.prototype, "isBlocked", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], User.prototype, "refreshToken", void 0);
__decorate([
    typeorm_1.ManyToOne(type => role_entity_1.Role, role => role.users),
    __metadata("design:type", role_entity_1.Role)
], User.prototype, "role", void 0);
__decorate([
    typeorm_1.OneToOne(type => doctor_entity_1.Doctor, doctor => doctor.user),
    __metadata("design:type", doctor_entity_1.Doctor)
], User.prototype, "doctor", void 0);
__decorate([
    typeorm_1.OneToOne(type => pharmacy_entity_1.Pharmacy, pharmacy => pharmacy.user),
    __metadata("design:type", pharmacy_entity_1.Pharmacy)
], User.prototype, "pharmacy", void 0);
__decorate([
    typeorm_1.OneToOne(type => patient_entity_1.Patient, patient => patient.user),
    __metadata("design:type", patient_entity_1.Patient)
], User.prototype, "patient", void 0);
User = User_1 = __decorate([
    typeorm_1.Entity()
], User);
exports.User = User;
//# sourceMappingURL=user.entity.js.map