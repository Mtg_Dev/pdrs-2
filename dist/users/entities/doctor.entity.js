"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Doctor = void 0;
const openapi = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const user_entity_1 = require("./user.entity");
const classification_entity_1 = require("../../prescriptions/entities/classification.entity");
const prescription_entity_1 = require("../../prescriptions/entities/prescription.entity");
const city_entity_1 = require("./city.entity");
let Doctor = class Doctor {
    static _OPENAPI_METADATA_FACTORY() {
        return { id: { required: true, type: () => String }, name: { required: true, type: () => String }, image: { required: true, type: () => String }, email: { required: true, type: () => String }, phone: { required: true, type: () => String }, address: { required: true, type: () => String }, specification: { required: true, type: () => String }, summary: { required: true, type: () => String }, userId: { required: true, type: () => String }, user: { required: true, type: () => require("./user.entity").User }, classifications: { required: true, type: () => [require("../../prescriptions/entities/classification.entity").Classification] }, prescriptions: { required: true, type: () => [require("../../prescriptions/entities/prescription.entity").Prescription] }, city: { required: true, type: () => require("./city.entity").City } };
    }
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Doctor.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Doctor.prototype, "name", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Doctor.prototype, "image", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Doctor.prototype, "email", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Doctor.prototype, "phone", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Doctor.prototype, "address", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Doctor.prototype, "specification", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Doctor.prototype, "summary", void 0);
__decorate([
    typeorm_1.Column({ nullable: false }),
    __metadata("design:type", String)
], Doctor.prototype, "userId", void 0);
__decorate([
    typeorm_1.OneToOne(type => user_entity_1.User, {
        onDelete: 'CASCADE',
    }),
    typeorm_1.JoinColumn({ name: 'userId' }),
    __metadata("design:type", user_entity_1.User)
], Doctor.prototype, "user", void 0);
__decorate([
    typeorm_1.ManyToMany(type => classification_entity_1.Classification, c => c.doctors, {
        nullable: true,
        onDelete: 'CASCADE',
    }),
    typeorm_1.JoinTable({ name: 'doctors_classifications' }),
    __metadata("design:type", Array)
], Doctor.prototype, "classifications", void 0);
__decorate([
    typeorm_1.OneToMany(type => prescription_entity_1.Prescription, p => p.doctor),
    __metadata("design:type", Array)
], Doctor.prototype, "prescriptions", void 0);
__decorate([
    typeorm_1.ManyToOne(type => city_entity_1.City),
    __metadata("design:type", city_entity_1.City)
], Doctor.prototype, "city", void 0);
Doctor = __decorate([
    typeorm_1.Entity()
], Doctor);
exports.Doctor = Doctor;
//# sourceMappingURL=doctor.entity.js.map