export declare class Notification {
    id: number;
    date: Date;
    isRead: boolean;
    type: string;
    tableId: string;
}
