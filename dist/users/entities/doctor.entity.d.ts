import { User } from './user.entity';
import { Classification } from '../../prescriptions/entities/classification.entity';
import { Prescription } from '../../prescriptions/entities/prescription.entity';
import { City } from './city.entity';
export declare class Doctor {
    id: string;
    name: string;
    image: string;
    email: string;
    phone: string;
    address: string;
    specification: string;
    summary: string;
    userId: string;
    user: User;
    classifications: Classification[];
    prescriptions: Prescription[];
    city: City;
}
