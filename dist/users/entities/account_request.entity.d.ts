export declare class AccountRequest {
    id: number;
    name: string;
    email: string;
    phone: string;
    specification: string;
    address: string;
}
