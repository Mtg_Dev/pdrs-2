import { Doctor } from './doctor.entity';
export declare class Message {
    id: number;
    content: string;
    doctor: Doctor;
}
