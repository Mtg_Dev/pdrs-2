import { Country } from './country.entity';
export declare class City {
    id: number;
    name: string;
    country: Country;
}
