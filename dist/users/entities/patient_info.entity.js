"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PatientInfo = void 0;
const openapi = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const doctor_entity_1 = require("./doctor.entity");
let PatientInfo = class PatientInfo {
    static _OPENAPI_METADATA_FACTORY() {
        return { id: { required: true, type: () => String }, bloodType: { required: true, type: () => String }, height: { required: true, type: () => Number }, weight: { required: true, type: () => Number }, sugarLevel: { required: true, type: () => Number }, updatedAt: { required: true, type: () => Date }, doctor: { required: true, type: () => require("./doctor.entity").Doctor } };
    }
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], PatientInfo.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], PatientInfo.prototype, "bloodType", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], PatientInfo.prototype, "height", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], PatientInfo.prototype, "weight", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], PatientInfo.prototype, "sugarLevel", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({ nullable: true }),
    __metadata("design:type", Date)
], PatientInfo.prototype, "updatedAt", void 0);
__decorate([
    typeorm_1.ManyToOne(type => doctor_entity_1.Doctor, { onDelete: 'SET NULL' }),
    typeorm_1.JoinColumn(),
    __metadata("design:type", doctor_entity_1.Doctor)
], PatientInfo.prototype, "doctor", void 0);
PatientInfo = __decorate([
    typeorm_1.Entity()
], PatientInfo);
exports.PatientInfo = PatientInfo;
//# sourceMappingURL=patient_info.entity.js.map