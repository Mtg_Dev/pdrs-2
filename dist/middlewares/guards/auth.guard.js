"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Auth = void 0;
const common_1 = require("@nestjs/common");
const roles_guard_1 = require("./roles.guard");
const swagger_1 = require("@nestjs/swagger");
const jwt_auth_guard_1 = require("../../users/auth/jwt-auth-guard");
function Auth(...roles) {
    return common_1.applyDecorators(common_1.SetMetadata('roles', roles), common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard, roles_guard_1.RolesGuard), swagger_1.ApiCookieAuth());
}
exports.Auth = Auth;
//# sourceMappingURL=auth.guard.js.map