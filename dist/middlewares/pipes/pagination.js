"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiPagination = void 0;
const swagger_1 = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const decorators_1 = require("../../utils/decorators");
function ApiPagination() {
    return common_1.applyDecorators(swagger_1.ApiQuery({
        name: 'size',
        type: Number,
        required: false,
    }), swagger_1.ApiQuery({
        name: 'page',
        type: Number,
        required: false,
    }));
}
exports.ApiPagination = ApiPagination;
//# sourceMappingURL=pagination.js.map