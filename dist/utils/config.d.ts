import { TypeOrmModuleOptions } from '@nestjs/typeorm';
declare class ConfigService {
    private env;
    constructor(env: {
        [k: string]: string | undefined;
    });
    private getValue;
    ensureValues(keys: string[]): this;
    getJwtSecret(): string;
    getEmail(): string;
    getEmailCredentials(): {
        clientID: string;
        clientSecret: string;
        accessToken: string;
        refreshToken: string;
    };
    getPort(): string;
    isProduction(): boolean;
    isTest(): boolean;
    getTypeOrmConfig(): TypeOrmModuleOptions;
}
declare const configService: ConfigService;
export { configService };
