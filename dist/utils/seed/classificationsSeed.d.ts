import { Factory } from 'typeorm-seeding';
import { Connection } from 'typeorm';
export declare const classifications: ({
    name: string;
    conditions: {
        name: string;
    }[];
    isPrivate?: undefined;
} | {
    name: string;
    isPrivate: boolean;
    conditions: {
        name: string;
    }[];
})[];
export declare const medicins: {
    name: string;
}[];
export declare function SeedClassifications(factory: Factory, connection: Connection): Promise<any>;
