"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const classificationsSeed_1 = require("./classificationsSeed");
const usersSeed_1 = require("./usersSeed");
const prescriptionSeed_1 = require("./prescriptionSeed");
const countriesSeed_1 = require("./countriesSeed");
const remove_date_1 = require("./remove-date");
class CreateSeed {
    async run(factory, connection) {
        await remove_date_1.removeData(connection);
        await countriesSeed_1.SeedCountries(factory, connection);
        await classificationsSeed_1.SeedClassifications(factory, connection);
        await usersSeed_1.SeedUsers(factory, connection);
        await prescriptionSeed_1.SeedPrescriptions(factory, connection);
    }
}
exports.default = CreateSeed;
//# sourceMappingURL=index.seed.js.map