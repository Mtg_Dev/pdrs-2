"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SeedUsers = void 0;
const typeorm_1 = require("typeorm");
const role_entity_1 = require("../../users/entities/role.entity");
const constants_1 = require("../constants");
const user_entity_1 = require("../../users/entities/user.entity");
const doctor_entity_1 = require("../../users/entities/doctor.entity");
const pharmacy_entity_1 = require("../../users/entities/pharmacy.entity");
const patient_entity_1 = require("../../users/entities/patient.entity");
const patient_info_entity_1 = require("../../users/entities/patient_info.entity");
const classificationsSeed_1 = require("./classificationsSeed");
const classification_entity_1 = require("../../prescriptions/entities/classification.entity");
const helpers_1 = require("../helpers");
const argon2 = require("argon2");
const city_entity_1 = require("../../users/entities/city.entity");
const message_entity_1 = require("../../users/entities/message.entity");
const account_request_entity_1 = require("../../users/entities/account_request.entity");
const notification_entity_1 = require("../../users/entities/notification.entity");
const faker = require("faker");
const roles = [
    { id: '101', name: constants_1.ROLES.Admin },
    { id: '102', name: constants_1.ROLES.Doctor },
    { id: '103', name: constants_1.ROLES.Pharmacy },
    { id: '104', name: constants_1.ROLES.User },
];
const users = [
    {
        email: 'mtg@gmail.com',
        password: 'mtgmtgmtg',
        role: roles[0],
    },
    {
        email: 'ahmad@gmail.com',
        password: '123123123',
        role: roles[1],
        info: {
            name: 'Ahmad Ghazal',
            image: null,
            email: 'ahmad@gmail.com',
            phone: '0963 969737645',
        },
    },
    {
        email: 'mourad79@gmail.com',
        password: '123123123',
        role: roles[1],
        info: {
            name: 'Mourad Niazi',
            image: null,
            email: 'mourad79@gmail.com',
            phone: '0963 912341231',
        },
    },
    {
        email: 'samer@gmail.com',
        password: '123123123',
        role: roles[2],
        info: {
            name: 'Al-Fateh Pharmacy',
        },
    },
];
const getRandomId = () => {
    let id = '';
    for (let i = 0; i < 4; i++) {
        id = id.concat(faker.random.number({ min: 100, max: 999 }).toString());
    }
    return id;
};
async function SeedUsers(factory, connection) {
    console.log('  Seeding Users');
    const rolesRepository = connection.getRepository(role_entity_1.Role);
    const usersRepository = connection.getRepository(user_entity_1.User);
    const doctorsRepository = connection.getRepository(doctor_entity_1.Doctor);
    const pharmaciesRepository = connection.getRepository(pharmacy_entity_1.Pharmacy);
    const patientsRepository = connection.getRepository(patient_entity_1.Patient);
    const patientsInfoRepository = connection.getRepository(patient_info_entity_1.PatientInfo);
    const classificationRepository = connection.getRepository(classification_entity_1.Classification);
    const cityRepository = connection.getRepository(city_entity_1.City);
    const messageRepository = connection.getRepository(message_entity_1.Message);
    const accountRequestRepository = connection.getRepository(account_request_entity_1.AccountRequest);
    const notificationRepository = connection.getRepository(notification_entity_1.Notification);
    let doctors = [];
    let patients = [];
    const classifications = await classificationRepository.find({
        where: {
            name: typeorm_1.In(classificationsSeed_1.classifications.map(c => c.name)),
        },
    });
    await rolesRepository.save(roles);
    for (const user of users) {
        const role = await rolesRepository.findOne(user.role.id);
        let password = user.password;
        if (password)
            password = await argon2.hash(user.password);
        let createdUser = null;
        if (user.email)
            createdUser = await factory(user_entity_1.User)(Object.assign(Object.assign({}, user), { password,
                role })).create();
        if (user.role.name === constants_1.ROLES.Doctor) {
            const city = helpers_1.getRandomItem(await cityRepository.find({ take: 20 }));
            const doctor = await doctorsRepository.save(Object.assign(Object.assign({}, user.info), { user: createdUser, city }));
            doctors.push(doctor);
            doctor.classifications = helpers_1.getRandomItem(classifications, 2, false);
            await doctorsRepository.save(doctor);
        }
        else if (user.role.name === constants_1.ROLES.Pharmacy) {
            await pharmaciesRepository.save(Object.assign(Object.assign({}, user.info), { user: createdUser }));
        }
    }
    const cities = await cityRepository.find({ take: 20 });
    for (let i = 0; i < helpers_1.getRandomRange(10, 15); i++) {
        const firstName = faker.name.firstName();
        const lastName = faker.name.lastName();
        const email = faker.internet.email(firstName, lastName, 'gmail.com');
        const createdUser = await factory(user_entity_1.User)({
            password: await argon2.hash(faker.internet.password(8)),
            email,
            role: roles[1],
        }).create();
        const city = helpers_1.getRandomItem(cities);
        const doctor = await doctorsRepository.save({
            user: createdUser,
            email,
            city,
            address: faker.address.streetAddress(),
            classifications: helpers_1.getRandomItem(classifications, 2, false),
            phone: faker.phone.phoneNumber(),
            specification: 'Heart Surgon',
            summary: faker.lorem.paragraph(2),
            name: `${firstName} ${lastName}`,
        });
        doctors.push(doctor);
    }
    for (let i = 0; i < helpers_1.getRandomRange(10, 15); i++) {
        const patientInfo = await patientsInfoRepository.save({
            doctor: helpers_1.getRandomItem(doctors),
            bloodType: faker.random.arrayElement(['A', 'B', 'O', 'AB']),
            height: faker.random.number({ min: 150, max: 190 }),
            weight: faker.random.number({ min: 50, max: 90 }),
            sugarLevel: faker.random.number({ min: 70, max: 110 }),
        });
        const createdPatient = await patientsRepository.save({
            id: getRandomId(),
            birthDate: faker.date.between('1980', '2003'),
            sex: faker.random.arrayElement([constants_1.GENDER.Male, constants_1.GENDER.Female]),
            name: faker.name.findName(),
            patientInfo,
        });
        patients.push(createdPatient);
    }
    const messages = [
        { content: ' Hello how are you doing ?? ', doctor: helpers_1.getRandomItem(doctors) },
        {
            content: ' I want to contact you for something would you mind calling me  ?? ',
            doctor: helpers_1.getRandomItem(doctors),
        },
    ];
    const createdMsgs = await messageRepository.save(messages);
    for (const msg of createdMsgs) {
        await notificationRepository.save({
            type: constants_1.NOTIFICATIONS_TYPES.NewMessage,
            tableId: msg.id.toString(),
        });
    }
    const requests = [
        {
            name: 'Kareem Fallaha',
            email: 'dr.fallaha@email.com',
            address: 'Cairo Egypt',
            phone: '935 98115331',
            specification: 'Dentist',
        },
        {
            name: 'Mazen Sabagh',
            email: 'mazen@email.com',
            address: 'USA New York',
            phone: '911 94772324',
            specification: 'Cosmetics Doctor',
        },
        {
            name: 'Hamadan Khaled',
            email: 'hamdan@email.com',
            address: 'Damascus Syria',
            phone: '963 988422423',
            specification: 'Al Hadi Pharmacy',
        },
        {
            name: 'Sami Tahaan',
            email: 'samitah@email.com',
            address: 'UK London',
            phone: '954 94772324',
            specification: 'General Surgon',
        },
    ];
    const createdRequests = await accountRequestRepository.save(requests);
    for (const request of createdRequests) {
        await notificationRepository.save({
            type: constants_1.NOTIFICATIONS_TYPES.NewRequest,
            tableId: request.id.toString(),
        });
    }
}
exports.SeedUsers = SeedUsers;
//# sourceMappingURL=usersSeed.js.map