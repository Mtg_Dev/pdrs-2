"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SeedPrescriptions = void 0;
const role_entity_1 = require("../../users/entities/role.entity");
const constants_1 = require("../constants");
const user_entity_1 = require("../../users/entities/user.entity");
const doctor_entity_1 = require("../../users/entities/doctor.entity");
const pharmacy_entity_1 = require("../../users/entities/pharmacy.entity");
const patient_entity_1 = require("../../users/entities/patient.entity");
const patient_info_entity_1 = require("../../users/entities/patient_info.entity");
const classification_entity_1 = require("../../prescriptions/entities/classification.entity");
const medicins_entity_1 = require("../../prescriptions/entities/medicins.entity");
const condition_entity_1 = require("../../prescriptions/entities/condition.entity");
const prescription_entity_1 = require("../../prescriptions/entities/prescription.entity");
const prescriptionMedicins_entity_1 = require("../../prescriptions/entities/prescriptionMedicins.entity");
const notification_entity_1 = require("../../users/entities/notification.entity");
const helpers_1 = require("../helpers");
const tempConditions = [
    'Heart Attack Type C',
    'High Sugar Level',
    'Kidney Collapse',
    'Critical Brain Damage',
];
const notes = [
    'The Patient has previously suffered from a trauma',
    'please note that the patient occasionally takes some painkillers',
    'although the accident didnt do much visible damage but the brain suffered from an intense shock',
    'the patinet should not be given any extra sleeping pills',
];
const tempMedicins = ['Vardinal 420', 'Tramaroll', 'Krapreen 710', 'Dramazol '];
async function SeedPrescriptions(factory, connection) {
    console.log('  Seeding Prescriptions');
    const rolesRepository = connection.getRepository(role_entity_1.Role);
    const usersRepository = connection.getRepository(user_entity_1.User);
    const doctorsRepository = connection.getRepository(doctor_entity_1.Doctor);
    const pharmaciesRepository = connection.getRepository(pharmacy_entity_1.Pharmacy);
    const patientsRepository = connection.getRepository(patient_entity_1.Patient);
    const patientsInfoRepository = connection.getRepository(patient_info_entity_1.PatientInfo);
    const classificationRepository = connection.getRepository(classification_entity_1.Classification);
    const conditionRepository = connection.getRepository(condition_entity_1.Condition);
    const medicineRepository = connection.getRepository(medicins_entity_1.Medicine);
    const prescriptionRepository = connection.getRepository(prescription_entity_1.Prescription);
    const notificationRepository = connection.getRepository(notification_entity_1.Notification);
    const prescriptionMedicineRepository = connection.getRepository(prescriptionMedicins_entity_1.PrescriptionMedicine);
    const classifications = await classificationRepository.find();
    const medicins = await medicineRepository.find();
    const doctors = await doctorsRepository.find();
    const pharmacies = await pharmaciesRepository.find();
    const patients = await patientsRepository.find();
    for (const patient of patients.slice(0, patients.length - 1)) {
        for (let i = 0; i < helpers_1.getRandomRange(5, 15); i++) {
            const selectedDoctor = helpers_1.getRandomItem(doctors);
            const selectedPharmacy = helpers_1.getRandomItem(pharmacies);
            const selectedClassification = helpers_1.getRandomItem(classifications);
            let tempCondition = '';
            let note = '';
            if (helpers_1.getRandomPossibility(10)) {
                tempCondition = helpers_1.getRandomItem(tempConditions);
            }
            if (helpers_1.getRandomPossibility(10)) {
                note = helpers_1.getRandomItem(notes);
            }
            const selectedCondition = helpers_1.getRandomItem(await conditionRepository.find({
                where: {
                    classification: selectedClassification,
                },
            }));
            const prescription = await prescriptionRepository.save(Object.assign(Object.assign(Object.assign({ classification: selectedClassification }, (!tempCondition
                ? { condition: selectedCondition }
                : { tempCondition })), (note && { note })), { doctor: selectedDoctor, patientId: patient.id }));
            if (tempCondition)
                await notificationRepository.save({
                    type: constants_1.NOTIFICATIONS_TYPES.NewCondition,
                    tableId: prescription.id.toString(),
                });
            let selectedMedicins = helpers_1.getRandomItem(medicins, helpers_1.getRandomRange(2, 5), false);
            if (helpers_1.getRandomPossibility(10))
                selectedMedicins.push({
                    tempMedicine: helpers_1.getRandomItem(tempMedicins),
                });
            const prescriptionMedicins = await prescriptionMedicineRepository.save(selectedMedicins.map(medicine => (Object.assign(Object.assign({}, (medicine.tempMedicine
                ? { tempMedicine: medicine.tempMedicine }
                : { medicine })), { prescription, isBold: helpers_1.getRandomPossibility(15), isChronic: helpers_1.getRandomPossibility(7), pharmacy: helpers_1.getRandomPossibility(85) ? selectedPharmacy : null }))));
            for (const pm of prescriptionMedicins) {
                if (pm.tempMedicine)
                    await notificationRepository.save({
                        type: constants_1.NOTIFICATIONS_TYPES.NewMedicine,
                        tableId: pm.id.toString(),
                    });
            }
        }
    }
}
exports.SeedPrescriptions = SeedPrescriptions;
//# sourceMappingURL=prescriptionSeed.js.map