"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SeedClassifications = exports.medicins = exports.classifications = void 0;
const classification_entity_1 = require("../../prescriptions/entities/classification.entity");
const condition_entity_1 = require("../../prescriptions/entities/condition.entity");
const medicins_entity_1 = require("../../prescriptions/entities/medicins.entity");
exports.classifications = [
    {
        name: 'Heart',
        conditions: [
            { name: 'Abnormal heart rhythms' },
            { name: 'Aorta disease' },
            { name: 'Congenital heart disease' },
            { name: 'Coronary artery disease ' },
        ],
    },
    {
        name: 'Bones',
        conditions: [
            { name: 'Bone cancer' },
            { name: 'Curvature of the spine' },
            { name: 'Broken Arm' },
            { name: 'Broken Leg' },
            { name: 'Broken Skull' },
            { name: 'arthritis' },
        ],
    },
    {
        name: 'Brain',
        conditions: [
            { name: "Alzheimer's Disease" },
            { name: 'Dementias' },
            { name: 'Brain Cancer' },
            { name: 'Mental Disorders' },
            { name: "Parkinson's and Other Movement Disorders" },
            { name: 'Epilepsy and Other Seizure Disorders' },
            { name: 'Stroke and Transient Ischemic Attack' },
        ],
    },
    {
        name: 'Eye',
        conditions: [
            { name: 'Eyestrain' },
            { name: 'Red Eyes' },
            { name: 'Night Blindness' },
        ],
    },
    {
        name: 'Stomach',
        conditions: [
            { name: 'Gastroesophageal Reflux Disease' },
            { name: 'Celiac Disease' },
            { name: "Crohn's Disease" },
            { name: 'Ulcerative Colitis' },
            { name: 'Irritable Bowel Syndrome' },
        ],
    },
    {
        name: 'Tooth',
        conditions: [
            { name: 'Cavities' },
            { name: 'Periodontitis' },
            { name: 'Cracked or broken teeth' },
            { name: 'Sensitive teeth' },
            { name: 'Oral cancer' },
        ],
    },
    {
        name: 'Kidney',
        conditions: [
            { name: 'Chronic kidney disease' },
            { name: 'Kidney stones' },
            { name: 'Glomerulonephritis' },
            { name: 'Polycystic kidney disease' },
            { name: 'Urinary tract infections' },
        ],
    },
    {
        name: 'Lungs',
        conditions: [
            { name: 'Asthma' },
            { name: 'Lung cancer' },
            { name: 'Covid-19' },
            { name: 'Lung infection (pneumonia)' },
        ],
    },
    {
        name: 'Ear',
        conditions: [
            { name: 'Otosclerosis' },
            { name: 'Menieres Disease' },
            { name: 'Ear Infections' },
            { name: "Swimmer's ear (otitis externa)" },
        ],
    },
    {
        name: 'Sex',
        isPrivate: true,
        conditions: [
            { name: 'Desire disorders' },
            { name: 'Arousal disorders' },
            { name: 'Orgasm disorders' },
            { name: 'Pain disorders' },
        ],
    },
];
exports.medicins = [
    { name: 'Sitamol' },
    { name: 'Panadol' },
    { name: 'Profien' },
    { name: 'Shfazien-Forte' },
    { name: 'Citalopram' },
    { name: 'Adderall' },
    { name: 'Trazodone' },
    { name: 'Metformin' },
    { name: 'Hydrochlorothiazide' },
    { name: 'Azithromycin' },
    { name: 'Ibuprofen' },
    { name: 'Cymbalta' },
    { name: 'Doxycycline' },
    { name: 'Lorazepam' },
];
async function SeedClassifications(factory, connection) {
    console.log('  Seeding Classifications/Conditions/Medicins');
    const classificationsRepository = connection.getRepository(classification_entity_1.Classification);
    const conditionsRepository = connection.getRepository(condition_entity_1.Condition);
    const medicinsRepository = connection.getRepository(medicins_entity_1.Medicine);
    for (const c of exports.classifications) {
        const createdClassification = await classificationsRepository.save({
            name: c.name,
            isPrivate: c.isPrivate,
            image: `assets\\images\\classifications\\${c.name
                .toLowerCase()
                .replace(' ', '-')}.svg`,
        });
        await conditionsRepository.save(c.conditions.map(condition => (Object.assign(Object.assign({}, condition), { classification: createdClassification }))));
    }
    const createdMedicins = await medicinsRepository.save(exports.medicins);
    createdMedicins[0].contradictingMedicins = [
        createdMedicins[1],
        createdMedicins[4],
    ];
    createdMedicins[2].contradictingMedicins = [
        createdMedicins[1],
        createdMedicins[5],
    ];
    await medicinsRepository.save([createdMedicins[0], createdMedicins[2]]);
}
exports.SeedClassifications = SeedClassifications;
//# sourceMappingURL=classificationsSeed.js.map