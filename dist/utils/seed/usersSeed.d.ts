import { Factory } from 'typeorm-seeding';
import { Connection } from 'typeorm';
export declare function SeedUsers(factory: Factory, connection: Connection): Promise<any>;
