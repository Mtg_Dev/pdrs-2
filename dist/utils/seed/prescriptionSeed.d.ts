import { Factory } from 'typeorm-seeding';
import { Connection } from 'typeorm';
export declare function SeedPrescriptions(factory: Factory, connection: Connection): Promise<any>;
