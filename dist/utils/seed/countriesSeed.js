"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SeedCountries = void 0;
const country_entity_1 = require("../../users/entities/country.entity");
const city_entity_1 = require("../../users/entities/city.entity");
const helpers_1 = require("../../utils/helpers");
const countriesData = {
    Syria: [
        'Aleppo',
        'Damascus',
        'Homs',
        'Latakia',
        'Dūmā',
        'Ḩamāh',
        'Dayr az Zawr',
        'Ar Raqqah',
        'Ţarţūs',
        'Dar‘ā',
        'Idlib',
        'Al Ḩasakah',
        'Manbij',
        'Al Qāmishlī',
        'Ath Thawrah',
        'As Suwaydā’',
        'Al Qunayţirah',
        'Ālbū Kamāl',
        'Tadmur',
        'An Nabk',
        'I‘zāz',
    ],
    Egypt: [
        'Cairo',
        'Alexandria',
        'Giza',
        'Ismailia',
        'Port Said',
        'Luxor',
        'Sūhāj',
        'Al Manşūrah',
        'Suez',
        'Damanhūr',
        'Al Minyā',
        'Banī Suwayf',
        'Asyūţ',
        'Ţanţā',
        'Al Fayyūm',
        'Aswān',
        'Kawm Umbū',
        'Qinā',
        'Damietta',
        'Az Zaqāzīq',
        'Mallawī',
        'Rosetta',
        'Shibīn al Kawm',
        'Al ‘Arīsh',
        'Banhā',
        'Al Ghardaqah',
        'Samālūţ',
        'Kafr ash Shaykh',
        'Jirjā',
        'Marsá Maţrūḩ',
        'Al Khārijah',
        'Aţ Ţūr',
        'Isnā',
        'Banī Mazār',
        'Safājā',
        'Sīwah',
        'Aḑ Ḑab‘ah',
        'Al ‘Alamayn',
        'As Sallūm',
        'Qaşr al Farāfirah',
        'Al Qaşr',
        'Barnīs',
    ],
    Japan: [
        'Tokyo',
        'Ōsaka',
        'Yokohama',
        'Nagoya',
        'Fukuoka',
        'Sapporo',
        'Sendai',
        'Hiroshima',
        'Kyōto',
        'Kōbe',
        'Kawanakajima',
        'Kitaku',
        'Hamamatsu',
        'Naha',
        'Okayama',
        'Kumamoto',
        'Shizuoka',
        'Utsunomiya',
        'Nagano',
        'Hachiōji',
        'Niigata',
        'Kagoshima',
        'Kanazawa',
        'Ōtsu',
        'Matsuyama',
        'Tsu',
        'Ōita',
        'Tokushima',
        'Wakayama',
        'Nagasaki',
        'Gifu',
        'Iwaki',
        'Asahikawa',
        'Mito',
        'Maebashi',
        'Tamuramachi-moriyama',
        'Kawagoe',
        'Kōchi',
        'Takamatsu',
        'Toyama',
        'Miyazaki',
        'Akita',
        'Hakodate',
        'Aomori',
        'Morioka',
        'Fukushima',
        'Yamagata',
        'Fukui',
        'Shinozaki',
        'Hachimanchō',
        'Sato',
        'Minamiōzuma',
        'Ashino',
        'Kōfu',
        'Kure',
        'Naka',
        'Ōbiraki',
        'Takaoka',
        'Beppuchō',
        'Takaoka',
        'Matsue',
        'Muroran',
        'Tottori',
        'Otaru',
        'Kōenchō',
        'Sakata',
        'Saitama',
        'Chiba',
        'Nara',
        'Saga',
        'Tottori',
        'Yamaguchi',
        'Tsuruoka',
        'Maizuru',
        'Kaneyama',
    ],
};
async function SeedCountries(factory, connection) {
    console.log('  Seeding Countries/Cities');
    const countryRepository = connection.getRepository(country_entity_1.Country);
    const cityRepository = connection.getRepository(city_entity_1.City);
    const data = countriesData;
    await countryRepository.delete({});
    try {
        let countries = Object.keys(data);
        for (const country of countries) {
            await countryRepository.insert({
                name: country,
            });
        }
        countries = await countryRepository.find();
        for (const country of countries) {
            const cities = data[country.name];
            for (const cityChunk of helpers_1.chunkArray(cities, 150)) {
                await cityRepository.insert(cityChunk.map(city => ({
                    country,
                    name: city,
                })));
            }
        }
    }
    catch (err) {
        console.info(err);
    }
    finally {
    }
}
exports.SeedCountries = SeedCountries;
//# sourceMappingURL=countriesSeed.js.map