import { Factory } from 'typeorm-seeding';
import { Connection } from 'typeorm';
export declare function SeedCountries(factory: Factory, connection: Connection): Promise<any>;
