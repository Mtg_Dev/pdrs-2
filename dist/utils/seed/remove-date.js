"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.removeData = void 0;
const classification_entity_1 = require("../../prescriptions/entities/classification.entity");
const condition_entity_1 = require("../../prescriptions/entities/condition.entity");
const medicins_entity_1 = require("../../prescriptions/entities/medicins.entity");
const role_entity_1 = require("../../users/entities/role.entity");
const user_entity_1 = require("../../users/entities/user.entity");
const patient_info_entity_1 = require("../../users/entities/patient_info.entity");
const prescription_entity_1 = require("../../prescriptions/entities/prescription.entity");
const prescriptionMedicins_entity_1 = require("../../prescriptions/entities/prescriptionMedicins.entity");
const notification_entity_1 = require("../../users/entities/notification.entity");
async function removeData(connection) {
    console.log(' Removing Data ');
    const classificationsRepository = connection.getRepository(classification_entity_1.Classification);
    const conditionsRepository = connection.getRepository(condition_entity_1.Condition);
    const medicinsRepository = connection.getRepository(medicins_entity_1.Medicine);
    const prescriptionRepository = connection.getRepository(prescription_entity_1.Prescription);
    const prescriptionMedicineRepository = connection.getRepository(prescriptionMedicins_entity_1.PrescriptionMedicine);
    const rolesRepository = connection.getRepository(role_entity_1.Role);
    const usersRepository = connection.getRepository(user_entity_1.User);
    const notificationsRepository = connection.getRepository(notification_entity_1.Notification);
    const patientsInfoRepository = connection.getRepository(patient_info_entity_1.PatientInfo);
    await notificationsRepository.delete({});
    await prescriptionMedicineRepository.delete({});
    await prescriptionRepository.delete({});
    await medicinsRepository.delete({});
    await classificationsRepository.delete({});
    await usersRepository.delete({});
    await rolesRepository.delete({});
    await patientsInfoRepository.delete({});
}
exports.removeData = removeData;
//# sourceMappingURL=remove-date.js.map