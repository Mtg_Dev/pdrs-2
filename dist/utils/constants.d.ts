export declare const ROLES: {
    Admin: string;
    Doctor: string;
    Pharmacy: string;
    User: string;
};
export declare const UPLOAD_DIR: {
    Classifications: string;
    Profiles: string;
};
export declare const GENDER: {
    Male: string;
    Female: string;
};
export declare const TOKENS_TYPES: {
    ResetPassword: string;
};
export declare const NOTIFICATIONS_TYPES: {
    NewCondition: string;
    NewMedicine: string;
    NewMessage: string;
    NewRequest: string;
    NewReport: string;
};
export declare const ROOT_DIR: string;
declare const _default: {
    ROLES: {
        Admin: string;
        Doctor: string;
        Pharmacy: string;
        User: string;
    };
    UPLOAD_DIR: {
        Classifications: string;
        Profiles: string;
    };
    ROOT_DIR: string;
};
export default _default;
