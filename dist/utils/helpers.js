"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.removeFile = exports.autoMap = exports.chunkArray = exports.getRandomPossibility = exports.getRandomRange = exports.getRandomItem = exports.generatePassword = void 0;
const fs = require("fs");
const constants_1 = require("./constants");
function generatePassword() {
    var length = 8, charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', retVal = '';
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}
exports.generatePassword = generatePassword;
function getRandomItem(items, count = 1, allowDuplication = true) {
    let result = [];
    if (count > 1) {
        if (allowDuplication) {
            for (let i = 0; i < count; i++) {
                result.push(items[Math.floor(Math.random() * items.length)]);
            }
        }
        else {
            if (count > items.length)
                return items;
            let itemsToPick = new Set();
            while (itemsToPick.size < count)
                itemsToPick.add(items[Math.floor(Math.random() * items.length)]);
            result = Array.from(itemsToPick);
        }
        return result;
    }
    else
        return items[Math.floor(Math.random() * items.length)];
}
exports.getRandomItem = getRandomItem;
function getRandomRange(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}
exports.getRandomRange = getRandomRange;
function getRandomPossibility(percentage) {
    if (percentage > 100 || percentage < 0)
        return false;
    return Math.random() * 100 < percentage;
}
exports.getRandomPossibility = getRandomPossibility;
function chunkArray(myArray, chunk_size) {
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];
    for (index = 0; index < arrayLength; index += chunk_size) {
        const myChunk = myArray.slice(index, index + chunk_size);
        tempArray.push(myChunk);
    }
    return tempArray;
}
exports.chunkArray = chunkArray;
function autoMap(obj1, obj2) {
    let result = {};
    let keys = [];
    if (typeof obj1 === 'function')
        keys = Object.keys(new obj1());
    else if (Array.isArray(obj1))
        keys = obj1;
    else
        keys = Object.keys(obj1);
    for (const key of keys) {
        if (obj2.hasOwnProperty(key)) {
            result[key] = obj2[key];
        }
    }
    if (Object.keys(result).length === 0)
        return null;
    return result;
}
exports.autoMap = autoMap;
async function removeFile(path) {
    path = `${constants_1.ROOT_DIR}/${path}`;
    fs.exists(path, exists => {
        if (!exists)
            return;
        return new Promise((res, rej) => fs.unlink(path, err => {
            if (err)
                rej(err);
            res();
        }));
    });
}
exports.removeFile = removeFile;
//# sourceMappingURL=helpers.js.map