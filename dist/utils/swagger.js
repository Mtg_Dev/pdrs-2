"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.initSwagger = void 0;
const swagger_1 = require("@nestjs/swagger");
exports.initSwagger = app => {
    const options = new swagger_1.DocumentBuilder()
        .setTitle('P.D.R.S ')
        .setDescription('Prescription Dispensing Regulating System is an online central platform that improves & enhances the doctors work and makes their work easier and more effiecent')
        .setVersion('1.0')
        .addCookieAuth('accessToken')
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, options);
    swagger_1.SwaggerModule.setup('swagger', app, document);
};
//# sourceMappingURL=swagger.js.map