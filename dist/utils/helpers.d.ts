export declare function generatePassword(): string;
export declare function getRandomItem<T extends any[] | []>(items: T, count?: number, allowDuplication?: boolean): any;
export declare function getRandomRange(min: number, max: number): number;
export declare function getRandomPossibility(percentage: number): boolean;
export declare function chunkArray(myArray: any, chunk_size: any): any[];
export declare function autoMap(obj1: any, obj2: any): {};
export declare function removeFile(path: string): Promise<void>;
