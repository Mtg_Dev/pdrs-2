export declare const User: (...dataOrPipes: (string | import("@nestjs/common").PipeTransform<any, any> | import("@nestjs/common").Type<import("@nestjs/common").PipeTransform<any, any>>)[]) => ParameterDecorator;
export declare const UserId: (...dataOrPipes: (string | import("@nestjs/common").PipeTransform<any, any> | import("@nestjs/common").Type<import("@nestjs/common").PipeTransform<any, any>>)[]) => ParameterDecorator;
export interface PaginationQuery {
    page: number;
    size: number;
}
export declare const Pagination: (...dataOrPipes: (import("@nestjs/common").PipeTransform<any, any> | import("@nestjs/common").Type<import("@nestjs/common").PipeTransform<any, any>> | {
    defaultPage?: number;
    defaultSize?: number;
})[]) => ParameterDecorator;
interface fileInterface {
    fieldName: string;
    uploadDir: string;
    fileSize?: number;
    allowdFileTypes?: string[];
}
export declare const FileUpload: (options: fileInterface) => MethodDecorator & ClassDecorator;
export {};
