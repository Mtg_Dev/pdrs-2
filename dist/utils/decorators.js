"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileUpload = exports.Pagination = exports.UserId = exports.User = void 0;
const common_1 = require("@nestjs/common");
const multer_1 = require("multer");
const path_1 = require("path");
const platform_express_1 = require("@nestjs/platform-express");
exports.User = common_1.createParamDecorator((data, ctx) => {
    const request = ctx.switchToHttp().getRequest();
    const user = request.user;
    return data ? user && user[data] : user;
});
exports.UserId = common_1.createParamDecorator((data, ctx) => {
    const request = ctx.switchToHttp().getRequest();
    const user = request.user;
    if (user)
        return user.id;
    return null;
});
exports.Pagination = common_1.createParamDecorator(({ defaultPage = 0, defaultSize = 10, } = {}, ctx) => {
    let { page, size } = ctx.switchToHttp().getRequest().query;
    page = page !== null && page !== void 0 ? page : defaultPage;
    size = size !== null && size !== void 0 ? size : defaultSize;
    return { page, size };
});
exports.FileUpload = (options) => {
    return common_1.UseInterceptors(platform_express_1.FileInterceptor(options.fieldName, {
        storage: multer_1.diskStorage({
            destination: `assets/${options.uploadDir}`,
            filename: (req, file, cb) => {
                const randomName = Array(32)
                    .fill(null)
                    .map(() => Math.round(Math.random() * 16).toString(16))
                    .join('');
                cb(null, `${randomName}${path_1.extname(file.originalname)}`);
            },
        }),
        limits: {
            fileSize: options.fileSize || 2 * 1024 * 1024,
        },
        fileFilter: (req, file, cb) => {
            if (!options.allowdFileTypes)
                return cb(null, true);
            for (let i = 0; i < options.allowdFileTypes.length; i++) {
                const allowedFileType = options.allowdFileTypes[i];
                if (allowedFileType.indexOf('/*') !== -1 &&
                    file.mimetype.startsWith(allowedFileType.slice(0, allowedFileType.indexOf('/*'))))
                    return cb(null, true);
                if (file.mimetype === allowedFileType)
                    return cb(null, true);
            }
            return cb(new common_1.BadRequestException('File Type Not Allowed'), false);
        },
    }));
};
//# sourceMappingURL=decorators.js.map