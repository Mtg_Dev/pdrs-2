"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ROOT_DIR = exports.NOTIFICATIONS_TYPES = exports.TOKENS_TYPES = exports.GENDER = exports.UPLOAD_DIR = exports.ROLES = void 0;
const path_1 = require("path");
exports.ROLES = {
    Admin: 'admin',
    Doctor: 'doctor',
    Pharmacy: 'pharmacy',
    User: 'user',
};
exports.UPLOAD_DIR = {
    Classifications: 'images/classifications',
    Profiles: 'images/profiles',
};
exports.GENDER = { Male: 'male', Female: 'female' };
exports.TOKENS_TYPES = {
    ResetPassword: 'rp',
};
exports.NOTIFICATIONS_TYPES = {
    NewCondition: 'cond',
    NewMedicine: 'med',
    NewMessage: 'msg',
    NewRequest: 'req',
    NewReport: 'rep',
};
exports.ROOT_DIR = path_1.resolve(__dirname, '../..');
exports.default = {
    ROLES: exports.ROLES,
    UPLOAD_DIR: exports.UPLOAD_DIR,
    ROOT_DIR: exports.ROOT_DIR,
};
//# sourceMappingURL=constants.js.map