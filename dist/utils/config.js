"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.configService = void 0;
require('dotenv').config();
class ConfigService {
    constructor(env) {
        this.env = env;
    }
    getValue(key, throwOnMissing = true) {
        const value = this.env[key];
        if (!value && throwOnMissing) {
            throw new Error(`config error - missing env.${key}`);
        }
        return value;
    }
    ensureValues(keys) {
        keys.forEach(k => this.getValue(k, true));
        return this;
    }
    getJwtSecret() {
        return this.getValue('JWT_SECRET', true);
    }
    getEmail() {
        return this.getValue('EMAIL', true);
    }
    getEmailCredentials() {
        return {
            clientID: this.getValue('GMAIL_CLIENT_ID'),
            clientSecret: this.getValue('GMAIL_CLIENT_SECRET'),
            accessToken: this.getValue('GMAIL_ACCESS_TOKEN'),
            refreshToken: this.getValue('GMAIL_REFRESH_TOKEN'),
        };
    }
    getPort() {
        return this.getValue('PORT', true);
    }
    isProduction() {
        const mode = this.getValue('MODE', false);
        return mode != 'DEV';
    }
    isTest() {
        return this.getValue('NODE_ENV', false) === 'test';
    }
    getTypeOrmConfig() {
        if (this.isTest())
            return {
                type: 'sqlite',
                database: ':memory:',
                entities: ['src/**/*.entity.ts'],
                dropSchema: true,
                synchronize: true,
            };
        return {
            type: 'sqlite',
            database: './sqlite/db.db',
            entities: ['dist/**/*.entity.js'],
            synchronize: true,
            migrationsTableName: 'migration',
            migrations: ['dist/migration/*.js'],
            cli: {
                migrationsDir: 'migration',
            },
        };
    }
}
const configService = new ConfigService(process.env).ensureValues([]);
exports.configService = configService;
//# sourceMappingURL=config.js.map