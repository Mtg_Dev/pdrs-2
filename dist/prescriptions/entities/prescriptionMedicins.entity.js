"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PrescriptionMedicine = void 0;
const openapi = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const prescription_entity_1 = require("./prescription.entity");
const medicins_entity_1 = require("./medicins.entity");
const pharmacy_entity_1 = require("../../users/entities/pharmacy.entity");
let PrescriptionMedicine = class PrescriptionMedicine {
    static _OPENAPI_METADATA_FACTORY() {
        return { id: { required: true, type: () => Number }, prescription: { required: true, type: () => require("./prescription.entity").Prescription }, medicine: { required: true, type: () => require("./medicins.entity").Medicine }, medicineId: { required: true, type: () => Number }, pharmacy: { required: true, type: () => require("../../users/entities/pharmacy.entity").Pharmacy }, pharmacyId: { required: true, type: () => Number }, tempMedicine: { required: true, type: () => String }, count: { required: true, type: () => Number }, isBold: { required: true, type: () => Boolean }, isChronic: { required: true, type: () => Boolean } };
    }
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], PrescriptionMedicine.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => prescription_entity_1.Prescription, Prescription => Prescription.prescriptionMedicine, { onDelete: 'CASCADE' }),
    __metadata("design:type", prescription_entity_1.Prescription)
], PrescriptionMedicine.prototype, "prescription", void 0);
__decorate([
    typeorm_1.ManyToOne(type => medicins_entity_1.Medicine),
    typeorm_1.JoinColumn({ name: 'medicineId' }),
    __metadata("design:type", medicins_entity_1.Medicine)
], PrescriptionMedicine.prototype, "medicine", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], PrescriptionMedicine.prototype, "medicineId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => pharmacy_entity_1.Pharmacy, { onDelete: 'SET NULL' }),
    typeorm_1.JoinColumn({ name: 'pharmacyId' }),
    __metadata("design:type", pharmacy_entity_1.Pharmacy)
], PrescriptionMedicine.prototype, "pharmacy", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], PrescriptionMedicine.prototype, "pharmacyId", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], PrescriptionMedicine.prototype, "tempMedicine", void 0);
__decorate([
    typeorm_1.Column({ default: 1 }),
    __metadata("design:type", Number)
], PrescriptionMedicine.prototype, "count", void 0);
__decorate([
    typeorm_1.Column({ default: false }),
    __metadata("design:type", Boolean)
], PrescriptionMedicine.prototype, "isBold", void 0);
__decorate([
    typeorm_1.Column({ default: false }),
    __metadata("design:type", Boolean)
], PrescriptionMedicine.prototype, "isChronic", void 0);
PrescriptionMedicine = __decorate([
    typeorm_1.Entity()
], PrescriptionMedicine);
exports.PrescriptionMedicine = PrescriptionMedicine;
//# sourceMappingURL=prescriptionMedicins.entity.js.map