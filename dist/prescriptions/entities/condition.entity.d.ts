import { Classification } from './classification.entity';
export declare class Condition {
    id: number;
    name: string;
    classificationId: number;
    classification: Classification;
}
