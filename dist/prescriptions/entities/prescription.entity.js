"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Prescription = void 0;
const openapi = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const condition_entity_1 = require("./condition.entity");
const doctor_entity_1 = require("../../users/entities/doctor.entity");
const patient_entity_1 = require("../../users/entities/patient.entity");
const classification_entity_1 = require("./classification.entity");
const prescriptionMedicins_entity_1 = require("./prescriptionMedicins.entity");
let Prescription = class Prescription {
    static _OPENAPI_METADATA_FACTORY() {
        return { id: { required: true, type: () => Number }, createdAt: { required: true, type: () => Date }, tempCondition: { required: true, type: () => String }, note: { required: true, type: () => String }, isPrivate: { required: true, type: () => Boolean }, patientId: { required: true, type: () => String }, patient: { required: true, type: () => require("../../users/entities/patient.entity").Patient }, doctorId: { required: true, type: () => String }, doctor: { required: true, type: () => require("../../users/entities/doctor.entity").Doctor }, classification: { required: true, type: () => require("./classification.entity").Classification }, condition: { required: true, type: () => require("./condition.entity").Condition }, prescriptionMedicine: { required: true, type: () => [require("./prescriptionMedicins.entity").PrescriptionMedicine] } };
    }
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Prescription.prototype, "id", void 0);
__decorate([
    typeorm_1.CreateDateColumn(),
    __metadata("design:type", Date)
], Prescription.prototype, "createdAt", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Prescription.prototype, "tempCondition", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Prescription.prototype, "note", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Boolean)
], Prescription.prototype, "isPrivate", void 0);
__decorate([
    typeorm_1.Column({ nullable: false }),
    __metadata("design:type", String)
], Prescription.prototype, "patientId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => patient_entity_1.Patient, Patient => Patient.prescriptions),
    typeorm_1.JoinColumn({ name: 'patientId' }),
    __metadata("design:type", patient_entity_1.Patient)
], Prescription.prototype, "patient", void 0);
__decorate([
    typeorm_1.Column({ nullable: false }),
    __metadata("design:type", String)
], Prescription.prototype, "doctorId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => doctor_entity_1.Doctor, Doctor => Doctor.prescriptions, { onDelete: 'SET NULL' }),
    typeorm_1.JoinColumn({ name: 'doctorId' }),
    __metadata("design:type", doctor_entity_1.Doctor)
], Prescription.prototype, "doctor", void 0);
__decorate([
    typeorm_1.ManyToOne(type => classification_entity_1.Classification),
    __metadata("design:type", classification_entity_1.Classification)
], Prescription.prototype, "classification", void 0);
__decorate([
    typeorm_1.ManyToOne(type => condition_entity_1.Condition),
    __metadata("design:type", condition_entity_1.Condition)
], Prescription.prototype, "condition", void 0);
__decorate([
    typeorm_1.OneToMany(type => prescriptionMedicins_entity_1.PrescriptionMedicine, pm => pm.prescription, { onDelete: 'CASCADE' }),
    __metadata("design:type", Array)
], Prescription.prototype, "prescriptionMedicine", void 0);
Prescription = __decorate([
    typeorm_1.Entity()
], Prescription);
exports.Prescription = Prescription;
//# sourceMappingURL=prescription.entity.js.map