export declare class Medicine {
    id: number;
    name: string;
    contradictingMedicins: Medicine[];
}
