import { Prescription } from './prescription.entity';
import { User } from '../../users/entities/user.entity';
export declare class Report {
    id: number;
    note: string;
    prescriptionId: number;
    prescription: Prescription;
    userId: string;
    user: User;
}
