import { Prescription } from './prescription.entity';
import { Medicine } from './medicins.entity';
import { Pharmacy } from '../../users/entities/pharmacy.entity';
export declare class PrescriptionMedicine {
    id: number;
    prescription: Prescription;
    medicine: Medicine;
    medicineId: number;
    pharmacy: Pharmacy;
    pharmacyId: number;
    tempMedicine: string;
    count: number;
    isBold: boolean;
    isChronic: boolean;
}
