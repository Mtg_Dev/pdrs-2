"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Report = void 0;
const openapi = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const prescription_entity_1 = require("./prescription.entity");
const user_entity_1 = require("../../users/entities/user.entity");
let Report = class Report {
    static _OPENAPI_METADATA_FACTORY() {
        return { id: { required: true, type: () => Number }, note: { required: true, type: () => String }, prescriptionId: { required: true, type: () => Number }, prescription: { required: true, type: () => require("./prescription.entity").Prescription }, userId: { required: true, type: () => String }, user: { required: true, type: () => require("../../users/entities/user.entity").User } };
    }
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Report.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Report.prototype, "note", void 0);
__decorate([
    typeorm_1.Column({ nullable: false }),
    __metadata("design:type", Number)
], Report.prototype, "prescriptionId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => prescription_entity_1.Prescription, { onDelete: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'prescriptionId' }),
    __metadata("design:type", prescription_entity_1.Prescription)
], Report.prototype, "prescription", void 0);
__decorate([
    typeorm_1.Column({ nullable: false }),
    __metadata("design:type", String)
], Report.prototype, "userId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.User, { onDelete: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'userId' }),
    __metadata("design:type", user_entity_1.User)
], Report.prototype, "user", void 0);
Report = __decorate([
    typeorm_1.Entity()
], Report);
exports.Report = Report;
//# sourceMappingURL=reports.entity.js.map