import { Condition } from './condition.entity';
import { Doctor } from '../../users/entities/doctor.entity';
import { Patient } from '../../users/entities/patient.entity';
import { Classification } from './classification.entity';
import { PrescriptionMedicine } from './prescriptionMedicins.entity';
export declare class Prescription {
    id: number;
    createdAt: Date;
    tempCondition: string;
    note: string;
    isPrivate: boolean;
    patientId: string;
    patient: Patient;
    doctorId: string;
    doctor: Doctor;
    classification: Classification;
    condition: Condition;
    prescriptionMedicine: PrescriptionMedicine[];
}
