"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Classification = void 0;
const openapi = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const condition_entity_1 = require("./condition.entity");
const doctor_entity_1 = require("../../users/entities/doctor.entity");
let Classification = class Classification {
    static _OPENAPI_METADATA_FACTORY() {
        return { id: { required: true, type: () => Number }, name: { required: true, type: () => String }, image: { required: true, type: () => String }, isPrivate: { required: true, type: () => Boolean }, conditions: { required: true, type: () => [require("./condition.entity").Condition] }, doctors: { required: true, type: () => [require("../../users/entities/doctor.entity").Doctor] } };
    }
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Classification.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Classification.prototype, "name", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Classification.prototype, "image", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Boolean)
], Classification.prototype, "isPrivate", void 0);
__decorate([
    typeorm_1.OneToMany(type => condition_entity_1.Condition, Condition => Condition.classification),
    __metadata("design:type", Array)
], Classification.prototype, "conditions", void 0);
__decorate([
    typeorm_1.ManyToMany(type => doctor_entity_1.Doctor, d => d.classifications),
    __metadata("design:type", Array)
], Classification.prototype, "doctors", void 0);
Classification = __decorate([
    typeorm_1.Entity()
], Classification);
exports.Classification = Classification;
//# sourceMappingURL=classification.entity.js.map