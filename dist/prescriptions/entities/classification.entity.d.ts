import { Condition } from './condition.entity';
import { Doctor } from '../../users/entities/doctor.entity';
export declare class Classification {
    id: number;
    name: string;
    image: string;
    isPrivate: boolean;
    conditions: Condition[];
    doctors: Doctor[];
}
