import { Prescription } from './entities/prescription.entity';
import { Repository } from 'typeorm';
import { CreatePrescriptionDTO } from './Dtos/createPrescription.dto';
import { Doctor } from 'src/users/entities/doctor.entity';
import { Classification } from './entities/classification.entity';
import { Condition } from './entities/condition.entity';
import { Patient } from 'src/users/entities/patient.entity';
import { PrescriptionMedicine } from './entities/prescriptionMedicins.entity';
import { Medicine } from './entities/medicins.entity';
import { DispensePrescriptionDto } from './Dtos/dispensePrescription.dto';
import { UsersService } from 'src/users/users.service';
import { ClassificationsService } from './classifications/classifications.service';
import { NotificationsService } from 'src/users/notifications/notifications.service';
import { PaginationQuery } from 'src/utils/decorators';
import { Report } from './entities/reports.entity';
export declare class PrescriptionsService {
    private userService;
    private classificationService;
    private notificationsService;
    private prescriptionsRepository;
    private doctorsRepository;
    private classificationsRepository;
    private conditionsRepository;
    private patientsRepository;
    private medicineRepository;
    private prescriptionMedicinesRepository;
    private reportsRepository;
    constructor(userService: UsersService, classificationService: ClassificationsService, notificationsService: NotificationsService, prescriptionsRepository: Repository<Prescription>, doctorsRepository: Repository<Doctor>, classificationsRepository: Repository<Classification>, conditionsRepository: Repository<Condition>, patientsRepository: Repository<Patient>, medicineRepository: Repository<Medicine>, prescriptionMedicinesRepository: Repository<PrescriptionMedicine>, reportsRepository: Repository<Report>);
    getPatientPrescriptions(patientId: string, userId: string, classification?: number, pagination?: PaginationQuery): Promise<Prescription[]>;
    getPatientPrescriptionsToDispense(patientId: string, pagination?: PaginationQuery): Promise<Prescription[]>;
    createPrescription(prescription: CreatePrescriptionDTO, userId: string): Promise<{
        tempCondition: any;
        condition: any;
        note: string;
        patientId: string;
        classification: Classification;
        isPrivate: boolean;
        doctor: Doctor;
    } & Prescription>;
    dispensePrescription(prescription: DispensePrescriptionDto, userId: string): Promise<import("typeorm").UpdateResult>;
    reportPrescription(userId: string, prescriptionId: number, note?: string): Promise<{
        userId: string;
        note: string;
        prescriptionId: number;
    } & Report>;
}
