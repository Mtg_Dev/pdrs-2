"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PrescriptionsService = void 0;
const common_1 = require("@nestjs/common");
const prescription_entity_1 = require("./entities/prescription.entity");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const doctor_entity_1 = require("../users/entities/doctor.entity");
const classification_entity_1 = require("./entities/classification.entity");
const condition_entity_1 = require("./entities/condition.entity");
const patient_entity_1 = require("../users/entities/patient.entity");
const prescriptionMedicins_entity_1 = require("./entities/prescriptionMedicins.entity");
const medicins_entity_1 = require("./entities/medicins.entity");
const classificationsSeed_1 = require("../utils/seed/classificationsSeed");
const user_entity_1 = require("../users/entities/user.entity");
const users_service_1 = require("../users/users.service");
const classifications_service_1 = require("./classifications/classifications.service");
const notifications_service_1 = require("../users/notifications/notifications.service");
const decorators_1 = require("../utils/decorators");
const reports_entity_1 = require("./entities/reports.entity");
let PrescriptionsService = class PrescriptionsService {
    constructor(userService, classificationService, notificationsService, prescriptionsRepository, doctorsRepository, classificationsRepository, conditionsRepository, patientsRepository, medicineRepository, prescriptionMedicinesRepository, reportsRepository) {
        this.userService = userService;
        this.classificationService = classificationService;
        this.notificationsService = notificationsService;
        this.prescriptionsRepository = prescriptionsRepository;
        this.doctorsRepository = doctorsRepository;
        this.classificationsRepository = classificationsRepository;
        this.conditionsRepository = conditionsRepository;
        this.patientsRepository = patientsRepository;
        this.medicineRepository = medicineRepository;
        this.prescriptionMedicinesRepository = prescriptionMedicinesRepository;
        this.reportsRepository = reportsRepository;
    }
    async getPatientPrescriptions(patientId, userId, classification, pagination) {
        const doctor = await this.userService.getDoctor(userId);
        const prescriptions = await this.prescriptionsRepository.find({
            where: Object.assign({ patientId }, (classification && { classification })),
            order: {
                createdAt: 'DESC',
            },
            join: {
                alias: 'prescription',
                leftJoinAndSelect: {
                    classification: 'prescription.classification',
                    condition: 'prescription.condition',
                    prescriptionMedicine: 'prescription.prescriptionMedicine',
                    medicine: 'prescriptionMedicine.medicine',
                },
            },
            skip: pagination.size * pagination.page,
            take: pagination.size,
        });
        return prescriptions.map(p => {
            if (!p.isPrivate || p.doctorId === doctor.id)
                return p;
            return Object.assign(Object.assign({}, p), { condition: null });
        });
    }
    async getPatientPrescriptionsToDispense(patientId, pagination) {
        return this.prescriptionsRepository
            .createQueryBuilder('prescription')
            .where('prescription.patientId = :patientId', { patientId })
            .innerJoinAndSelect('prescription.prescriptionMedicine', 'prescriptionMedicine')
            .innerJoinAndSelect('prescription.doctor', 'doctor')
            .andWhere('prescriptionMedicine.pharmacyId IS NULL')
            .innerJoinAndSelect('prescriptionMedicine.medicine', 'medicine')
            .take(pagination.size)
            .skip(pagination.size * pagination.page)
            .orderBy('prescription.createdAt', 'DESC')
            .getMany();
    }
    async createPrescription(prescription, userId) {
        try {
            const doctor = await this.userService.getDoctor(userId);
            const classification = await this.classificationService.getClassification(prescription.classificationId);
            if (!classification)
                throw new common_1.BadRequestException({ msg: 'Classification Error' });
            let condition = null;
            let tempCondition = null;
            if (!prescription.conditionId && !prescription.tempCondition)
                throw new common_1.BadRequestException();
            if (!prescription.conditionId)
                tempCondition = prescription.tempCondition;
            else
                condition = await this.classificationService.getCondition(prescription.conditionId);
            const newPrescription = await this.prescriptionsRepository.save(Object.assign(Object.assign(Object.assign({ patientId: prescription.patientId, classification, isPrivate: classification.isPrivate && prescription.isPrivate, doctor }, (prescription.note && { note: prescription.note })), (condition && { condition })), (tempCondition && { tempCondition })));
            if (tempCondition)
                this.notificationsService.addCondition(newPrescription);
            for (let { id, isBold, isChronic, count, tempMedicine, } of prescription.medicins) {
                const medicine = await this.medicineRepository.findOne({
                    where: {
                        id,
                    },
                });
                if (medicine)
                    tempMedicine = null;
                const createdMedicine = await this.prescriptionMedicinesRepository.save(Object.assign(Object.assign(Object.assign({ isBold,
                    isChronic,
                    count }, (medicine && { medicine })), (tempMedicine && { tempMedicine })), { prescription: newPrescription }));
                if (tempMedicine)
                    this.notificationsService.addMedicine(createdMedicine);
            }
            return newPrescription;
        }
        catch (error) {
            throw new common_1.BadRequestException();
        }
    }
    async dispensePrescription(prescription, userId) {
        try {
            const pharmacy = await this.userService.getPharmacy(userId);
            return this.prescriptionMedicinesRepository.update(prescription.prescriptionMedicins.map(m => m.id), { pharmacy });
        }
        catch (error) {
            throw new common_1.BadRequestException();
        }
    }
    async reportPrescription(userId, prescriptionId, note) {
        const report = await this.reportsRepository.save({
            userId,
            note,
            prescriptionId,
        });
        this.notificationsService.addReport(report);
        return report;
    }
};
PrescriptionsService = __decorate([
    common_1.Injectable(),
    __param(3, typeorm_1.InjectRepository(prescription_entity_1.Prescription)),
    __param(4, typeorm_1.InjectRepository(doctor_entity_1.Doctor)),
    __param(5, typeorm_1.InjectRepository(classification_entity_1.Classification)),
    __param(6, typeorm_1.InjectRepository(condition_entity_1.Condition)),
    __param(7, typeorm_1.InjectRepository(patient_entity_1.Patient)),
    __param(8, typeorm_1.InjectRepository(medicins_entity_1.Medicine)),
    __param(9, typeorm_1.InjectRepository(prescriptionMedicins_entity_1.PrescriptionMedicine)),
    __param(10, typeorm_1.InjectRepository(reports_entity_1.Report)),
    __metadata("design:paramtypes", [users_service_1.UsersService,
        classifications_service_1.ClassificationsService,
        notifications_service_1.NotificationsService,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], PrescriptionsService);
exports.PrescriptionsService = PrescriptionsService;
//# sourceMappingURL=prescriptions.service.js.map