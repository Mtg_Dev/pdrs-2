import { Classification } from '../entities/classification.entity';
import { Condition } from '../entities/condition.entity';
import { Repository } from 'typeorm';
import { PostConditionDTO } from '../Dtos/PostCondition.dto';
import { PostClassificationDTO } from '../Dtos/PostClassification.dto';
export declare class ClassificationsService {
    private classificationsRepository;
    private conditionsRepository;
    constructor(classificationsRepository: Repository<Classification>, conditionsRepository: Repository<Condition>);
    getAllClassifications(): Promise<Classification[]>;
    getClassification(id: number): Promise<Classification>;
    getCondition(id: number): Promise<Condition>;
    getConditions(classificationId: string): Promise<Condition[]>;
    postCondition(newCondition: PostConditionDTO): Promise<PostConditionDTO & Condition>;
    postClassification(classification: PostClassificationDTO, image?: string): Promise<{
        image: string;
        id: number;
        name: string;
    } & Classification>;
    deleteClassification(id: number): Promise<import("typeorm").DeleteResult>;
}
