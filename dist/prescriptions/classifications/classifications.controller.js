"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClassificationsController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const classifications_service_1 = require("./classifications.service");
const PostCondition_dto_1 = require("../Dtos/PostCondition.dto");
const swagger_1 = require("@nestjs/swagger");
const decorators_1 = require("../../utils/decorators");
const constants_1 = require("../../utils/constants");
const PostClassification_dto_1 = require("../Dtos/PostClassification.dto");
let ClassificationsController = class ClassificationsController {
    constructor(classificationsService) {
        this.classificationsService = classificationsService;
    }
    async getClassifications() {
        return this.classificationsService.getAllClassifications();
    }
    async getConditions(id) {
        return this.classificationsService.getConditions(id);
    }
    async createCondition(conditionDto) {
        return this.classificationsService.postCondition(conditionDto);
    }
    async createClassification(classificationDto, file) {
        return this.classificationsService.postClassification(classificationDto, file.path);
    }
    async deleteClassification({ id }) {
        return this.classificationsService.deleteClassification(id);
    }
};
__decorate([
    common_1.Get(),
    openapi.ApiResponse({ status: 200, type: [require("../entities/classification.entity").Classification] }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ClassificationsController.prototype, "getClassifications", null);
__decorate([
    common_1.Get('conditions'),
    openapi.ApiResponse({ status: 200, type: [require("../entities/condition.entity").Condition] }),
    __param(0, common_1.Query('classificationId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ClassificationsController.prototype, "getConditions", null);
__decorate([
    common_1.Post('conditions'),
    openapi.ApiResponse({ status: 201, type: Object }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [PostCondition_dto_1.PostConditionDTO]),
    __metadata("design:returntype", Promise)
], ClassificationsController.prototype, "createCondition", null);
__decorate([
    common_1.Post(),
    decorators_1.FileUpload({
        fieldName: 'image',
        uploadDir: constants_1.UPLOAD_DIR.Classifications,
        allowdFileTypes: ['image/png', 'image/svg'],
    }),
    openapi.ApiResponse({ status: 201, type: Object }),
    __param(0, common_1.Body()),
    __param(1, common_1.UploadedFile()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [PostClassification_dto_1.PostClassificationDTO, Object]),
    __metadata("design:returntype", Promise)
], ClassificationsController.prototype, "createClassification", null);
__decorate([
    common_1.Delete(),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ClassificationsController.prototype, "deleteClassification", null);
ClassificationsController = __decorate([
    swagger_1.ApiTags('Classifications'),
    common_1.Controller('api/classifications'),
    __metadata("design:paramtypes", [classifications_service_1.ClassificationsService])
], ClassificationsController);
exports.ClassificationsController = ClassificationsController;
//# sourceMappingURL=classifications.controller.js.map