import { ClassificationsService } from './classifications.service';
import { PostConditionDTO } from '../Dtos/PostCondition.dto';
import { PostClassificationDTO } from '../Dtos/PostClassification.dto';
export declare class ClassificationsController {
    private classificationsService;
    constructor(classificationsService: ClassificationsService);
    getClassifications(): Promise<import("../entities/classification.entity").Classification[]>;
    getConditions(id: string): Promise<import("../entities/condition.entity").Condition[]>;
    createCondition(conditionDto: PostConditionDTO): Promise<PostConditionDTO & import("../entities/condition.entity").Condition>;
    createClassification(classificationDto: PostClassificationDTO, file: any): Promise<{
        image: string;
        id: number;
        name: string;
    } & import("../entities/classification.entity").Classification>;
    deleteClassification({ id }: {
        id: number;
    }): Promise<import("typeorm").DeleteResult>;
}
