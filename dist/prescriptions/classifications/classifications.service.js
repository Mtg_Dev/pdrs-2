"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClassificationsService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const classification_entity_1 = require("../entities/classification.entity");
const condition_entity_1 = require("../entities/condition.entity");
const typeorm_2 = require("typeorm");
const helpers_1 = require("../../utils/helpers");
const constants_1 = require("../../utils/constants");
let ClassificationsService = class ClassificationsService {
    constructor(classificationsRepository, conditionsRepository) {
        this.classificationsRepository = classificationsRepository;
        this.conditionsRepository = conditionsRepository;
    }
    async getAllClassifications() {
        return this.classificationsRepository.find();
    }
    getClassification(id) {
        return this.classificationsRepository.findOne({
            where: {
                id,
            },
        });
    }
    getCondition(id) {
        return this.conditionsRepository.findOne({
            where: {
                id,
            },
        });
    }
    async getConditions(classificationId) {
        return this.conditionsRepository.find({
            where: {
                classificationId,
            },
        });
    }
    async postCondition(newCondition) {
        try {
            return this.conditionsRepository.save(newCondition);
        }
        catch (error) {
            throw new common_1.BadRequestException();
        }
    }
    async postClassification(classification, image) {
        try {
            if (classification.id && image) {
                const oldImage = (await this.classificationsRepository.findOne(classification.id)).image;
                await helpers_1.removeFile(oldImage);
            }
            return await this.classificationsRepository.save(Object.assign(Object.assign({}, classification), (image && { image })));
        }
        catch (error) {
            if (image)
                await helpers_1.removeFile(image);
            throw new common_1.BadRequestException();
        }
    }
    async deleteClassification(id) {
        try {
            const classification = await this.classificationsRepository.findOneOrFail(id);
            await helpers_1.removeFile(classification.image);
            return this.classificationsRepository.delete({ id });
        }
        catch (error) {
            throw new common_1.BadRequestException();
        }
    }
};
ClassificationsService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(classification_entity_1.Classification)),
    __param(1, typeorm_1.InjectRepository(condition_entity_1.Condition)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository])
], ClassificationsService);
exports.ClassificationsService = ClassificationsService;
//# sourceMappingURL=classifications.service.js.map