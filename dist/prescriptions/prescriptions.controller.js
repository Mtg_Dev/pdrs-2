"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PrescriptionsController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const prescriptions_service_1 = require("./prescriptions.service");
const classificationsSeed_1 = require("../utils/seed/classificationsSeed");
const swagger_1 = require("@nestjs/swagger");
const auth_guard_1 = require("../middlewares/guards/auth.guard");
const constants_1 = require("../utils/constants");
const createPrescription_dto_1 = require("./Dtos/createPrescription.dto");
const dispensePrescription_dto_1 = require("./Dtos/dispensePrescription.dto");
const decorators_1 = require("../utils/decorators");
const pagination_1 = require("../middlewares/pipes/pagination");
let PrescriptionsController = class PrescriptionsController {
    constructor(prescriptionsService) {
        this.prescriptionsService = prescriptionsService;
    }
    async getPrescriptions(patientId, userId, classificationId, pagination) {
        return this.prescriptionsService.getPatientPrescriptions(patientId, userId, classificationId, pagination);
    }
    async getPrescriptionsToDispense(patientId, pagination) {
        return this.prescriptionsService.getPatientPrescriptionsToDispense(patientId, pagination);
    }
    async createPrescription(prescriptionDto, userId) {
        return this.prescriptionsService.createPrescription(prescriptionDto, userId);
    }
    async DispensePrescription(prescriptionDto, userId) {
        return this.prescriptionsService.dispensePrescription(prescriptionDto, userId);
    }
    async ReportPrescription({ prescriptionId, note }, userId) {
        return this.prescriptionsService.reportPrescription(userId, prescriptionId, note);
    }
};
__decorate([
    common_1.Get(),
    pagination_1.ApiPagination(),
    auth_guard_1.Auth(constants_1.ROLES.Admin, constants_1.ROLES.Doctor),
    openapi.ApiResponse({ status: 200, type: [require("./entities/prescription.entity").Prescription] }),
    __param(0, common_1.Query('patientId')),
    __param(1, decorators_1.UserId()),
    __param(2, common_1.Query('classificationId')),
    __param(3, decorators_1.Pagination()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Number, Object]),
    __metadata("design:returntype", Promise)
], PrescriptionsController.prototype, "getPrescriptions", null);
__decorate([
    common_1.Get('to-dispense'),
    pagination_1.ApiPagination(),
    auth_guard_1.Auth(constants_1.ROLES.Pharmacy),
    openapi.ApiResponse({ status: 200, type: [require("./entities/prescription.entity").Prescription] }),
    __param(0, common_1.Query('patientId')),
    __param(1, decorators_1.Pagination()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], PrescriptionsController.prototype, "getPrescriptionsToDispense", null);
__decorate([
    common_1.Post(),
    auth_guard_1.Auth(constants_1.ROLES.Admin, constants_1.ROLES.Doctor),
    openapi.ApiResponse({ status: 201, type: Object }),
    __param(0, common_1.Body()),
    __param(1, decorators_1.UserId()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [createPrescription_dto_1.CreatePrescriptionDTO, String]),
    __metadata("design:returntype", Promise)
], PrescriptionsController.prototype, "createPrescription", null);
__decorate([
    common_1.Post('dispense'),
    common_1.HttpCode(200),
    auth_guard_1.Auth(constants_1.ROLES.Admin, constants_1.ROLES.Pharmacy),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Body()),
    __param(1, decorators_1.UserId()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dispensePrescription_dto_1.DispensePrescriptionDto, String]),
    __metadata("design:returntype", Promise)
], PrescriptionsController.prototype, "DispensePrescription", null);
__decorate([
    common_1.Post('report'),
    common_1.HttpCode(200),
    auth_guard_1.Auth(constants_1.ROLES.Admin, constants_1.ROLES.Pharmacy, constants_1.ROLES.Doctor),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Body()),
    __param(1, decorators_1.UserId()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], PrescriptionsController.prototype, "ReportPrescription", null);
PrescriptionsController = __decorate([
    swagger_1.ApiTags('Prescripitons'),
    common_1.Controller('api/prescriptions'),
    __metadata("design:paramtypes", [prescriptions_service_1.PrescriptionsService])
], PrescriptionsController);
exports.PrescriptionsController = PrescriptionsController;
//# sourceMappingURL=prescriptions.controller.js.map