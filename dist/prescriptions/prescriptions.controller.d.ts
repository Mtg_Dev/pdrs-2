import { PrescriptionsService } from './prescriptions.service';
import { CreatePrescriptionDTO } from './Dtos/createPrescription.dto';
import { DispensePrescriptionDto } from './Dtos/dispensePrescription.dto';
export declare class PrescriptionsController {
    private prescriptionsService;
    constructor(prescriptionsService: PrescriptionsService);
    getPrescriptions(patientId: string, userId: any, classificationId?: number, pagination?: any): Promise<import("./entities/prescription.entity").Prescription[]>;
    getPrescriptionsToDispense(patientId: string, pagination?: any): Promise<import("./entities/prescription.entity").Prescription[]>;
    createPrescription(prescriptionDto: CreatePrescriptionDTO, userId: string): Promise<{
        tempCondition: any;
        condition: any;
        note: string;
        patientId: string;
        classification: import("./entities/classification.entity").Classification;
        isPrivate: boolean;
        doctor: import("../users/entities/doctor.entity").Doctor;
    } & import("./entities/prescription.entity").Prescription>;
    DispensePrescription(prescriptionDto: DispensePrescriptionDto, userId: string): Promise<import("typeorm").UpdateResult>;
    ReportPrescription({ prescriptionId, note }: {
        prescriptionId: number;
        note: string;
    }, userId: string): Promise<{
        userId: string;
        note: string;
        prescriptionId: number;
    } & import("./entities/reports.entity").Report>;
}
