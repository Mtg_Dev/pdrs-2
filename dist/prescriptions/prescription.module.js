"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PrescriptionsModule = void 0;
const common_1 = require("@nestjs/common");
const prescriptions_service_1 = require("./prescriptions.service");
const prescriptions_controller_1 = require("./prescriptions.controller");
const classifications_controller_1 = require("./classifications/classifications.controller");
const classifications_service_1 = require("./classifications/classifications.service");
const typeorm_1 = require("@nestjs/typeorm");
const classification_entity_1 = require("./entities/classification.entity");
const condition_entity_1 = require("./entities/condition.entity");
const medicins_controller_1 = require("./medicins/medicins.controller");
const medicins_service_1 = require("./medicins/medicins.service");
const medicins_entity_1 = require("./entities/medicins.entity");
const prescription_entity_1 = require("./entities/prescription.entity");
const doctor_entity_1 = require("../users/entities/doctor.entity");
const patient_entity_1 = require("../users/entities/patient.entity");
const prescriptionMedicins_entity_1 = require("./entities/prescriptionMedicins.entity");
const users_module_1 = require("../users/users.module");
const notifications_module_1 = require("../users/notifications/notifications.module");
const reports_entity_1 = require("./entities/reports.entity");
let PrescriptionsModule = class PrescriptionsModule {
};
PrescriptionsModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([
                classification_entity_1.Classification,
                doctor_entity_1.Doctor,
                patient_entity_1.Patient,
                condition_entity_1.Condition,
                medicins_entity_1.Medicine,
                prescription_entity_1.Prescription,
                prescriptionMedicins_entity_1.PrescriptionMedicine,
                reports_entity_1.Report,
            ]),
            users_module_1.UsersModule,
            notifications_module_1.NotificationsModule,
        ],
        providers: [prescriptions_service_1.PrescriptionsService, classifications_service_1.ClassificationsService, medicins_service_1.MedicinsService],
        controllers: [
            prescriptions_controller_1.PrescriptionsController,
            classifications_controller_1.ClassificationsController,
            medicins_controller_1.MedicinsController,
        ],
    })
], PrescriptionsModule);
exports.PrescriptionsModule = PrescriptionsModule;
//# sourceMappingURL=prescription.module.js.map