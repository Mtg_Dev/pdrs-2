"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreatePrescriptionDTO = void 0;
const openapi = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class CreatePrescriptionDTO {
    static _OPENAPI_METADATA_FACTORY() {
        return { patientId: { required: true, type: () => String }, classificationId: { required: true, type: () => Number }, conditionId: { required: true, type: () => Number }, tempCondition: { required: true, type: () => String }, medicins: { required: true, type: () => [Medicine] }, note: { required: true, type: () => String }, isPrivate: { required: true, type: () => Boolean } };
    }
}
__decorate([
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CreatePrescriptionDTO.prototype, "patientId", void 0);
__decorate([
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], CreatePrescriptionDTO.prototype, "classificationId", void 0);
__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], CreatePrescriptionDTO.prototype, "conditionId", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.ValidateIf(p => !p.conditionId),
    __metadata("design:type", String)
], CreatePrescriptionDTO.prototype, "tempCondition", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    __metadata("design:type", Array)
], CreatePrescriptionDTO.prototype, "medicins", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], CreatePrescriptionDTO.prototype, "note", void 0);
__decorate([
    class_validator_1.IsBoolean(),
    class_validator_1.IsOptional(),
    __metadata("design:type", Boolean)
], CreatePrescriptionDTO.prototype, "isPrivate", void 0);
exports.CreatePrescriptionDTO = CreatePrescriptionDTO;
class Medicine {
    constructor() {
        this.count = 1;
    }
    static _OPENAPI_METADATA_FACTORY() {
        return { id: { required: true, type: () => Number }, tempMedicine: { required: true, type: () => String }, isBold: { required: true, type: () => Boolean }, isChronic: { required: true, type: () => Boolean }, count: { required: true, type: () => Number, default: 1 } };
    }
}
__decorate([
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], Medicine.prototype, "id", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.ValidateIf(m => !m.id),
    __metadata("design:type", String)
], Medicine.prototype, "tempMedicine", void 0);
__decorate([
    class_validator_1.IsBoolean(),
    class_validator_1.IsOptional(),
    __metadata("design:type", Boolean)
], Medicine.prototype, "isBold", void 0);
__decorate([
    class_validator_1.IsBoolean(),
    class_validator_1.IsOptional(),
    __metadata("design:type", Boolean)
], Medicine.prototype, "isChronic", void 0);
__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], Medicine.prototype, "count", void 0);
//# sourceMappingURL=createPrescription.dto.js.map