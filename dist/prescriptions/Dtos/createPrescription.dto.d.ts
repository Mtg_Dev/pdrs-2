export declare class CreatePrescriptionDTO {
    patientId: string;
    classificationId: number;
    conditionId: number;
    tempCondition: string;
    medicins: Medicine[];
    note: string;
    isPrivate: boolean;
}
declare class Medicine {
    id: number;
    tempMedicine: string;
    isBold: boolean;
    isChronic: boolean;
    count: number;
}
export {};
