export declare class UpdateMedicineDto {
    id: number;
    name: string;
    contradictingMedicins: {
        id: number;
    }[];
}
