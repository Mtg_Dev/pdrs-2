export declare class PostConditionDTO {
    id: number;
    name: string;
    classificationId: number;
}
