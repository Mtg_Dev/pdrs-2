import { Medicine } from '../entities/medicins.entity';
import { Repository } from 'typeorm';
import { UpdateMedicineDto } from '../Dtos/updateMedicine.dto';
export declare class MedicinsService {
    private medicinesRepository;
    constructor(medicinesRepository: Repository<Medicine>);
    getMedicins(name: string, page?: number, size?: number): Promise<Medicine[]>;
    createMedicine(name: string): Promise<{
        name: string;
    } & Medicine>;
    updateMedicine(updatedMedicine: UpdateMedicineDto): Promise<import("typeorm").UpdateResult>;
}
