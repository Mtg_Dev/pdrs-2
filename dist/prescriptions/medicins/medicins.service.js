"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MedicinsService = void 0;
const common_1 = require("@nestjs/common");
const medicins_entity_1 = require("../entities/medicins.entity");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const classificationsSeed_1 = require("../../utils/seed/classificationsSeed");
let MedicinsService = class MedicinsService {
    constructor(medicinesRepository) {
        this.medicinesRepository = medicinesRepository;
    }
    async getMedicins(name, page = 0, size = 10) {
        return (this.medicinesRepository
            .createQueryBuilder('medicine')
            .where('medicine.name like :name ', {
            name: '%' + name + '%',
        })
            .leftJoinAndSelect('medicine.contradictingMedicins', 'contradictingMedicins')
            .getMany());
    }
    createMedicine(name) {
        return this.medicinesRepository.save({
            name,
        });
    }
    updateMedicine(updatedMedicine) {
        return this.medicinesRepository.update(updatedMedicine.id, updatedMedicine);
    }
};
MedicinsService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(medicins_entity_1.Medicine)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], MedicinsService);
exports.MedicinsService = MedicinsService;
//# sourceMappingURL=medicins.service.js.map