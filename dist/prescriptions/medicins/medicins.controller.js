"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MedicinsController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const medicins_service_1 = require("./medicins.service");
const decorators_1 = require("../../utils/decorators");
const pagination_1 = require("../../middlewares/pipes/pagination");
const auth_guard_1 = require("../../middlewares/guards/auth.guard");
const constants_1 = require("../../utils/constants");
const updateMedicine_dto_1 = require("../Dtos/updateMedicine.dto");
const classificationsSeed_1 = require("../../utils/seed/classificationsSeed");
let MedicinsController = class MedicinsController {
    constructor(medicinsService) {
        this.medicinsService = medicinsService;
    }
    async getMedicins({ page, size }, name) {
        return this.medicinsService.getMedicins(name, page, size);
    }
    addMedicine(newMedicine) {
        return this.medicinsService.createMedicine(newMedicine.name);
    }
    updateMedicine(medicine) {
        return this.medicinsService.updateMedicine(medicine);
    }
};
__decorate([
    common_1.Get(),
    pagination_1.ApiPagination(),
    openapi.ApiResponse({ status: 200, type: [require("../entities/medicins.entity").Medicine] }),
    __param(0, decorators_1.Pagination()), __param(1, common_1.Query('name')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], MedicinsController.prototype, "getMedicins", null);
__decorate([
    common_1.Post(),
    auth_guard_1.Auth(constants_1.ROLES.Admin),
    openapi.ApiResponse({ status: 201, type: Object }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], MedicinsController.prototype, "addMedicine", null);
__decorate([
    common_1.Put(),
    auth_guard_1.Auth(constants_1.ROLES.Admin),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [updateMedicine_dto_1.UpdateMedicineDto]),
    __metadata("design:returntype", void 0)
], MedicinsController.prototype, "updateMedicine", null);
MedicinsController = __decorate([
    swagger_1.ApiTags('Medicins'),
    common_1.Controller('api/medicins'),
    __metadata("design:paramtypes", [medicins_service_1.MedicinsService])
], MedicinsController);
exports.MedicinsController = MedicinsController;
//# sourceMappingURL=medicins.controller.js.map