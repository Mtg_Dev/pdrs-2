import { MedicinsService } from './medicins.service';
import { UpdateMedicineDto } from '../Dtos/updateMedicine.dto';
export declare class MedicinsController {
    private medicinsService;
    constructor(medicinsService: MedicinsService);
    getMedicins({ page, size }: {
        page: any;
        size: any;
    }, name: string): Promise<import("../entities/medicins.entity").Medicine[]>;
    addMedicine(newMedicine: {
        name: string;
    }): Promise<{
        name: string;
    } & import("../entities/medicins.entity").Medicine>;
    updateMedicine(medicine: UpdateMedicineDto): Promise<import("typeorm").UpdateResult>;
}
