import { User } from 'src/users/entities/user.entity';
export declare class MailingService {
    constructor();
    static sendCreateAccountEmail(user: User, password: string, name: string): Promise<any>;
    static sendResetPassword(email: string, token: string): Promise<any>;
    static newMessageToAdmin(name: string): Promise<any>;
    static responseMessage(content: string, to: string): Promise<any>;
    static newRequestToAdmin(name: string): Promise<any>;
}
