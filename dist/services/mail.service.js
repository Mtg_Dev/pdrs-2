"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MailingService = void 0;
const nodemailer = require("nodemailer");
const Mailgen = require("mailgen");
const config_1 = require("../utils/config");
const adminMailingList = ['mtg0987654321@gmail.com'];
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        type: 'OAuth2',
        user: 'pdrs.sys@gmail.com',
        clientId: config_1.configService.getEmailCredentials().clientID,
        clientSecret: config_1.configService.getEmailCredentials().clientSecret,
        refreshToken: config_1.configService.getEmailCredentials().refreshToken,
        accessToken: config_1.configService.getEmailCredentials().accessToken,
    },
});
transporter.verify(function (error, success) {
    if (error) {
        console.log(error);
    }
    else {
        console.log('Server is ready to take our messages');
    }
});
let MailGenerator = new Mailgen({
    theme: 'default',
    product: {
        name: 'PDRS',
        link: 'https://pdrs.herokuapp.com',
    },
});
const user_entity_1 = require("../users/entities/user.entity");
class MailingService {
    constructor() { }
    static async sendCreateAccountEmail(user, password, name) {
        let response = {
            body: {
                name,
                intro: [
                    'Welcome to PDRS!!',
                    'Your request have been accepted and You are now offically a member of this system.',
                    'Your Login Credentials Are:',
                    `Email: ${user.email}`,
                    `Password: ${password}`,
                ],
                action: {
                    instructions: 'To get started with PDRS, Login From Here:',
                    button: {
                        color: '#3f51b5',
                        text: 'Login Now',
                        link: 'https://pdrs.herokuapp.com/login',
                    },
                },
                outro: "Need help, or have questions? Just reply to this email, we'd love to help.",
            },
        };
        let mail = MailGenerator.generate(response);
        let message = {
            from: config_1.configService.getEmail(),
            to: user.email,
            subject: 'Welcome To PDRS!!',
            html: mail,
        };
        return await transporter.sendMail(message);
    }
    static async sendResetPassword(email, token) {
        let response = {
            body: {
                intro: [
                    'You have requested a password reset for your PDRS Account',
                    'if it is not you,then please simply ignore this email',
                    'this email is valid for 6 hours only',
                ],
                action: {
                    instructions: 'To set your new password use the link below:',
                    button: {
                        color: '#3f51b5',
                        text: 'Reset Password',
                        link: `https://pdrs.herokuapp.com/reset-password?token=${token}`,
                    },
                },
                outro: "Need help, or have questions? Just reply to this email, we'd love to help.",
            },
        };
        let mail = MailGenerator.generate(response);
        let message = {
            from: config_1.configService.getEmail(),
            to: email,
            subject: 'Reset Password',
            html: mail,
        };
        return await transporter.sendMail(message);
    }
    static async newMessageToAdmin(name) {
        let response = {
            body: {
                intro: [`Dr.${name} sent you a new message`],
                action: {
                    instructions: 'To View the message go to your dashboard:',
                    button: {
                        color: '#3f51b5',
                        text: 'Open Dashboard',
                        link: 'https://pdrs.herokuapp.com/admin',
                    },
                },
            },
        };
        let mail = MailGenerator.generate(response);
        let message = {
            from: config_1.configService.getEmail(),
            to: adminMailingList,
            subject: 'New Message!',
            html: mail,
        };
        return await transporter.sendMail(message);
    }
    static async responseMessage(content, to) {
        let response = {
            body: {
                intro: content,
            },
        };
        let mail = MailGenerator.generate(response);
        let message = {
            from: config_1.configService.getEmail(),
            to,
            subject: 'Message From PDRS',
            html: mail,
        };
        return await transporter.sendMail(message);
    }
    static async newRequestToAdmin(name) {
        let response = {
            body: {
                intro: [`${name} sent you an account request`],
                action: {
                    instructions: 'To View the request go to your dashboard:',
                    button: {
                        color: '#3f51b5',
                        text: 'Open Dashboard',
                        link: 'https://pdrs.herokuapp.com/admin',
                    },
                },
            },
        };
        let mail = MailGenerator.generate(response);
        let message = {
            from: config_1.configService.getEmail(),
            to: adminMailingList,
            subject: 'New Account Request!',
            html: mail,
        };
        return await transporter.sendMail(message);
    }
}
exports.MailingService = MailingService;
//# sourceMappingURL=mail.service.js.map